@echo off
set filesToDelete=*.ilk,*.pdb,*.idb,*.obj,*.sdf,*.tlog,*.ipch,*.psess,*.vsp,*.pch

rem add project-specifc file masks here
rem set filesToDelete=%filesToDelete%,

echo Cleaning large, temporary/useless files.

call :parse "%filesToDelete%"

pause
goto :eof

:parse
setlocal
set list=%~1
for /F "tokens=1* delims=," %%f in ("%list%") do (
    rem if the item exist
    if not "%%f" == "" call :processItem %%f
    rem if next item exist
    if not "%%g" == "" call :parse "%%g"
)
endlocal
goto :eof

:processItem
setlocal
del /S /Q %1
goto :eof
