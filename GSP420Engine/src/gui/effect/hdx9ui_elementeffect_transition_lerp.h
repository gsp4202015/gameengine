#pragma once

#pragma warning(disable:4250)

#include "gui/effect/hdx9ui_elementeffect_transition_base.h"
#include "gui/effect/hdx9ui_elementeffect_varcontrolbase.h"

template<typename T>
class HDXUIElementEffect_Transition_LERP : public HDXUIElementEffect_VarControlBase<T>,public HDXUIElementEffect_Transition_Base {
public:
	HDXUIElementEffect_Transition_LERP(const string_normal &name,
									   HDXUIElementBase *parent,
									   std::function<T(void)> func_varget,
									   std::function<void(const T &var)> func_varset,
									   DWORD runflags,
									   float transtime,
									   const T &target);
	HDXUIElementEffect_Transition_LERP(HDXUIElementBase *parent,void *pvar);
	virtual ~HDXUIElementEffect_Transition_LERP();

	T getTarget() const;
	void setTarget(const T &var);

	void swapValues();

	virtual void transition_onStart(bool enter);
	virtual void transition_onFinish();
	virtual void transition_onUpdate(float dt);

	virtual void update(float dt);

	virtual string_normal xml_getTypeName() const { return "transition_lerp"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const;

protected:
	virtual T _getEffect() const;
	virtual void _onDelta(const T &delta);

private:
	std::function<T(void)> mfunc_varget;
	std::function<void(const T &var)> mfunc_varset;
	T mtarget;
};

template<typename T>
HDXUIElementEffect_Transition_LERP<T>::HDXUIElementEffect_Transition_LERP(const string_normal &name,
																		  HDXUIElementBase *parent,
																		  std::function<T(void)> func_varget,
																		  std::function<void(const T &var)> func_varset,
																		  DWORD runflags,
																		  float transtime,
																		  const T &target) :
																		  HDXUIElementEffect_VarControlBase(name,parent,func_varget,func_varset),
																		  HDXUIElementEffect_Transition_Base(name,parent,runflags,transtime),
																		  HDXUIElementEffect_Base(name,parent) {
	mtarget = target;
}

template<typename T>
HDXUIElementEffect_Transition_LERP<T>::HDXUIElementEffect_Transition_LERP(HDXUIElementBase *parent,void *pvar) :
HDXUIElementEffect_VarControlBase(parent,pvar),
HDXUIElementEffect_Transition_Base(parent),
HDXUIElementEffect_Base(parent) {
}

template<typename T>
HDXUIElementEffect_Transition_LERP<T>::~HDXUIElementEffect_Transition_LERP() {
}

template<typename T>
T HDXUIElementEffect_Transition_LERP<T>::getTarget() const {
	return mtarget;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::setTarget(const T &var) {
	mtarget = var;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::swapValues() {
	T tmp = getFinal();
	setFinal(mtarget);
	mtarget = tmp;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::transition_onStart(bool enter) {
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::transition_onFinish() {
	// only swap values if this transition runs on both entrance and exit
	if(getRunFlags()&HDX_TRANS_ENTER && getRunFlags()&HDX_TRANS_EXIT) {
		swapValues();
	}
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::transition_onUpdate(float dt) {
	HDXUIElementEffect_VarControlBase<T>::update(dt);
}

template<typename T>
T HDXUIElementEffect_Transition_LERP<T>::_getEffect() const {
	float progress = getProgress();
	return (mtarget-getFinal())*progress;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::_onDelta(const T &delta) {
	mtarget += delta;
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::update(float dt) {
	HDXUIElementEffect_Transition_Base::update(dt);
	//do NOT run HDXUIElementEffect_VarControlBase's update here, it is in transition_onUpdate()
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementEffect_Transition_Base::xml_initialize(xmlscript,xmlnode,func_oninitialize);
	HDXUIElementEffect_VarControlBase<T>::xml_initialize(xmlscript,xmlnode,func_oninitialize);
}

template<typename T>
void HDXUIElementEffect_Transition_LERP<T>::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementEffect_Transition_Base::xml_node_read(xmlscript,xmlnode,func_oninitialize);
	HDXUIElementEffect_VarControlBase<T>::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"target")==0) {
		XMLRead(&mtarget,xmlscript,xmlnode);
	}
}

template<typename T>
bool HDXUIElementEffect_Transition_LERP<T>::xml_getVarByName(string_normal *out,const string_normal &name) const {
	if(HDXUIElementEffect_Transition_Base::xml_getVarByName(out,name) ||
	   HDXUIElementEffect_VarControlBase<T>::xml_getVarByName(out,name)) return true;

	string_normal tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="target") {
		return XMLToString(out,mtarget,arg);
	}

	return false;
}
