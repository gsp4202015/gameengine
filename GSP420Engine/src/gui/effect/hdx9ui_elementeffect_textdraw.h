#pragma once

#include "gui/effect/hdx9ui_elementeffect_base.h"

#include "gui/hdx9color.h"

class HDXUIElementEffect_TextDraw : public HDXUIElementEffect_Base {
public:
	HDXUIElementEffect_TextDraw(const string_normal &name,
								HDXUIElementBase *parent,
								const string_normal &font,
								const string_normal &text,
								HDXColor color);
	HDXUIElementEffect_TextDraw(HDXUIElementBase *parent);
	virtual ~HDXUIElementEffect_TextDraw();

	string_normal getFont() const;
	void setFont(const string_normal &font);

	string_normal getText() const;
	void setText(const string_normal &text);

	HDXColor getFontDrawColor() const;
	void setFontDrawColor(const HDXColor &color);

	void getFontDrawRect(RECT *rect) const;
	void setFontDrawRect(const RECT *rect);

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransiton);

	virtual void update(float dt);
	virtual void render();

	virtual string_normal xml_getTypeName() const { return "draw_text"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const;

private:
	string_normal mfont;
	string_normal mtext;
	RECT mfontdrawrect;
	HDXColor mfontdrawcolor;
};
