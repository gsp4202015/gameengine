#pragma once

#pragma warning(disable:4250)

#include "gui/effect/hdx9ui_elementeffect_transition_base.h"
#include "gui/effect/hdx9ui_elementeffect_varcontrolbase.h"

#include <type_traits>

template<typename T>
class HDXUIElementEffect_Var_Oscillate : public HDXUIElementEffect_VarControlBase<T> {
public:
	HDXUIElementEffect_Var_Oscillate(const string_normal &name,
									 HDXUIElementBase *parent,
									 std::function<T(void)> func_varget,
									 std::function<void(const T &var)> func_varset,
									 const T &wavelength,
									 float frequency,
									 float phaseshift = 0);
	HDXUIElementEffect_Var_Oscillate(HDXUIElementBase *parent,void *pvar);
	virtual ~HDXUIElementEffect_Var_Oscillate();

	T getWavelength() const;
	void setWavelength(const T &var);

	float getFrequency() const;
	void setFrequency(float frequency);

	float getFrquencyCounter() const;

	virtual void update(float dt);

	virtual string_normal xml_getTypeName() const { return "var_oscillate"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const;

protected:
	virtual T _getEffect() const;
	virtual void _onDelta(const T &delta);

private:
	std::function<T(void)> mfunc_varget;
	std::function<void(const T &var)> mfunc_varset;
	T mwavelength;
	float mfrequency;
	float mfrequency_counter;
};

template<typename T>
HDXUIElementEffect_Var_Oscillate<T>::HDXUIElementEffect_Var_Oscillate(const string_normal &name,
																	  HDXUIElementBase *parent,
																	  std::function<T(void)> func_varget,
																	  std::function<void(const T &var)> func_varset,
																	  const T &wavelength,
																	  float frequency,
																	  float phaseshift) :
																	  HDXUIElementEffect_VarControlBase(name,parent,func_varget,func_varset),
																	  HDXUIElementEffect_Base(name,parent) {
	mwavelength = wavelength;
	mfrequency = frequency;
	mfrequency_counter = phaseshift*mfrequency;
}

template<typename T>
HDXUIElementEffect_Var_Oscillate<T>::HDXUIElementEffect_Var_Oscillate(HDXUIElementBase *parent,void *pvar) :
HDXUIElementEffect_VarControlBase(parent,pvar),
HDXUIElementEffect_Base(parent) {
}

template<typename T>
HDXUIElementEffect_Var_Oscillate<T>::~HDXUIElementEffect_Var_Oscillate() {
}

template<typename T>
T HDXUIElementEffect_Var_Oscillate<T>::getWavelength() const {
	return mwavelength;
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::setWavelength(const T &var) {
	mwavelength = var;
}

template<typename T>
float HDXUIElementEffect_Var_Oscillate<T>::getFrequency() const {
	return mfrequency;
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::setFrequency(float frequency) {
	mfrequency = frequency;
}

template<typename T>
float HDXUIElementEffect_Var_Oscillate<T>::getFrquencyCounter() const {
	return mfrequency_counter;
}

template<typename T>
T HDXUIElementEffect_Var_Oscillate<T>::_getEffect() const {
	float s = sin((mfrequency_counter/mfrequency*3.14159f*2));
	return mwavelength*s;
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::_onDelta(const T &delta) {
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::update(float dt) {
	HDXUIElementEffect_VarControlBase<T>::update(dt);
	mfrequency_counter += dt;
	while(mfrequency_counter > mfrequency) {
		mfrequency_counter -= mfrequency;
	}
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	//does not work
	//memset(&mwavelength,0,sizeof(mwavelength));
	mfrequency = 0.0f;
	mfrequency_counter = 0.0f;

	HDXUIElementEffect_VarControlBase<T>::xml_initialize(xmlscript,xmlnode,func_oninitialize);
}

template<typename T>
void HDXUIElementEffect_Var_Oscillate<T>::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementEffect_VarControlBase<T>::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"wavelength")==0) {
		XMLRead(&mwavelength,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"frequency")==0) {
		XMLRead(&mfrequency,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"phaseshift")==0) {
		float phaseshift;
		XMLRead(&phaseshift,xmlscript,xmlnode);
		mfrequency_counter = mfrequency * phaseshift;
	}
}

template<typename T>
bool HDXUIElementEffect_Var_Oscillate<T>::xml_getVarByName(string_normal *out,const string_normal &name) const {
	if(HDXUIElementEffect_VarControlBase<T>::xml_getVarByName(out,name)) return true;

	string_normal tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="wavelength") {
		return XMLToString(out,mwavelength,arg);
	} else if(tok=="frequency") {
		return XMLToString(out,mfrequency,arg);
	} else if(tok=="phaseshift") {
		return XMLToString(out,mfrequency_counter/mfrequency,arg);
	}

	return false;
}