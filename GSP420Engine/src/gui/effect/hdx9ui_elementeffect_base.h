#pragma once

#include <directxmath.h>
#include <string>

#include "gui/hdx9ui_iuiactor.h"

#include <rapidxml.hpp>
#include <rapidxml_utils.hpp>
#include "gui/hdx9ui_xmlscript.h"

class HDXUIElementBase;

class HDXUIElementEffect_Base : public IHDXUIIActor {
public:
	HDXUIElementEffect_Base(const string_normal &name,HDXUIElementBase *parent);
	HDXUIElementEffect_Base(HDXUIElementBase *parent);
	virtual ~HDXUIElementEffect_Base();

	HDXUIElementBase* getParent() const;

	bool isActive() const;
	void setActive(bool active);

	bool isEnabled() const;
	void setEnabled(bool enabled);

	virtual float getZPos() const; //returns parent's zpos by default

	virtual float getDesiredTransitionTime() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransiton);

	virtual void update(float dt);
	virtual void render();

	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const;

private:
	HDXUIElementBase *mparent;
	bool mactive,menabled;
};
