#include "gui/effect/hdx9ui_elementeffect_base.h"

#include "gui/hdx9ui.h"

HDXUIElementEffect_Base::HDXUIElementEffect_Base(const string_normal &name,HDXUIElementBase *parent) {
	mname = name;
	mparent = parent;
	mactive = true;
	menabled = true;
}

HDXUIElementEffect_Base::HDXUIElementEffect_Base(HDXUIElementBase *parent) {
	mparent = parent;
}

HDXUIElementEffect_Base::~HDXUIElementEffect_Base() {
}

HDXUIElementBase* HDXUIElementEffect_Base::getParent() const {
	return mparent;
}

bool HDXUIElementEffect_Base::isActive() const {
	return mactive;
}

void HDXUIElementEffect_Base::setActive(bool active) {
	mactive = active;
}

bool HDXUIElementEffect_Base::isEnabled() const {
	return menabled;
}

void HDXUIElementEffect_Base::setEnabled(bool enabled) {
	menabled = mactive;
}

float HDXUIElementEffect_Base::getZPos() const {
	return mparent->getZPos();
}

float HDXUIElementEffect_Base::getDesiredTransitionTime() const {
	return 0.0f;
}

void HDXUIElementEffect_Base::onMenuTransitionEnter(bool dotransition) {
}

void HDXUIElementEffect_Base::onMenuTransitionExit(bool dotransiton) {
}

void HDXUIElementEffect_Base::update(float dt) {
}

void HDXUIElementEffect_Base::render() {
}

void HDXUIElementEffect_Base::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	mname = xmlnode->first_attribute("name")->value();

	mactive = true;
	menabled = true;

	for(rapidxml::xml_node<> *node_i = xmlnode->first_node(); node_i; node_i = node_i->next_sibling()) {
		xml_node_read(xmlscript,node_i,func_oninitialize);
	}

	if(func_oninitialize != nullptr) func_oninitialize(this,xmlnode);
}

void HDXUIElementEffect_Base::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	xmlscript->setScriptData_this(this,false);

	if(strcmp(xmlnode->name(),"enabled")==0) {
		XMLRead(&menabled,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"active")==0) {
		XMLRead(&mactive,xmlscript,xmlnode);
	}
}

bool HDXUIElementEffect_Base::xml_getVarByName(string_normal *out,const string_normal &name) const {
	string_normal tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="enabled") {
		return XMLToString(out,menabled,arg);
	} else if(tok=="active") {
		return XMLToString(out,mactive,arg);
	}

	return false;
}