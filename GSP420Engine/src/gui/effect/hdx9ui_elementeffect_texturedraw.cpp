#include "gui/effect/hdx9ui_elementeffect_texturedraw.h"

#include "graphics\DX11\Tools\Texture2D.h"
#include "graphics\DX11\Manager\TextureManager.h"

#include "gui\hdx9ui.h"

#include "gui\hdx9color.h"

#include "core\Engine.h"

HDXUIElementEffect_TextureDraw::HDXUIElementEffect_TextureDraw(const string_normal &name,
															   HDXUIElementBase *parent,
															   const string_normal &tex) : HDXUIElementEffect_Base(name,parent) {
	mtexture = nullptr;
	setTexture(tex);
	mblendcolor = 0xffffffff; //white, no blend
	setSourceRectToWholeTexture();
	setSourceCenterToCenterSourceRect();
}

HDXUIElementEffect_TextureDraw::HDXUIElementEffect_TextureDraw(HDXUIElementBase *parent) : HDXUIElementEffect_Base(parent) {
}

HDXUIElementEffect_TextureDraw::~HDXUIElementEffect_TextureDraw() {
}

string_normal HDXUIElementEffect_TextureDraw::getTexture() const {
	if(mtexture) return mtexture->GetRegistrationName();
	return "";
}

void HDXUIElementEffect_TextureDraw::setTexture(const string_normal &tex) {
	auto dxDevice = EngineSettings::Instance().graphics.device;
	mtexture = EngineSettings::Instance().graphics.textureManager->GetOrLoadTexture(dxDevice,tex,tex);
}

HDXColor HDXUIElementEffect_TextureDraw::getBlendColor() const {
	return mblendcolor;
}

void HDXUIElementEffect_TextureDraw::setBlendColor(HDXColor blendcolor) {
	mblendcolor = blendcolor;
}

void HDXUIElementEffect_TextureDraw::getSourceRect(RECT *rect) const {
	*rect = msource_rect;
}

void HDXUIElementEffect_TextureDraw::setSourceRect(const RECT *rect) {
	msource_rect = *rect;
}

int HDXUIElementEffect_TextureDraw::getSourceWidth() const {
	return (msource_rect.right-msource_rect.left);
}

int HDXUIElementEffect_TextureDraw::getSourceHeight() const {
	return (msource_rect.bottom-msource_rect.top);
}

void HDXUIElementEffect_TextureDraw::setSourceRectToWholeTexture() {
	SetRect(&msource_rect,0,0,mtexture->GetWidth(),mtexture->GetHeight());
}

Vector3 HDXUIElementEffect_TextureDraw::getSourceCenter() const {
	Vector3 ret;
	getSourceCenter(&ret);
	return ret;
}

void HDXUIElementEffect_TextureDraw::getSourceCenter(Vector3 *center) const {
	*center = msource_center;
}

void HDXUIElementEffect_TextureDraw::setSourceCenter(const Vector3 *center) {
	msource_center = *center;
}

void HDXUIElementEffect_TextureDraw::setSourceCenterToCenterSourceRect() {
	msource_center.x = (msource_rect.right-msource_rect.left)/2.0f;
	msource_center.y = (msource_rect.bottom-msource_rect.top)/2.0f;
	msource_center.z = 0;
}

float HDXUIElementEffect_TextureDraw::getZPos() const {
	return HDXUI_GETZABOVE(getParent()->getZPos());
}

void HDXUIElementEffect_TextureDraw::onMenuTransitionEnter(bool dotransition) {
}

void HDXUIElementEffect_TextureDraw::onMenuTransitionExit(bool dotransiton) {
}

void HDXUIElementEffect_TextureDraw::update(float dt) {
}

void HDXUIElementEffect_TextureDraw::render() {
	//if(mtexture) {
		//ID3DXSprite *com_sprite = HDX_MAIN->getD3DSprite();

		Matrix trans_parent;
		getParent()->getTransform(&trans_parent);

		Vector3 position;
		Vector3 scale;
		Quaternion rotation;
		trans_parent.Decompose(scale,rotation,position);
		float rot = quatToEuler(rotation).z;

		position.z = getZPos();

		HDXColor pcol;
		if(getParent()) {
			pcol = getParent()->getBlendColor();
		} else {
			pcol = 0xffffffff;
		}
		HDXColor mcol = getBlendColor();
		HDXColor tcol = colorMultiply(pcol,mcol);

		auto spriteBatch = EngineSettings::Instance().graphics.spriteBatch;

		const Vector2 origin = Vector2(mtexture->GetWidth()/2.0f,
									   mtexture->GetHeight()/2.0f);

		spriteBatch->Draw(mtexture->GetTexture(),position,nullptr,tcol,rot,origin,scale);

		//com_sprite->Draw(mtexture->getD3DTexture(),&msource_rect,msource_center.getD3DX(),pos.getD3DX(),tcol);

		//com_sprite->Flush();
	//}
}

void HDXUIElementEffect_TextureDraw::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	mtexture = 0;
	setBlendColor(0xffffffff);

	HDXUIElementEffect_Base::xml_initialize(xmlscript,xmlnode,func_oninitialize);
}

void HDXUIElementEffect_TextureDraw::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementEffect_Base::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"file")==0) {
		string_normal texfname;
		XMLRead(&texfname,xmlscript,xmlnode);
		setTexture(texfname);
	} else if(strcmp(xmlnode->name(),"blendcolor")==0) {
		XMLRead(&mblendcolor,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"sourcerect")==0) {
		if(strcmp(xmlnode->first_attribute("mode")->value(),"texture")==0) {
			setSourceRectToWholeTexture();
		} else if(strcmp(xmlnode->first_attribute("mode")->value(),"rect")==0) {
			XMLRead(&msource_rect,xmlscript,xmlnode);
		}
	} else if(strcmp(xmlnode->name(),"sourcecenter")==0) {
		if(strcmp(xmlnode->first_attribute("mode")->value(),"sourcerect")==0) {
			setSourceCenterToCenterSourceRect();
		} else if(strcmp(xmlnode->first_attribute("mode")->value(),"pos")==0) {
			XMLRead(&msource_center,xmlscript,xmlnode);
		}
	} else if(strcmp(xmlnode->name(),"setparentactionrect")==0) {
		Vector2 scale;
		XMLRead(&scale,xmlscript,xmlnode);
		getParent()->setActionWidth(scale.x * (float)RECTWIDTH(msource_rect));
		getParent()->setActionHeight(scale.y * (float)RECTHEIGHT(msource_rect));
	}
}

bool HDXUIElementEffect_TextureDraw::xml_getVarByName(string_normal *out,const string_normal &name) const {
	if(HDXUIElementEffect_Base::xml_getVarByName(out,name)) return true;

	string_normal tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="file") {
		return XMLToString(out,mtexture ? mtexture->GetFileName() : "",arg);
	} else if(tok=="blendcolor") {
		return XMLToString(out,mblendcolor,arg);
	} else if(tok=="sourcerect") {
		return XMLToString(out,msource_rect,arg);
	} else if(tok=="sourcecenter") {
		return XMLToString(out,msource_center,arg);
	}

	return false;
}