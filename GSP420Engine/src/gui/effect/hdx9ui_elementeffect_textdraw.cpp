#include "gui/effect/hdx9ui_elementeffect_textdraw.h"

#include "gui/hdx9ui.h"

#include "gui/hdx9color.h"

#include "core/Engine.h"

HDXUIElementEffect_TextDraw::HDXUIElementEffect_TextDraw(const string_normal &name,
														 HDXUIElementBase *parent,
														 const string_normal &font,
														 const string_normal &text,
														 HDXColor color) : HDXUIElementEffect_Base(name,parent) {
	mfont = font;
	mfontdrawcolor = color;
	setText(text);

	rectSetPosSize(&mfontdrawrect,0,0,(int)parent->getActionWidth(),(int)parent->getActionHeight());
}

HDXUIElementEffect_TextDraw::HDXUIElementEffect_TextDraw(HDXUIElementBase *parent) : HDXUIElementEffect_Base(parent) {
}

HDXUIElementEffect_TextDraw::~HDXUIElementEffect_TextDraw() {
}

string_normal HDXUIElementEffect_TextDraw::getFont() const {
	return mfont;
}

void HDXUIElementEffect_TextDraw::setFont(const string_normal &font) {
	mfont = font;
}

string_normal HDXUIElementEffect_TextDraw::getText() const {
	return mtext;
}

void HDXUIElementEffect_TextDraw::setText(const string_normal &text) {
	mtext = text;
}

HDXColor HDXUIElementEffect_TextDraw::getFontDrawColor() const {
	return mfontdrawcolor;
}

void HDXUIElementEffect_TextDraw::setFontDrawColor(const HDXColor &color) {
	mfontdrawcolor = color;
}

void HDXUIElementEffect_TextDraw::getFontDrawRect(RECT *rect) const {
	*rect = mfontdrawrect;
}

void HDXUIElementEffect_TextDraw::setFontDrawRect(const RECT *rect) {
	mfontdrawrect = *rect;
}

void HDXUIElementEffect_TextDraw::onMenuTransitionEnter(bool dotransition) {
}

void HDXUIElementEffect_TextDraw::onMenuTransitionExit(bool dotransiton) {
}

void HDXUIElementEffect_TextDraw::update(float dt) {
}

void HDXUIElementEffect_TextDraw::render() {
	//HDXFontBase *testfont = HDX_FONTMAN->getFont(mfont);
	//if(testfont) {
		//ID3DXSprite *com_sprite = HDX_MAIN->getD3DSprite();

		Matrix trans_parent;
		getParent()->getTransform(&trans_parent);

		Vector3 position;
		Vector3 scale;
		Quaternion rotation;
		trans_parent.Decompose(scale,rotation,position);
		float rot = quatToEuler(rotation).z;

		//com_sprite->SetTransform(trans_parent.getD3DX());

		HDXColor pcol;
		if(getParent()) {
			pcol = getParent()->getBlendColor();
		} else {
			pcol = 0xffffffff;
		}
		HDXColor mcol = getFontDrawColor();
		HDXColor tcol = colorMultiply(pcol,mcol);

		auto spriteBatch = EngineSettings::Instance().graphics.spriteBatch;
		auto font = EngineSettings::Instance().graphics.spriteFont;
		auto stringToDraw = string_normal_to_wide(mtext);
		
		Vector2 origin = font->MeasureString(stringToDraw.c_str());
		origin *= 0.5f;

		const Vector2 printLocation(
			(mfontdrawrect.right + mfontdrawrect.left)/2.0f,
			(mfontdrawrect.bottom + mfontdrawrect.top)/2.0f);

		font->DrawString(spriteBatch,stringToDraw.c_str(),position,tcol,rot,origin,scale);

		//testfont->drawText(mtext.c_str(),mfontdrawrect,mfontdrawflags,tcol,com_sprite);
	//}
}

void HDXUIElementEffect_TextDraw::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	setFont("");
	setFontDrawColor(0xffffffff);
	setText("");
	rectSetPosSize(&mfontdrawrect,0,0,(int)getParent()->getActionWidth(),(int)getParent()->getActionHeight());

	HDXUIElementEffect_Base::xml_initialize(xmlscript,xmlnode,func_oninitialize);
}

void HDXUIElementEffect_TextDraw::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementEffect_Base::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"font")==0) {
		string_normal fontname;
		XMLRead(&fontname,xmlscript,xmlnode);
		setFont(fontname);
	} else if(strcmp(xmlnode->name(),"color")==0) {
		XMLRead(&mfontdrawcolor,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"text")==0) {
		string_normal text;
		XMLRead(&text,xmlscript,xmlnode);
		setText(text);
	} else if(strcmp(xmlnode->name(),"drawrect")==0) {
		if(strcmp(xmlnode->first_attribute("mode")->value(),"actionsize")==0) {
			rectSetPosSize(&mfontdrawrect,0,0,(int)getParent()->getActionWidth(),(int)getParent()->getActionHeight());
		} else if(strcmp(xmlnode->first_attribute("mode")->value(),"rect")==0) {
			XMLRead(&mfontdrawrect,xmlscript,xmlnode);
		}
	}
}

bool HDXUIElementEffect_TextDraw::xml_getVarByName(string_normal *out,const string_normal &name) const {
	if(HDXUIElementEffect_Base::xml_getVarByName(out,name)) return true;

	string_normal tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="font") {
		return XMLToString(out,mfont,arg);
	} else if(tok=="blendcolor") {
		return XMLToString(out,mfontdrawcolor,arg);
	} else if(tok=="drawrect") {
		return XMLToString(out,mfontdrawrect,arg);
	} else if(tok=="text") {
		return XMLToString(out,mtext,arg);
	}

	return false;
}