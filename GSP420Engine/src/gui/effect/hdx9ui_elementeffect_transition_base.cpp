#include "gui/effect/hdx9ui_elementeffect_transition_base.h"

#include <algorithm>

HDXUIElementEffect_Transition_Base::HDXUIElementEffect_Transition_Base(const string_normal &name,
																	   HDXUIElementBase *parent,
																	   DWORD runflags,
																	   float transtime) : HDXUIElementEffect_Base(name,parent) {
	mrunflags = runflags;
	mtranstime = transtime;
	mtranscounter = -1.0f;
}

HDXUIElementEffect_Transition_Base::HDXUIElementEffect_Transition_Base(HDXUIElementBase *parent) : HDXUIElementEffect_Base(parent) {
}

HDXUIElementEffect_Transition_Base::~HDXUIElementEffect_Transition_Base() {
}

DWORD HDXUIElementEffect_Transition_Base::getRunFlags() const {
	return mrunflags;
}

void HDXUIElementEffect_Transition_Base::setRunFlags(DWORD runflags) {
	mrunflags = runflags;
}

float HDXUIElementEffect_Transition_Base::getTransTime() const {
	return mtranstime;
}

void HDXUIElementEffect_Transition_Base::setTransTime(float time) {
	mtranstime = time;
}

float HDXUIElementEffect_Transition_Base::getTransCounter() const {
	return mtranscounter;
}

void HDXUIElementEffect_Transition_Base::setTransCounter(float counter) {
	mtranscounter = counter;
}

float HDXUIElementEffect_Transition_Base::getProgress() const {
	return mtranstime>0 ? (mtranscounter<mtranstime ? (mtranscounter/mtranstime) : 1.0f) : 1.0f;
}

bool HDXUIElementEffect_Transition_Base::isTransitionDone() const {
	return mtranstime<=0 || mtranscounter<0 || mtranscounter>mtranstime;
}

float HDXUIElementEffect_Transition_Base::getDesiredTransitionTime() const {
	return mtranstime;// (std::max)(0.0f,mtranstime-mtranscounter);
}

void HDXUIElementEffect_Transition_Base::onMenuTransitionEnter(bool dotransition) {
	HDXUIElementEffect_Base::onMenuTransitionEnter(dotransition);
	if(getRunFlags()&HDX_TRANS_ENTER) {
		mtranscounter = dotransition ? 0.0f : getDesiredTransitionTime();
		transition_onStart(true);
	}
}

void HDXUIElementEffect_Transition_Base::onMenuTransitionExit(bool dotransition) {
	HDXUIElementEffect_Base::onMenuTransitionExit(dotransition);
	if(getRunFlags()&HDX_TRANS_EXIT) {
		mtranscounter = dotransition ? 0.0f : getDesiredTransitionTime();
		transition_onStart(false);
	}
}

void HDXUIElementEffect_Transition_Base::update(float dt) {
	if(!isTransitionDone()) {
		mtranscounter += dt;
		transition_onUpdate(dt);
		if(isTransitionDone()) {
			transition_onFinish();
			mtranscounter = -1.0f;
		}
	}

	HDXUIElementEffect_Base::update(dt);
}

void HDXUIElementEffect_Transition_Base::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	setTransTime(0.0f);
	setRunFlags(0);

	HDXUIElementEffect_Base::xml_initialize(xmlscript,xmlnode,func_oninitialize);
}

void HDXUIElementEffect_Transition_Base::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementEffect_Base::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"duration")==0) {
		XMLRead(&mtranstime,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"runflags")==0) {
		XMLRead(&mrunflags,xmlscript,xmlnode);
	}
}

bool HDXUIElementEffect_Transition_Base::xml_getVarByName(string_normal *out,const string_normal &name) const {
	if(HDXUIElementEffect_Base::xml_getVarByName(out,name)) return true;

	string_normal tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="duration") {
		return XMLToString(out,mtranstime,arg);
	}

	return false;
}