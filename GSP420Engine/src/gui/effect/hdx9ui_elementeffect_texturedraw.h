#pragma once

#include "gui/effect/hdx9ui_elementeffect_base.h"

#include "gui/hdx9color.h"

class DXTexture2D;

class HDXUIElementEffect_TextureDraw : public HDXUIElementEffect_Base {
public:
	HDXUIElementEffect_TextureDraw(const string_normal &name,
								   HDXUIElementBase *parent,
								   const string_normal &tex);
	HDXUIElementEffect_TextureDraw(HDXUIElementBase *parent);
	virtual ~HDXUIElementEffect_TextureDraw();

	string_normal getTexture() const;
	void setTexture(const string_normal &tex);

	HDXColor getBlendColor() const;
	void setBlendColor(HDXColor blendcolor);

	void getSourceRect(RECT *rect) const;
	void setSourceRect(const RECT *rect);
	int getSourceWidth() const;
	int getSourceHeight() const;
	void setSourceRectToWholeTexture();

	Vector3 getSourceCenter() const;
	void getSourceCenter(Vector3 *center) const;
	void setSourceCenter(const Vector3 *center);
	void setSourceCenterToCenterSourceRect();

	virtual float getZPos() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransiton);

	virtual void update(float dt);
	virtual void render();

	virtual string_normal xml_getTypeName() const { return "draw_texture"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const;

private:
	DXTexture2D *mtexture;
	RECT msource_rect;
	Vector3 msource_center;
	HDXColor mblendcolor;
};
