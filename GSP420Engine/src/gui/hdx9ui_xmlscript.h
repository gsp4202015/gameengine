#pragma once

#include "core\EngineTypes.h"

#include "xmlutils.h"

#include <windows.h>

#include "core\EngineMath.h"

class IHDXUIIActor;

class HDXUIXMLScript : public XMLScript {
public:

	class Source_UIActor : public Source {
	public:
		Source_UIActor(const string_normal &name,XMLScript *parentscript);

		virtual string_normal dereference(Source *prevsource,const string_normal &entryname,const string_normal &str);

		IHDXUIIActor *mcuractor;
		bool miselement;
	};

	HDXUIXMLScript();
	HDXUIXMLScript(const HDXUIXMLScript&) = delete;
	HDXUIXMLScript(HDXUIXMLScript&&) = delete;
	~HDXUIXMLScript();

	void setScriptData_this(IHDXUIIActor *data,bool iselement);
	void setScriptData_this(std::pair<IHDXUIIActor*,bool> data);
	std::pair<IHDXUIIActor*,bool> getScriptData_this() const;

private:
	IHDXUIIActor *mthis;
	bool mthis_iselement;
};

bool XMLRead(struct HDXColor *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(Vector2 *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(Vector3 *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(RECT *out,XMLScript *script,rapidxml::xml_node<> *node);

template<> bool XMLToString<HDXColor>(string_normal *out,const HDXColor &in,string_normal arg);
template<> bool XMLToString<Vector2>(string_normal *out,const Vector2 &in,string_normal arg);
template<> bool XMLToString<Vector3>(string_normal *out,const Vector3 &in,string_normal arg);
template<> bool XMLToString<RECT>(string_normal *out,const RECT &in,string_normal arg);
