#pragma once

#include <unordered_map>
#include <vector>
#include <functional>
#include <cassert>

template<class DATA,class KEY,class HASH>
class __BaseManager {
public:
	class iterator {
		friend class __BaseManager<DATA,KEY,HASH>;

	public:
		typedef iterator self_type;
		typedef DATA value_type;
		typedef DATA& reference;
		typedef DATA* pointer;
		typedef std::random_access_iterator_tag iterator_category;
		typedef int difference_type;

		iterator() {}
		iterator(const iterator &r) { msubit = r.msubit; }

		self_type operator++() { msubit++; return *this; }
		self_type operator++(int) { msubit++; return *this; }
		self_type operator--() { msubit--; return *this; }
		self_type operator--(int) { msubit--; return *this; }

		self_type operator+(difference_type r) { return self_type(msubit+r); }
		self_type operator-(difference_type r) { return self_type(msubit-r); }

		self_type operator+=(difference_type r) { msubit += r; return *this; }
		self_type operator-=(difference_type r) { msubit -= r; return *this; }

		reference operator*() { return **msubit; }
		pointer operator->() { return *msubit; }
		bool operator==(const self_type &r) { return msubit == r.msubit; }
		bool operator!=(const self_type &r) { return msubit != r.msubit; }

		reference operator[](difference_type r) const { return *((*this)+r); }

	private:
		typename std::vector<DATA*>::iterator msubit;

		iterator(typename std::vector<DATA*>::iterator subit) : msubit(subit) {}
	};

	__BaseManager() {
	}

	virtual ~__BaseManager() {
	}

	iterator begin() {
		return iterator(mitemvector.begin());
	}

	const iterator begin() const {
		return iterator(mitemvector.begin());
	}

	iterator end() {
		return iterator(mitemvector.end());
	}

	const iterator end() const {
		return iterator(mitemvector.end());
	}

	void addItem(const KEY &id,const DATA &data) {
		//assert(mitemmap.count(id)==0); //items must have unique key
		mitemmap[id] = data;
		mitemvector.push_back(&mitemmap[id]); //possible issue here, if problems arise use new above
	}

	bool hasItem(const KEY &id) const {
		return mitemmap.count(id)>0;
	}

	DATA& getItem(const KEY &id) {
		//assert(hasItem(id));
		return mitemmap[id];
	}

	const DATA& getItem(const KEY &id) const {
		assert(hasItem(id));
		return mitemmap.find(id)->second;
	}

	void setItem(const KEY &id,const DATA &data) {
		assert(hasItem(id));
		mitemmap.find(id)->second = data;
	}

	void removeItem(const KEY &id) {
		auto &fitmap = mitemmap.find(id);
		if(fitmap != mitemmap.end()) {
			mitemvector.erase(std::find(mitemvector.begin(),mitemvector.end(),&fitmap->second));
			mitemmap.erase(fitmap);
		}
	}

	unsigned size() const {
		return mitemvector.size();
	}

	void clear() {
		mitemmap.clear();
		mitemvector.clear();
	}

	DATA& operator[](const KEY &r) {
		if(!hasItem(r)) {
			addItem(r,DATA());
		}
		return getItem(r);
	}

	const DATA& operator[](const KEY &r) const {
		return getItem(r);
	}

	void doIterate(std::function<bool(DATA &item)> &&func) {
		for(unsigned int i = 0; i < mitemvector.size(); i++) {
			if(func(*mitemvector[i])) break;
		}
	}

	void doIterate(std::function<bool(const DATA &item)> &&func) const {
		for(unsigned int i = 0; i < mitemvector.size(); i++) {
			if(func(*mitemvector[i])) break;
		}
	}

private:
	std::unordered_map<KEY,DATA,HASH> mitemmap;
	std::vector<DATA*> mitemvector;
};

template<class DATA,class KEY = int,class HASH = std::hash<KEY>>
class BaseManagerKey : public __BaseManager<DATA,KEY,HASH> {
public:
	BaseManagerKey() {
	}

	virtual ~BaseManagerKey() {
	}
};

template<class DATA>
class BaseManagerID : public __BaseManager<DATA,int,std::hash<int>> {
private:
	__BaseManager<DATA,int,std::hash<int>>::addItem; //custom version
public:
	BaseManagerID() {
		mnvid = 0;
	}

	virtual ~BaseManagerID() {
	}

	int addItem(const DATA &data) {
		int newid = _getNVID();
		addItem(newid,data);
		return newid;
	}

	//public version, does not increment counter
	int getNVID() {
		return mnvid;
	}

private:
	int mnvid;

	int _getNVID() {
		return mnvid++;
	}
};
