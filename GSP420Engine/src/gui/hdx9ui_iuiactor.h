#pragma once

#include "gui/hdx9ui_xmlscript.h"

class IHDXUIIActor {
public:
	IHDXUIIActor();
	virtual ~IHDXUIIActor() {};

	string_normal getName() const { return mname; }

	virtual void onMenuTransitionEnter(bool dotransition) = 0;
	virtual void onMenuTransitionExit(bool dotransition) = 0;

	virtual void* getUserData() const { return muserdata; }
	virtual void setUserData(void *data) { muserdata = data; }

	virtual void update(float dt) = 0;
	virtual void render() = 0;

	virtual string_normal xml_getTypeName() const { return "void"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr) {}
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr) {}
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const { return false; }

protected:
	string_normal mname;

private:
	void *muserdata;
};
