#pragma once

#include <windows.h>
#include "core\EngineMath.h"

class HDX9IntersectionTests {
public:
	static bool box_point(const Vector3 *boxext,const Vector3 *boxpos,const Vector3 *point);
	static bool box_point(const RECT *box,const POINT *point);

	static bool obb_point(const Vector3 *boxext,const Matrix *trans,const Vector3 *point);

	static bool obb_obb(const Vector3 *boxext0,const Matrix *trans0,const Vector3 *boxext1,const Matrix *trans1);
};
