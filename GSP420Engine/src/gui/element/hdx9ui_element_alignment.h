#pragma once

#include "gui/hdx9ui.h"

#define HDXAL_CENTER 0x00000001
#define HDXAL_LEFT 0x00000002
#define HDXAL_RIGHT 0x00000004
#define HDXAL_VCENTER 0x00000008
#define HDXAL_TOP 0x00000010
#define HDXAL_BOTTOM 0x00000020

class HDXUIElement_Alignment : public HDXUIElementBase {
public:
	HDXUIElement_Alignment(const string_normal name,
						   HDXUIElementBase *parent,
						   DWORD flags,
						   float dirx,
						   float diry,
						   float offx,
						   float offy,
						   float padding);
	HDXUIElement_Alignment(HDXUIElementBase *parent);
	virtual ~HDXUIElement_Alignment();

	Vector2 getDirection() const;
	void getDirection(Vector2 *dir) const;
	void setDirection(const Vector2 *dir);
	void setDirection(const float &x,const float &y);

	Vector2 getCurrentPostion() const;
	void getCurrentPostion(Vector2 *pos) const;
	void setCurrentPostion(const Vector2 *pos);
	void setCurrentPostion(const float &x,const float &y);

	Vector2 getNextPostion(HDXUIElementBase *newelement);
	void getNextPostion(Vector2 *pos,HDXUIElementBase *newelement);

	void jumpDistance(float dst);

	DWORD getFlags() const;
	void setFlags(const DWORD &flags);

	Vector2 getOffset() const;
	void getOffset(Vector2 *pos) const;
	void setOffset(const Vector2 *pos);
	void setOffset(const float &x,const float &y);

	float getPadding() const;
	void setPadding(const float &padding);

	virtual string_normal xml_getTypeName() const { return "alignment"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const;

protected:
	virtual void _addChild(HDXUIElementBase *child);

private:
	DWORD mflags;
	Vector2 mdir,mcurpos,moffset;
	float mpadding;
};
