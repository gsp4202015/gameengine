#include "gui/element/hdx9ui_element_slider.h"

#include "gui/effect/hdx9ui_elementeffect_texturedraw.h"
#include "gui/effect/hdx9ui_elementeffect_textdraw.h"

#include "core\Engine.h"
#include "input\Input.h"

HDXUIElement_Slider::HDXUIElement_Slider_Handle::HDXUIElement_Slider_Handle(const string_normal &name,
																			HDXUIElementBase *parent,
																			const string_normal &maintexture,
																			float size,
																			float xoff,
																			float yoff) : HDXUIElementBase(name,parent) {
	meffect_texture_main = createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																		maintexture);
	RECT texture_source_rect;
	meffect_texture_main->getSourceRect(&texture_source_rect);
	setActionSize((float)RECTWIDTH(texture_source_rect),
				  (float)RECTHEIGHT(texture_source_rect));

	msize = size;
	moffset.x = xoff;
	moffset.y = yoff;

	mdragging = false;
	mcallbackid_clicked = -1;
	mcallbackid_mouse_release = -1;
	mcallbackid_mouse_move = -1;

	mcallbackid_clicked = registerCallbackOnMousePress([&](WPARAM key)->void {
		if(key==VK_LBUTTON) mdragging = true;
	});

	mcallbackid_mouse_release = registerCallbackOnMouseRelease([&](WPARAM key,float timeheld)->void {
		if(key==VK_LBUTTON) mdragging = false;
	});

	mblendreset = mblendprev = meffect_texture_main->getBlendColor();
	mprevhighlight = false;
}

HDXUIElement_Slider::HDXUIElement_Slider_Handle::~HDXUIElement_Slider_Handle() {
	unregisterCallbackOnMousePress(mcallbackid_clicked);
	unregisterCallbackOnMouseRelease(mcallbackid_mouse_release);
}

void HDXUIElement_Slider::HDXUIElement_Slider_Handle::onMenuTransitionEnter(bool dotransition) {
	HDXUIElementBase::onMenuTransitionEnter(dotransition);
}

void HDXUIElement_Slider::HDXUIElement_Slider_Handle::onMenuTransitionExit(bool dotransition) {
	HDXUIElementBase::onMenuTransitionExit(dotransition);
}

void HDXUIElement_Slider::HDXUIElement_Slider_Handle::update(float dt) {
	HDXUIElementBase::update(dt);

	auto mouseMovement = EngineSettings::Instance().system.input.getMouseMotion();
	if(mouseMovement != MousePoint::Zero) {
		if(mdragging) {
			HDXUIElement_Slider *parent = (HDXUIElement_Slider*)getParent();

			const auto &mousePos = EngineSettings::Instance().system.input.getMousePostion();

			Vector2 pos_slider,pos_slider_new,pos_cursor_parent;
			Matrix mat_trans_parent,mat_trans_parent_inv;
			parent->getTransform(&mat_trans_parent);
			mat_trans_parent_inv = mat_trans_parent.Invert();
			getPosition(&pos_slider);
			pos_cursor_parent += Vector2(VEC2TOARG(mousePos)) * mat_trans_parent_inv;

			pos_slider_new = pos_slider;
			pos_slider_new.x = CLAMP((float)pos_cursor_parent.x,
									 -msize/2.0f,
									 msize/2.0f);

			setPosition(pos_slider_new);
		}
	}

	if(meffect_texture_main) {
		HDXColor blendcur = meffect_texture_main->getBlendColor();
		if(blendcur != mblendprev) {
			HDXColor delta = blendcur - mblendprev;
			mblendreset += delta;
		}

		if(mdragging || isMouseOver()) {
			if(!mprevhighlight) {
				meffect_texture_main->setBlendColor(colorMultiply(mblendreset,0xffff5555));
			}
			mprevhighlight = true;
		} else {
			if(mprevhighlight) {
				meffect_texture_main->setBlendColor(mblendreset);
			}
			mprevhighlight = false;
		}

		mblendprev = meffect_texture_main->getBlendColor();
	}
}

HDXUIElement_Slider::HDXUIElement_Slider(const string_normal &name,
										 HDXUIElementBase *parent,
										 const string_normal &maintexture,
										 const string_normal &handletexture,
										 const string_normal &textfont,
										 const string_normal &text,
										 DWORD textflags,
										 HDXColor textcolor) : HDXUIElementBase(name,parent) {
	meffect_texture_main = createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																		maintexture);

	meffect_text = createEffect<HDXUIElementEffect_TextDraw>("text_main",
															 textfont,
															 text,
															 textcolor);

	RECT texture_source_rect;
	meffect_texture_main->getSourceRect(&texture_source_rect);
	setActionSize((float)RECTWIDTH(texture_source_rect),
				  (float)RECTHEIGHT(texture_source_rect));

	RECT text_draw_rect;
	rectSetPosSize(&text_draw_rect,(int)getPosition().x,(int)getPosition().y,(int)getActionWidth(),(int)getActionHeight());
	rectShrink(&text_draw_rect,5,5);

	meffect_text->setFontDrawRect(&text_draw_rect);

	mhandle = createChildElement<HDXUIElement_Slider_Handle>("handle",
															 handletexture,
															 getActionWidth()*0.825f,
															 0.0f,
															 0.0f);

	mprevvalue = getValue();
}

HDXUIElement_Slider::HDXUIElement_Slider(HDXUIElementBase *parent) : HDXUIElementBase(parent) {
}

HDXUIElement_Slider::~HDXUIElement_Slider() {
}

HDXUIElementEffect_TextureDraw* HDXUIElement_Slider::getMainTextureEffect() const {
	return meffect_texture_main;
}

HDXUIElementEffect_TextureDraw* HDXUIElement_Slider::getHandleTextureEffect() const {
	return mhandle ? mhandle->meffect_texture_main : 0;
}

HDXUIElementEffect_TextDraw* HDXUIElement_Slider::getTextEffect() const {
	return meffect_text;
}

HDXUIElement_Slider::HDXUIElement_Slider_Handle* HDXUIElement_Slider::getHandle() const {
	return mhandle;
}

float HDXUIElement_Slider::getValue() const {
	return mhandle ? (mhandle->getPosition().x+mhandle->msize/2)/mhandle->msize : 0;
}

void HDXUIElement_Slider::setValue(float value) {
	value = (std::min)(1.0f,(std::max)(0.0f,value)); //clamp value to workable area
	mhandle->setPosition((value-0.5f)*(mhandle->msize),
						 mhandle->getPosition().y);
}

int HDXUIElement_Slider::registerCallbackOnValueChange(std::function<void(float value)> &&func) {
	return mcallback_onvaluechange.addItem(func);
	//this should be placed in the call queue after the slider's mouse move callback
}

void HDXUIElement_Slider::unregisterCallbackOnValueChange(int id) {
	mcallback_onvaluechange.removeItem(id);
}

void HDXUIElement_Slider::update(float dt) {
	HDXUIElementBase::update(dt);
	mprevvalue = getValue();

	auto mouseMovement = EngineSettings::Instance().system.input.getMouseMotion();
	if(mouseMovement != MousePoint::Zero) {
		if(mhandle->mdragging) {
			float newvalue = getValue();
			if(newvalue != mprevvalue) {
				mcallback_onvaluechange.doIterate([&](std::function<void(float)> func)->bool {
					func(newvalue);
					return false;
				});
			}
		}
	}
}

void HDXUIElement_Slider::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	mhandle = 0;

	HDXUIElementBase::xml_initialize(xmlscript,xmlnode,func_oninitialize);

	meffect_text = (HDXUIElementEffect_TextDraw*)getEffect("text_main");
	meffect_texture_main = (HDXUIElementEffect_TextureDraw*)getEffect("texture_main");
}

void HDXUIElement_Slider::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementBase::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"texture_handle")==0) {
		string_normal handletexture;
		XMLRead(&handletexture,xmlscript,xmlnode);
		mhandle = createChildElement<HDXUIElement_Slider_Handle>("handle",
																 handletexture,
																 getActionWidth()*0.825f,
																 0.0f,
																 0.0f);
	} else if(strcmp(xmlnode->name(),"value")==0) {
		float value;
		XMLRead(&value,xmlscript,xmlnode);
		setValue(value);
	}
}

bool HDXUIElement_Slider::xml_getVarByName(string_normal *out,const string_normal &name) const {
	if(HDXUIElementBase::xml_getVarByName(out,name)) return true;

	string_normal tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="texture_handle") {
		if(mhandle) return XMLToString(out,mhandle->meffect_texture_main->getTexture(),arg);
	} else if(tok=="value") {
		return XMLToString(out,getValue(),arg);
	}

	return false;
}