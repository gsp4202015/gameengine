#pragma once
#include "gui/hdx9ui.h"

class HDXTexture;
class HDXUIElementEffect_TextureDraw;
class HDXUIElementEffect_TextDraw;

class HDXUIElement_Checkbox : public HDXUIElementBase {
public:
	HDXUIElement_Checkbox(const string_normal &name,
						  HDXUIElementBase *parent,
						  const string_normal &maintexture,
						  const string_normal &hovertexture,
						  const string_normal &checktexture,
						  const string_normal &textfont,
						  const string_normal &text,
						  HDXColor textcolor);
	HDXUIElement_Checkbox(HDXUIElementBase *parent);
	virtual ~HDXUIElement_Checkbox();

	HDXUIElementEffect_TextureDraw* getMainTextureEffect() const;
	HDXUIElementEffect_TextureDraw* getHoverTextureEffect() const;
	HDXUIElementEffect_TextureDraw* getCheckTextureEffect() const;

	HDXUIElementEffect_TextDraw* getTextEffect() const;

	bool getChecked() const;
	void setChecked(bool checked);

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransition);

	virtual void update(float dt);

	virtual string_normal xml_getTypeName() const { return "checkbox"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const;

private:
	HDXUIElementEffect_TextureDraw *meffect_texture_main,*meffect_texture_hover,*meffect_texture_check;
	HDXUIElementEffect_TextDraw *meffect_text;
	bool mchecked;
	int mcallbackid_clicked;
};
