#pragma once

#include "gui/hdx9ui.h"

class HDXTexture;
class HDXUIElementEffect_TextureDraw;
class HDXUIElementEffect_TextDraw;

class HDXUIElement_Button : public HDXUIElementBase {
public:
	HDXUIElement_Button(const string_normal &name,
						HDXUIElementBase *parent,
						const string_normal &maintexture,
						const string_normal &hovertexture,
						const string_normal &textfont,
						const string_normal &text,
						HDXColor textcolor);
	HDXUIElement_Button(HDXUIElementBase *parent);
	virtual ~HDXUIElement_Button();

	HDXUIElementEffect_TextureDraw* getMainTextureEffect() const;
	HDXUIElementEffect_TextureDraw* getHoverTextureEffect() const;

	HDXUIElementEffect_TextDraw* getTextEffect() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransition);

	virtual void update(float dt);
	virtual void render();

	virtual string_normal xml_getTypeName() const { return "button"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);

private:
	HDXUIElementEffect_TextureDraw *meffect_texture_main,*meffect_texture_hover;
	HDXUIElementEffect_TextDraw *meffect_text;
};
