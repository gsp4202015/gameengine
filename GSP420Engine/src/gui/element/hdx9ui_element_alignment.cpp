#include "gui/element/hdx9ui_element_alignment.h"

HDXUIElement_Alignment::HDXUIElement_Alignment(const string_normal name,
											   HDXUIElementBase *parent,
											   DWORD flags,
											   float dirx,
											   float diry,
											   float offx,
											   float offy,
											   float padding) : HDXUIElementBase(name,parent) {
	setFlags(flags);
	setPadding(padding);
	setOffset(offx,offy);
	setDirection(dirx,diry);
	setCurrentPostion(0,0);
}

HDXUIElement_Alignment::HDXUIElement_Alignment(HDXUIElementBase *parent) : HDXUIElementBase(parent) {
}

HDXUIElement_Alignment::~HDXUIElement_Alignment() {
}

Vector2 HDXUIElement_Alignment::getDirection() const {
	return mdir;
}

void HDXUIElement_Alignment::getDirection(Vector2 *dir) const {
	*dir = mdir;
}

void HDXUIElement_Alignment::setDirection(const Vector2 *dir) {
	mdir = *dir;
}

void HDXUIElement_Alignment::setDirection(const float &x,const float &y) {
	mdir.x = x;
	mdir.y = y;
}

Vector2 HDXUIElement_Alignment::getCurrentPostion() const {
	return mcurpos;
}

void HDXUIElement_Alignment::getCurrentPostion(Vector2 *pos) const {
	*pos = mcurpos;
}

void HDXUIElement_Alignment::setCurrentPostion(const Vector2 *pos) {
	mcurpos = *pos;
}

void HDXUIElement_Alignment::setCurrentPostion(const float &x,const float &y) {
	mcurpos.x = x;
	mcurpos.y = y;
}

Vector2 HDXUIElement_Alignment::getNextPostion(HDXUIElementBase *newelement) {
	Vector2 ret;
	getNextPostion(&ret,newelement);
	return ret;
}

void HDXUIElement_Alignment::getNextPostion(Vector2 *pos,HDXUIElementBase *newelement) {
	Vector2 newext(newelement->getActionWidth()/2.0f,newelement->getActionHeight()/2.0f);
	float len = (mdir | newext)*2.0f;
	Vector2 alignment(0,0);

	if(mflags&HDXAL_CENTER) {
		// nothing
	}

	if(mflags&HDXAL_LEFT) {
		alignment.x = newext.x;
	}

	if(mflags&HDXAL_RIGHT) {
		alignment.x = -newext.x;
	}

	if(mflags&HDXAL_VCENTER) {
		// nothing
	}

	if(mflags&HDXAL_TOP) {
		alignment.y = newext.y;
	}

	if(mflags&HDXAL_BOTTOM) {
		alignment.y = -newext.y;
	}

	*pos = mcurpos + moffset + alignment;
	mcurpos += mdir * (len+mpadding);
}

void HDXUIElement_Alignment::jumpDistance(float dst) {
	mcurpos += mdir * dst;
}

DWORD HDXUIElement_Alignment::getFlags() const {
	return mflags;
}

void HDXUIElement_Alignment::setFlags(const DWORD &flags) {
	mflags = flags;
}

Vector2 HDXUIElement_Alignment::getOffset() const {
	return moffset;
}

void HDXUIElement_Alignment::getOffset(Vector2 *pos) const {
	*pos = moffset;
}

void HDXUIElement_Alignment::setOffset(const Vector2 *pos) {
	moffset = *pos;
}

void HDXUIElement_Alignment::setOffset(const float &x,const float &y) {
	moffset.x = x;
	moffset.y = y;
}

float HDXUIElement_Alignment::getPadding() const {
	return mpadding;
}

void HDXUIElement_Alignment::setPadding(const float &padding) {
	mpadding = padding;
}

void HDXUIElement_Alignment::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	setFlags(0);
	setPadding(0);
	setOffset(0,0);
	setDirection(0,1);
	setCurrentPostion(0,0);

	HDXUIElementBase::xml_initialize(xmlscript,xmlnode,func_oninitialize);
}

void HDXUIElement_Alignment::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementBase::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"posflags")==0) {
		XMLRead(&mflags,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"padding")==0) {
		XMLRead(&mpadding,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"offset")==0) {
		XMLRead(&moffset,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"direction")==0) {
		XMLRead(&mdir,xmlscript,xmlnode);
	}
}

bool HDXUIElement_Alignment::xml_getVarByName(string_normal *out,const string_normal &name) const {
	if(HDXUIElementBase::xml_getVarByName(out,name)) return true;

	string_normal tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="padding") {
		return XMLToString(out,mpadding,arg);
	} else if(tok=="offset") {
		return XMLToString(out,moffset,arg);
	} else if(tok=="direction") {
		return XMLToString(out,mdir,arg);
	}

	return false;
}

void HDXUIElement_Alignment::_addChild(HDXUIElementBase *child) {
	HDXUIElementBase::_addChild(child);
	child->setPosition(child->getPosition() + getNextPostion(child));
}