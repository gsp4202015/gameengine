#include "xmlutils.h"

#include <algorithm>
#include <string>

XMLScript::Source::Source(const string_normal &name,XMLScript *parentscript) {
	mname = name;
	mparentscript = parentscript;

	addSource("",this); //for blank tokens
}

XMLScript::Source::~Source() {
}

string_normal XMLScript::Source::dereference(Source *prevsource,const string_normal &entryname,const string_normal &str) {
	string_normal tok,arg;
	XMLGetTokArg(str,&tok,&arg);

	if(msources.count(tok)>0) {
		return msources[tok]->dereference(this,tok,arg);
	}

	if(mvars.count(tok)>0) {
		return mvars[tok](mparentscript,arg);
	}

	return "";
}

void XMLScript::Source::addSource(const string_normal &name,Source *source) {
	assert(msources.count(name)==0);
	msources[name] = source;
}

void XMLScript::Source::addVarAccess(const string_normal &name,FUNC_varAccess func) {
	assert(mvars.count(name)==0);
	mvars[name] = func;
}

XMLScript::XMLScript() {
	createSource<Source>("");
}

XMLScript::~XMLScript() {
	for(auto it = mmastersources.begin(); it != mmastersources.end(); it++) {
		delete it->second;
	}
}

string_normal XMLScript::scriptGetValue(rapidxml::xml_node<> *node) const {
	if(!node) return "";

	rapidxml::xml_node<> *node_t;

	// the node has no scripting
	node_t = node->first_node();
	if(node_t == 0 || strcmp(node_t->name(),"script")!=0) return node->value();

	enum { NUMERIC,STRING } scripttype = NUMERIC;
	{
		rapidxml::xml_attribute<> *attr = node_t->first_attribute("type");
		if(attr) {
			if(strcmp(attr->value(),"string")==0) scripttype = STRING;
		}
	}

	std::function<string_normal(string_normal)> lam_getValue = [&](string_normal str)->string_normal {
		string_normal str_tok_l,str_tok_r,str_op,str_args;

		enum { LTOK,RTOK,DONE } stage = LTOK;

		if(str.length()>0 && str[0]=='(' && str[str.length()-1]==')') {
			str = str.substr(1,str.length()-2);
		}

		int sublevel = 0;
		for(unsigned int i = 0; i < str.length(); i++) {
			char cc = str[i];
			if(scripttype==NUMERIC) {
				if(cc=='(') {
					sublevel++;
				} else if(cc==')') {
					sublevel--;
				}
				switch(cc) {
					case '+':
					case '-':
					case '*':
					case '/':
					case '|':
					case '&':
					case '^': {
						if(sublevel > 0) {
							if(stage==LTOK)
								str_tok_l += cc;
							else
								str_tok_r += cc;
						} else {
							if(stage==LTOK) {
								if(str_tok_l.length()==0 && cc=='-') {
									str_tok_l += cc;
								} else {
									str_op = cc;
									stage = RTOK;
								}
							} else {
								stage = DONE;
								str_args = str.substr(i,str.length()-i);
							}
						}
						break;
					}
					default: {
						if(stage==LTOK)
							str_tok_l += cc;
						else
							str_tok_r += cc;
					}
				}
			} else {
				switch(cc) {
					case 7: {
						if(stage==LTOK) {
							str_op = cc;
							stage = RTOK;
						} else {
							stage = DONE;
							str_args = str.substr(i,str.length()-i);
						}
						break;
					}
					default: {
						if(stage==LTOK)
							str_tok_l += cc;
						else
							str_tok_r += cc;
					}
				}
			}

			if(stage==DONE) break;
		}

		auto lam_math_add = [&](const string_normal &l,const string_normal &r)->string_normal {
			return std::to_string(std::stof(l.c_str()) + std::stof(r.c_str()));
		};

		auto lam_math_subtract = [&](const string_normal &l,const string_normal &r)->string_normal {
			return std::to_string(std::stof(l.c_str()) - std::stof(r.c_str()));
		};

		auto lam_math_multiply = [&](const string_normal &l,const string_normal &r)->string_normal {
			return std::to_string(std::stof(l.c_str()) * std::stof(r.c_str()));
		};

		auto lam_math_divide = [&](const string_normal &l,const string_normal &r)->string_normal {
			return std::to_string(std::stof(l.c_str()) / std::stof(r.c_str()));
		};

		auto lam_bitwise_or = [&](const string_normal &l,const string_normal &r)->string_normal {
			return std::to_string(std::stoull(l.c_str()) | std::stoull(r.c_str()));
		};

		auto lam_bitwise_and = [&](const string_normal &l,const string_normal &r)->string_normal {
			return std::to_string(std::stoull(l.c_str()) & std::stoull(r.c_str()));
		};

		auto lam_bitwise_xor = [&](const string_normal &l,const string_normal &r)->string_normal {
			return std::to_string(std::stoull(l.c_str()) ^ std::stoull(r.c_str()));
		};

		auto lam_concat = [&](const string_normal &l,const string_normal &r)->string_normal {
			return l + r;
		};

		if(str_op.length()>0 && str_tok_r.length()>0) {
			if(str_op=="+") {
				return lam_getValue(lam_math_add(lam_getValue(str_tok_l),lam_getValue(str_tok_r))+str_args);
			} else if(str_op=="-") {
				return lam_getValue(lam_math_subtract(lam_getValue(str_tok_l),lam_getValue(str_tok_r))+str_args);
			} else if(str_op=="*") {
				return lam_getValue(lam_math_multiply(lam_getValue(str_tok_l),lam_getValue(str_tok_r))+str_args);
			} else if(str_op=="/") {
				return lam_getValue(lam_math_divide(lam_getValue(str_tok_l),lam_getValue(str_tok_r))+str_args);
			} else if(str_op=="|") {
				return lam_getValue(lam_bitwise_or(lam_getValue(str_tok_l),lam_getValue(str_tok_r))+str_args);
			} else if(str_op=="&") {
				return lam_getValue(lam_bitwise_and(lam_getValue(str_tok_l),lam_getValue(str_tok_r))+str_args);
			} else if(str_op=="^") {
				return lam_getValue(lam_bitwise_xor(lam_getValue(str_tok_l),lam_getValue(str_tok_r))+str_args);
			} else if(str_op[0]==7) {
				return lam_getValue(lam_concat(lam_getValue(str_tok_l),lam_getValue(str_tok_r))+str_args);
			}
		}

		if(str_tok_l.length()>0) {
			if(str_tok_l[0]=='[') {
				return getSource("")->dereference(0,"",str_tok_l.substr(1,str_tok_l.length()-2));
			}
		}

		return str_tok_l;
	};

	return lam_getValue(node_t->value());
}

XMLScript::Source* XMLScript::getSource(const string_normal &name) const {
	assert(mmastersources.count(name)>0);
	return mmastersources.at(name);
}

void XMLGetTokArg(string_normal str,string_normal *tok,string_normal *arg) {
	*tok = *arg = "";

	auto fit = std::find_if(str.begin(),str.end(),[](char c)->bool {
		switch(c) {
			case '.':
			case '(':
			case ')': {
				return true;
				break;
			}
		}
		return false;
	});

	std::copy(str.begin(),fit,std::back_inserter(*tok));
	std::copy(fit!=str.end() ? fit+1 : fit,str.end(),std::back_inserter(*arg));
}

bool XMLRead(string_normal *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = script->scriptGetValue(node);
	return true;
}

bool XMLRead(bool *out,XMLScript *script,rapidxml::xml_node<> *node) {
	string_normal value = script->scriptGetValue(node);
	*out = !((value.length()==0) || (value=="false") || (value=="0"));
	return true;
}

bool XMLRead(int *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stoi(script->scriptGetValue(node));
	return true;
}

bool XMLRead(unsigned int *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stoul(script->scriptGetValue(node));
	return true;
}

bool XMLRead(long *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stol(script->scriptGetValue(node));
	return true;
}

bool XMLRead(unsigned long *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stoul(script->scriptGetValue(node));
	return true;
}

bool XMLRead(long long *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stoll(script->scriptGetValue(node));
	return true;
}

bool XMLRead(float *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stof(script->scriptGetValue(node));
	return true;
}

bool XMLRead(double *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stod(script->scriptGetValue(node));
	return true;
}

bool XMLRead(long double *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stold(script->scriptGetValue(node));
	return true;
}

template<> bool XMLToString<string_normal>(string_normal *out,const string_normal &in,string_normal arg) {
	*out = in;
	return true;
}

template<> bool XMLToString<bool>(string_normal *out,const bool &in,string_normal arg) {
	if(in) {
		*out = "true";
	} else {
		*out = "false";
	}

	return true;
}