
#include "hdx9color.h"

HDXColor colorAverage(const HDXColor &a,const HDXColor &b) {
	HDXColor ret;
	for(int i = 0; i < 4; i++) {
		((float*)ret)[i] = (((const float*)a)[i] + ((const float*)b)[i]) / 2.0f;
	}
	return ret;
}

HDXColor colorMultiply(const HDXColor &a,const HDXColor &b) {
	HDXColor ret;
	for(int i = 0; i < 4; i++) {
		((float*)ret)[i] = (((const float*)a)[i] * ((const float*)b)[i]);
	}
	return ret;
}
