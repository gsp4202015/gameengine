#include "hdx9intersection.h"

bool HDX9IntersectionTests::box_point(const Vector3 *boxext,const Vector3 *boxpos,const Vector3 *point) {
	Vector3 pt = *point - *boxpos;
	if(abs(pt.x) >(*boxext).x) return false;
	if(abs(pt.y) >(*boxext).y) return false;
	if(abs(pt.z) >(*boxext).z) return false;
	return true;
}

bool HDX9IntersectionTests::box_point(const RECT *box,const POINT *point) {
	if(point->x < box->left ||
	   point->x > box->right ||
	   point->y < box->top ||
	   point->y > box->bottom) {
		return false;
	}
	return true;
}

bool HDX9IntersectionTests::obb_point(const Vector3 *boxext,const Matrix *trans,const Vector3 *point) {
	Vector3 relcenter = *point * trans->Invert();

	if(abs(relcenter.x) >(*boxext).x) return false;
	if(abs(relcenter.y) >(*boxext).y) return false;
	if(abs(relcenter.z) >(*boxext).z) return false;

	return true;
}

bool HDX9IntersectionTests::obb_obb(const Vector3 *boxext0,const Matrix *trans0,const Vector3 *boxext1,const Matrix *trans1) {
	struct OBB {
		const Vector3 *ext;
		const Matrix *trans;
	};

	OBB obb0,obb1;
	obb0.ext = boxext0;
	obb0.trans = trans0;
	obb1.ext = boxext1;
	obb1.trans = trans1;

	auto transformToAxis = [&](OBB &obb,const Vector3 &axis)->float {
		return obb.ext->x * abs(axis | obb.trans->Right()) +
			obb.ext->y * abs(axis | obb.trans->Up()) +
			obb.ext->z * abs(axis | obb.trans->Forward());
	};

	auto overlapOnAxis = [&](OBB &obb0,OBB &obb1,const Vector3 &axis,const Vector3 &tocenter)->float {
		float proj[2];

		proj[0] = transformToAxis(obb0,axis);
		proj[1] = transformToAxis(obb1,axis);
		float distance = abs(tocenter | axis);

		return proj[0] + proj[1] - distance;
	};

	struct TEST {
		Vector3 axis;
		float overlap;
	};

	TEST tests[15] = {
		{obb0.trans->Right(),0},
		{obb0.trans->Up(),0},
		{obb0.trans->Forward(),0},

		{obb1.trans->Right(),0},
		{obb1.trans->Up(),0},
		{obb1.trans->Forward(),0},

		{(Vector3)obb0.trans->Right()%obb1.trans->Right(),0},
		{(Vector3)obb0.trans->Right()%obb1.trans->Up(),0},
		{(Vector3)obb0.trans->Right()%obb1.trans->Forward(),0},
		{(Vector3)obb0.trans->Up()%obb1.trans->Right(),0},
		{(Vector3)obb0.trans->Up()%obb1.trans->Up(),0},
		{(Vector3)obb0.trans->Up()%obb1.trans->Forward(),0},
		{(Vector3)obb0.trans->Forward()%obb1.trans->Right(),0},
		{(Vector3)obb0.trans->Forward()%obb1.trans->Up(),0},
		{(Vector3)obb0.trans->Forward()%obb1.trans->Forward(),0},
	};

	Vector3 tocenter = obb1.trans->Translation() - obb0.trans->Translation();

	int best = 0,bestsingle = 0;
	for(int i = 0; i < 15; i++) {
		if(tests[i].axis.LengthSquared() < 0.0001f) continue;
		tests[i].axis.Normalize();
		tests[i].overlap = overlapOnAxis(obb0,obb1,tests[i].axis,tocenter);
		if(tests[i].overlap < 0) return false; // no collision if there is a separating axis
		if(tests[i].overlap < tests[best].overlap) {
			if(i < 6) bestsingle = i;
			best = i;
		}
	}

	return true;
}