#pragma once

#include "core\EngineTypes.h"

#include <cfloat>
#include <string>
#include <vector>
#include <queue>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include "basemanager.h"
#include "gui/hdx9ui_iuiactor.h"

#include "hdx9color.h"

#include <rapidxml.hpp>
#include <rapidxml_utils.hpp>
#include "gui/hdx9ui_xmlscript.h"

#include "core\Tickable.h"

#define HDXUI_GETZABOVE(z) ((std::min)(1.0f,(z)+FLT_EPSILON))
#define HDXUI_GETZABELOW(z) ((std::max)(0.0f,(z)-FLT_EPSILON))

class HDXUIElementEffect_Base;

#define RECTWIDTH(rect) ((rect).right-(rect).left)
#define RECTHEIGHT(rect) ((rect).bottom-(rect).top)
void rectSetPosSize(RECT *rect,int x,int y,int w,int h);
void rectShrink(RECT *rect,int sx,int sy);
void rectGrow(RECT *rect,int gx,int gy);
void rectGetSubRect(RECT *rect,int sw,int sh,int i,int j);

class HDXUIElementBase : public IHDXUIIActor {
public:
	HDXUIElementBase(HDXUIElementBase *parent);
	HDXUIElementBase(const string_normal name,HDXUIElementBase *parent);
	virtual ~HDXUIElementBase();

	HDXUIElementBase* getParent() const;
	const HDXUIElementBase* getTopElement() const;
	HDXUIElementBase* getTopElement();

	bool isActive() const;
	void setActive(bool active);

	bool isEnabled() const;
	void setEnabled(bool enabled);

	void getTransform(Matrix *mat) const;

	Vector2 getPosition() const;
	Vector2* getPositionPtr();
	void getPosition(Vector2 *pos) const;
	void setPosition(const Vector2 *pos);
	void setPosition(const Vector2 &pos);
	void setPosition(const float &x,const float &y);

	Vector2 getScale() const;
	Vector2* getScalePtr();
	void getScale(Vector2 *scale) const;
	void setScale(const Vector2 *scale);
	void setScale(const Vector2 &scale);
	void setScale(const float &x,const float &y);

	Vector2 getRotationCenter() const;
	void getRotationCenter(Vector2 *center) const;
	void setRotationCenter(const Vector2 *center);
	void setRotationCenter(const Vector2 &center);
	void setRotationCenter(const float &x,const float &y);

	float getRotation() const;
	float* getRotationPtr();
	void setRotation(const float &rot);

	float getActionWidth() const;
	void setActionWidth(const float &actionwidth);

	float getActionHeight() const;
	void setActionHeight(const float &actionheight);

	Vector2 getActionSize() const;
	void setActionSize(const Vector2 &size);
	void setActionSize(const float &w,const float &h);

	void getEffectiveActionRect(RECT *rect) const;

	float getZPos() const;
	void setZPos(const float &zpos); //value must be between 0 and 1 inclusive, default is HDXUI_GETZABOVE(parnet.z), or 0.5 if parent is null
	float getMaxZPos() const; //gets the maximum zpos of this element and all of its children's and effect's zpos's
	float getMinZPos() const; //gets the minimum zpos of this element and all of its children's and effect's zpos's

	HDXColor getBlendColor() const;
	void setBlendColor(HDXColor color);

	bool isMouseOver() const;

	int registerCallbackOnMousePress(std::function<void(WPARAM key)> &&func);
	void unregisterCallbackOnMousePress(int id);

	int registerCallbackOnMouseRelease(std::function<void(WPARAM key,float timeheld)> &&func);
	void unregisterCallbackOnMouseRelease(int id);

	int registerCallbackOnMouseEnter(std::function<void(void)> &&func);
	void unregisterCallbackOnMouseEnter(int id);

	int registerCallbackOnMouseLeave(std::function<void(void)> &&func);
	void unregisterCallbackOnMouseLeave(int id);

	int registerCallbackOnTransitionEnter(std::function<void(bool dotransition)> &&func);
	void unregisterCallbackOnTransitionEnter(int id);

	int registerCallbackOnTransitionExit(std::function<void(bool dotransition)> &&func);
	void unregisterCallbackOnTransitionExit(int id);

	int registerCallbackOnDestroy(std::function<void(void)> &&func);
	void unregisterCallbackOnDestroy(int id);

	const BaseManagerKey<HDXUIElementBase*,string_normal>& getChilderen() const;
	const BaseManagerKey<HDXUIElementEffect_Base*,string_normal>& getEffects() const;

	template<typename T,typename ...Args>
	T* createEffect(const string_normal &name,Args ...args);
	HDXUIElementEffect_Base* getEffect(const string_normal &name) const;
	void destroyEffect(const string_normal &name);

	HDXUIElementEffect_Base* getSubEffect(const string_normal &name) const;

	template<typename T,typename ...Args>
	T* createChildElement(const string_normal &name,Args ...args);
	HDXUIElementBase* createChildElementFromXML(const string_normal &name,const string_normal &file);
	HDXUIElementBase* getChildElement(const string_normal &name) const;
	void destroyChildElement(const string_normal &name);

	//recursively searches for a element below this one in the hierarchy with the specified name
	HDXUIElementBase* getSubElement(const string_normal &name) const;

	virtual float getDesiredTransitionTime() const;
	float getMaxDesiredTransitionTime() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransition);

	virtual void update(float dt);
	virtual void render();

	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(string_normal *out,const string_normal &name) const;

protected:
	virtual void _addChild(HDXUIElementBase *child);
	virtual void _addEffect(HDXUIElementEffect_Base *effect);

private:
	HDXUIElementBase *mparent;
	BaseManagerKey<HDXUIElementBase*,string_normal> mchilderen;
	BaseManagerKey<HDXUIElementEffect_Base*,string_normal> meffects;
	std::vector<HDXUIElementBase*> mreactivate_childeren;
	std::vector<HDXUIElementEffect_Base*> mreactivate_effects;
	std::vector<HDXUIElementBase*> mreenable_childeren;
	std::vector<HDXUIElementEffect_Base*> mreenable_effects;

	bool mactive,menabled;

	BaseManagerID<std::function<void(WPARAM)>> mcallback_onmousepress;
	BaseManagerID<std::function<void(WPARAM,float)>> mcallback_onmouserelease;

	BaseManagerID<std::function<void(void)>> mcallback_ondestroy;

	BaseManagerID<std::function<void(void)>> mcallback_onmouseenter;
	BaseManagerID<std::function<void(void)>> mcallback_onmouseleave;

	BaseManagerID<std::function<void(bool dotransition)>> mcallback_ontransitionenter;
	BaseManagerID<std::function<void(bool dotransition)>> mcallback_ontransitionexit;

	float mactionwidth,mactionheight;
	Vector2 mpos,mscale,mrotcenter;
	float mrot,mzpos;
	bool mmousepressed,mwasmouseover;
	HDXColor mblendcolor;
};

template<typename T,typename ...Args>
T* HDXUIElementBase::createChildElement(const string_normal &name,Args ...args) {
	T *child = new T(name,this,args...);
	_addChild(child);
	return child;
}

template<typename T,typename ...Args>
T* HDXUIElementBase::createEffect(const string_normal &name,Args ...args) {
	T *effect = new T(name,this,args...);
	_addEffect(effect);
	return effect;
}

class HDXUIMenu : public HDXUIElementBase {
	friend class HDXUIMenuMap;
public:
	HDXUIMenu(const string_normal name,HDXUIElementBase *parent = 0);
	HDXUIMenu();
	virtual ~HDXUIMenu();

	HDXUIMenuMap* getMenuMap() const;

	virtual string_normal xml_getTypeName() const { return "menu"; }

private:
	HDXUIMenuMap *mmenumap;
};

class HDXUIMenuMap : public Tickable {
public:
	HDXUIMenuMap();
	~HDXUIMenuMap();

	void createFromXML(const string_normal &fname,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);

	HDXUIMenu* createMenu(const string_normal &name);
	HDXUIMenu* getMenu(const string_normal &name);
	void destroyMenu(const string_normal &name);

	void clear();

	HDXUIMenu* getCurrentMenu();
	void navigateToMenu(const string_normal &menu,bool dotransnition = false);
	void navigateBackwards(bool dotransnition = false);

	void doIterate(std::function<bool(HDXUIMenu *item)> &&func);

	void update(float dt);
	void render();

	// TICKABLE
	virtual void onTick(uint32 groups,engine_duration deltaTime);

private:
	struct _transition {
		string_normal mtarget;
		float mtimer;
		bool menter;
		bool mstarted;
		int mextraframes;

		_transition(const string_normal &target,float timer,bool enter) {
			mtarget = target;
			mtimer = timer;
			menter = enter;
			mstarted = false;
			mextraframes = 10; //extra time is to ensure the transition is complete
		}
	};

	BaseManagerKey<HDXUIMenu*,string_normal> mmenus;
	std::vector<HDXUIMenu*> mtraversal; //allows for a "back" button, the last element is the current active menu
	std::queue<_transition> mtransitions;

	void _setMenu(const string_normal &menu);
};
