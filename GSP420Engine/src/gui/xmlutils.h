#pragma once

#include <unordered_map>
#include <functional>

#include <string>
#include <type_traits>

#include <rapidxml.hpp>
#include <rapidxml_utils.hpp>
#include <rapidxml_print.hpp>
#include <rapidxml_iterators.hpp>

#include "core/EngineTypes.h"

class XMLScript {
public:
	class Source {
	public:
		typedef std::function<string_normal(const XMLScript *script,const string_normal &arg)> FUNC_varAccess;

		Source(const string_normal &name,XMLScript *parentscript);
		virtual ~Source();

		virtual string_normal dereference(Source *prevsource,const string_normal &entryname,const string_normal &str);

		void addSource(const string_normal &name,Source *source);
		void addVarAccess(const string_normal &name,FUNC_varAccess func);

		string_normal mname;
		XMLScript *mparentscript;
		std::unordered_map<string_normal,Source*> msources;
		std::unordered_map<string_normal,FUNC_varAccess> mvars;
	};

	XMLScript();
	XMLScript(const XMLScript&) = delete;
	XMLScript(XMLScript&&) = delete;
	~XMLScript();

	string_normal scriptGetValue(rapidxml::xml_node<> *node) const;

	template<typename T = Source>
	T* createSource(const string_normal &name);
	Source* getSource(const string_normal &name) const;

private:
	std::unordered_map<string_normal,Source*> mmastersources;
};

template<typename T>
T* XMLScript::createSource(const string_normal &name) {
	assert(mmastersources.count(name)==0);
	T *newsource = new T(name,this);
	mmastersources[name] = newsource;
	return newsource;
}

void XMLGetTokArg(string_normal str,string_normal *tok,string_normal *arg);

bool XMLRead(string_normal *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(bool *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(bool *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(int *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(unsigned int *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(long *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(unsigned long *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(long long *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(float *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(double *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(long double *out,XMLScript *script,rapidxml::xml_node<> *node);

template<typename T>
bool XMLToString(string_normal *out,const T &in,string_normal arg = "") {
	*out = std::to_string(in);
	return true;
}

template<> bool XMLToString<string_normal>(string_normal *out,const string_normal &in,string_normal arg);
template<> bool XMLToString<bool>(string_normal *out,const bool &in,string_normal arg);
