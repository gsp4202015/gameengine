#pragma once

#define TRIANGLE_NUMBER(n) ((n*n + n) / 2)

#define NEXT_MULTIPLE_OF_8(n) ((n+7) & ~7)

#define BIT_SET_TRUE(val,bit) val |= (1 << bit)
#define BIT_CLEAR(val,bit) val &= ~(1 << bit)
#define BIT_SET_VALUE(val,bit,value) BIT_CLEAR(val,bit); if(value) BIT_SET_TRUE(val,bit)
#define BIT_TOGGLE(val,bit) val ^= (1 << bit)
#define BIT_IS_SET(val,bit) ((val & (1 << bit)) != 0)

#define SIGN(n) ((n)<0 ? -1:1)

#define CLAMP(n,nummin,nummax) ((n)<(nummin) ? (nummin):((n)>(nummax) ?(nummax):(n)))
#define CLAMPVEC3(n,nummin,nummax) (Vector3(CLAMP((n).x,(nummin).x,(nummax).x),CLAMP((n).y,(nummin).y,(nummax).y),CLAMP((n).z,(nummin).z,(nummax).z)))

#define MAX3(a,b,c) ((a)>(b) ? ((a)>(c) ? (a) : (c)) : ((b)>(c) ? (b) : (c)))
#define VEC3_MAX_SCALAR(v) (MAX3((v).x,(v).y,(v).z))
#define VEC3_MAX_ABS_SCALAR(v) (MAX3(fabs((v).x),fabs((v).y),fabs((v).z)))

#define VEC2TOARG(v) (float)((v).x),(float)((v).y)
#define VEC3TOARG(v) (float)((v).x),(float)((v).y),(float)((v).z)
#define VEC2STREAM(v) "[" << (v).x << ", " << (v).y << "]"
#define VEC3STREAM(v) "[" << (v).x << ", " << (v).y << ", " << (v).z << "]"
#define QUATSTREAM(v) "[" << (v).x << ", " << (v).y << ", " << (v).z << ", " << (v).w << "]"

#define SQ(n) ((n)*(n))

#define FUNCTION_ALIAS(function,alias) \
template <typename... Args> \
auto alias(Args&&... args) -> decltype(function(std::forward<Args>(args)...)) { \
	return function(std::forward<Args>(args)...); \
}

inline unsigned int factorial(unsigned int n) {
	return n > 1 ? n * factorial(n-1) : 1;
}

#define N_CHOOSE_R(n,r) (factorial(n)/(factorial(r)*factorial(n-r)))

//////////////////////////////////////////////////////////////////////////
// FLOATING POINT VALIDITY
#include <limits>
template<typename T>
bool is_infinite(const T &value) {
	T max_value = (std::numeric_limits<T>::max)();
	T min_value = -max_value;
	return !(min_value <= value && value <= max_value);
}
template<typename T>
bool is_nan(const T &value) {
	// True if NAN
	return value != value;
}
template<typename T>
bool is_valid(const T &value) {
	return !is_infinite(value) && !is_nan(value);
}
