#include "EngineSettings.h"
#include "EngineTypes.h"

#include "JSON.h"

#include "..\physics\material\PhysicalMaterial.h"

void EngineSettings::_physics::_loadPhysicalMaterials() {
	try {
		json materialFile = json::parse_file("data/assets/json/physicalmaterials.json");

		for(uint32 i = 0; i < materialFile.size(); i++) {
			const engine_string materialName = string_from_normal(materialFile[i]["name"].as<string_normal>());
			PhysicalMaterial *newMat = constructObjectNameless<PhysicalMaterial>(nullptr,materialFile[i]);
			_physicalMaterials[materialName] = newMat;
		}
	} catch(const std::exception &e) {
		std::cerr << e.what() << '\n';
	}
}

PhysicalMaterial* EngineSettings::_physics::getPhysicalMaterial(const engine_string &name) const {
	auto foundIt = _physicalMaterials.find(name);
	assert(foundIt != _physicalMaterials.end());
	return foundIt->second;
}
