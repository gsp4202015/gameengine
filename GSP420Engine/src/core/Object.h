#pragma once

#include <cassert>
#include <typeinfo>
#include <type_traits>
#include <functional>
#include <string>
#include <vector>
#include <queue>
#include <atomic>

#include "pool_vector.h"

#include "utility\shared_mutex.h"

#include "EngineTypes.h"

class Object;

template<typename T,typename ...Args>
T* constructObject(Object *owner,const engine_string &name,Args ...args);
template<typename T,typename ...Args>
T* constructObjectNameless(Object *owner,Args ...args);

class Object {
	template<typename T,typename ...Args>
	friend T* constructObject(Object *owner,const engine_string &name,Args ...args);

	//////////////////////////////////////////////////////////////////////////
	// STATIC
private:
	static ting::shared_mutex __mtxStaticContainers;
	static pool_vector<Object*> __objectRegistry;
	static std::queue<Object*> __gcQueue;
	static uint32 __nextUniqueID;

	static void __registerObject(Object *obj);
	static void __unregisterObject(Object *obj);


public:
	static void doGarbageCollection();
	static std::vector<Object*> filterObjectsByPredicate(std::function<bool(const Object *obj)> &&pred);
	static Object* findObjectByName(const engine_string &name);
	static Object* findObjectByFullName(const engine_string &fullName);
	static Object* findObjectByID(uint32 id);

	//////////////////////////////////////////////////////////////////////////
	// NON - STATIC
private:
	uint32 _uniqueID;
	Object *_owner;
	engine_string _name;
	pool_vector<Object*> _subObjects;
	const std::type_info *_typeInfo;
	std::atomic<bool> _isPendingDestruction;

	void _registerSubObject(Object *obj);
	void _unregisterSubObject(Object *obj);

protected:
	virtual bool isReadyForDestruction() const;

	// events are fired BEFORE the sub objects are registered and unregistered.
	virtual void onSubObjectRegistering(Object *obj) {};
	virtual void onSubObjectUnregistering(Object *obj) {};

public:
	Object();
	virtual ~Object();

	const std::type_info* getTypeInfo() const;

	uint32 getUniqueID() const;
	Object* getOwner() const;
	const engine_string& getName() const;
	engine_string getFullName() const;
	void setName(const engine_string &name);

	bool isPendingDestruction() const;
	void destroy();

	bool isValid() const;

	std::vector<Object*> filterSubObjectsByPredicate(std::function<bool(const Object *obj)> &&pred);
	Object* findSubObjectByName(const engine_string &name) const;
	Object* findSubObjectByID(uint32 id) const;
};

template<typename T,typename ...Args>
T* constructObject(Object *owner,const engine_string &name,Args ...args) {
	static_assert(std::is_base_of<Object,T>::value,"T must derive from Object");
	//assert((is_base_of<class World,T> || owner != nullptr)); // only World doesn't require a parent

	T *newObject = new T(args...);

	newObject->_owner = owner;
	newObject->_name = name;

	newObject->_typeInfo = &typeid(T);

	if(owner != nullptr) {
		owner->_registerSubObject(newObject);
	}

	return newObject;
}

template<typename T,typename ...Args>
T* constructObjectNameless(Object *owner,Args ...args) {
	T *newObject = constructObject<T,Args...>(owner,STR(""),args...);

	newObject->setName(newObject->getName() + STR("_") + to_string(newObject->getUniqueID()));

	return newObject;
}
