#pragma once

#include "EngineTypes.h"

#include "utility\shared_mutex.h"
#include "pool_vector.h"

namespace TickGroup {
	enum _ : uint32 {
		NONE = 1<<0,

		ENTITY = 1<<1,

		UI = 1<<2,

		PRE_PHYSICS = 1<<3,
		POST_PHYSICS = 1<<4,

		PRE_GRAPHICS = 1<<5,
		POST_GRAPHICS = 1<<6,
	};
}

class Tickable {
	//////////////////////////////////////////////////////////////////////////
	// STATIC
private:
	static ting::shared_mutex __mtx_tickableRegistry;
	static pool_vector<Tickable*> __tickableRegistry;

	static void __registerTickableObject(Tickable* object);
	static void __unregisterTickableObject(Tickable* object);

public:
	static void tickGroups(uint32 groups,engine_duration deltaTime);

	//////////////////////////////////////////////////////////////////////////
	// NON - STATIC
public:
	uint32 tickGroup;

	Tickable();
	virtual ~Tickable();

	virtual void onTick(uint32 groups,engine_duration deltaTime) = 0;
};
