#include "ActorComponent.h"

#include "Actor.h"

#include <algorithm>

ActorComponent::ActorComponent() {
	absolutePosition.x = absolutePosition.y = absolutePosition.z =
		absoluteRotation.x = absoluteRotation.y = absoluteRotation.z =
		absoluteScale.x = absoluteScale.y = absoluteScale.z = false;
}

ActorComponent::~ActorComponent() {
	detach();
}

void ActorComponent::onComponentAttached(ActorComponent* comp) {
	componentsAttachedToMe.insert(comp);
}

void ActorComponent::onComponentDetached(ActorComponent* comp) {
	componentsAttachedToMe.remove(comp);
}

Actor* ActorComponent::getActor() const {
	Actor *ownerAsActor = dynamic_cast<Actor*>(getOwner());
	assert(ownerAsActor != nullptr);
	return ownerAsActor;
}

bool ActorComponent::isRoot() const {
	return this == getActor()->rootComponent;
}

Matrix ActorComponent::getComponentToWorld() const {
	assert(!(isRoot() && (componentAttachedTo != nullptr))); // cannot be the root and be attached to another component

	if(isRoot()) {
		return transform.getMatrix();
	} else {
		Matrix workingRootMat;

		if(componentAttachedTo == nullptr) {
			// not attached to another component, use the root
			workingRootMat = getActor()->rootComponent->getComponentToWorld();
		} else {
			// we are attached to another component, our transform must be relative to it
			workingRootMat = componentAttachedTo->getComponentToWorld();
		}

		if(absolutePosition != BoolVector3::FFF ||
		   absoluteRotation != BoolVector3::FFF ||
		   absoluteScale != BoolVector3::FFF) {
			Transform workingRootTransform = workingRootMat;

			if(absolutePosition.x) workingRootTransform.position.x = 0.0f;
			if(absolutePosition.y) workingRootTransform.position.y = 0.0f;
			if(absolutePosition.z) workingRootTransform.position.z = 0.0f;


			if(absoluteRotation.x || absoluteRotation.y || absoluteRotation.z) {
				if(absoluteRotation != BoolVector3::TTT) {
					Vector3 pivotDirection;
					if(!absoluteRotation.x)
						pivotDirection = Vector3::UnitX;
					else if(!absoluteRotation.y)
						pivotDirection = Vector3::UnitY;
					else
						pivotDirection = Vector3::UnitZ;

					Vector3 dir = Vector3::Transform(pivotDirection,workingRootTransform.rotation);

					if(absoluteRotation.x) dir.x = 0;
					if(absoluteRotation.y) dir.y = 0;
					if(absoluteRotation.z) dir.z = 0;

					workingRootTransform.rotation = rotateTo(dir,pivotDirection);
				} else {
					workingRootTransform.rotation = Quaternion::Identity;
				}
			}

			if(absoluteScale.x) workingRootTransform.scale.x = 1.0f;
			if(absoluteScale.y) workingRootTransform.scale.y = 1.0f;
			if(absoluteScale.z) workingRootTransform.scale.z = 1.0f;

			workingRootMat = workingRootTransform.getMatrix();
		}

		const Matrix myMatrix = transform.getMatrix();

		// myScale -> myRotation -> myTranslation -> rootScale -> rootRotation -> rootTranslation
		return myMatrix * workingRootMat;
	}
}

void ActorComponent::attachTo(ActorComponent *targetComponent) {
	assert(!isRoot());

	if(componentAttachedTo != nullptr) {
		componentAttachedTo->onComponentDetached(this);
	}
	componentAttachedTo = targetComponent;
	componentAttachedTo->onComponentAttached(this);
}

void ActorComponent::detach() {
	if(componentAttachedTo != nullptr) {
		componentAttachedTo->onComponentDetached(this);
	}
	componentAttachedTo = nullptr;
}

void ActorComponent::onContactBegin(ColliderComponent *myCollider,ColliderComponent *otherCollider) {
}

void ActorComponent::onContactSustained(ColliderComponent *myCollider,ColliderComponent *otherCollider) {
}

void ActorComponent::onContactEnd(ColliderComponent *myCollider,ColliderComponent *otherCollider) {
}