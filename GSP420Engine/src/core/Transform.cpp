#include "Transform.h"

#include <utility>

Transform::Transform() :
position(Vector3::Zero),
rotation(Quaternion::Identity),
scale(1,1,1) {

}

Transform::Transform(const Transform &other) :
position(other.position),
rotation(other.rotation),
scale(other.scale) {

}

Transform::Transform(Transform &&other) :
position(std::move(other.position)),
rotation(std::move(other.rotation)),
scale(std::move(other.scale)) {

}

Transform::Transform(Matrix &matrix) {
	matrix.Decompose(scale,rotation,position);
}

Transform& Transform::operator=(const Transform &other) {
	position = other.position;
	rotation = other.rotation;
	scale = other.scale;
	return *this;
}

Transform& Transform::operator=(Transform &&other) {
	position = std::move(other.position);
	rotation = std::move(other.rotation);
	scale = std::move(other.scale);
	return *this;
}

Matrix Transform::getMatrix() const {
	if(position != _cache_getMatrix_position ||
	   rotation != _cache_getMatrix_rotation ||
	   scale != _cache_getMatrix_scale) {
		_cache_getMatrix_position = position;
		_cache_getMatrix_rotation = rotation;
		_cache_getMatrix_scale = scale;
		_cache_getMatrix_ret = Matrix::CreateScale(scale) * Matrix::CreateFromQuaternion(rotation.GetNormal()) * Matrix::CreateTranslation(position);
	}
	return _cache_getMatrix_ret;
}

Matrix Transform::getMatrixNoScale() const {
	return Matrix::CreateFromQuaternion(rotation) * Matrix::CreateTranslation(position);
}
