#pragma once

#include "Object.h"
#include "Transform.h"

#include <vector>

class ActorComponent;

class Actor : public Object {
	friend class ActorComponent;
	friend class World;

private:
	pool_vector<ActorComponent*> _components;
	ActorComponent* _defaultRoot;

protected:
	virtual void onSubObjectRegistering(Object *obj);
	virtual void onSubObjectUnregistering(Object *obj);

public:
	ActorComponent *rootComponent;

	class RigidBodyComponent *rigidBody;
	class AIComponent *AI;
	Actor();
	virtual ~Actor();

	World* getWorld();

	void setDefaultRoot();
};
