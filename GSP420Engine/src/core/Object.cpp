#include "Object.h"

#include <algorithm>

//////////////////////////////////////////////////////////////////////////
// STATIC
uint32 Object::__nextUniqueID = 0;
ting::shared_mutex Object::__mtxStaticContainers;
pool_vector<Object*> Object::__objectRegistry;
std::queue<Object*> Object::__gcQueue;

void Object::__registerObject(Object *obj) {
	std::lock_guard<decltype(__mtxStaticContainers)> lck(__mtxStaticContainers);
	__objectRegistry.insert(obj);
}

void Object::__unregisterObject(Object *obj) {
	std::lock_guard<decltype(__mtxStaticContainers)> lck(__mtxStaticContainers);
	__objectRegistry.remove(obj);
}

void Object::doGarbageCollection() {
	ting::shared_lock<decltype(__mtxStaticContainers)> lck(__mtxStaticContainers);
	for(auto obj : __objectRegistry) if(obj != nullptr) {
		if(obj->isPendingDestruction() && obj->isReadyForDestruction()) {
			__unregisterObject(obj);
			delete obj;
		}
	}
}

std::vector<Object*> Object::filterObjectsByPredicate(std::function<bool(const Object *obj)> &&pred) {
	ting::shared_lock<decltype(__mtxStaticContainers)> lck(__mtxStaticContainers);
	std::vector<Object*> result;
	for(auto &obj : __objectRegistry) if(obj != nullptr) {
		if(obj->isValid() && pred(obj)) result.push_back(obj);
	}
	return std::move(result);
}

Object* Object::findObjectByName(const engine_string &name) {
	ting::shared_lock<decltype(__mtxStaticContainers)> lck(__mtxStaticContainers);
	for(auto &obj : __objectRegistry) if(obj != nullptr) {
		if(obj->isValid() && obj->getName() == name) return obj;
	}
	return nullptr;
}

Object* Object::findObjectByFullName(const engine_string &fullName) {
	ting::shared_lock<decltype(__mtxStaticContainers)> lck(__mtxStaticContainers);
	for(auto &obj : __objectRegistry) if(obj != nullptr) {
		if(obj->isValid() && obj->getFullName() == fullName) return obj;
	}
	return nullptr;
}

Object* Object::findObjectByID(uint32 id) {
	ting::shared_lock<decltype(__mtxStaticContainers)> lck(__mtxStaticContainers);
	for(auto &obj : __objectRegistry) if(obj != nullptr) {
		if(obj->isValid() && obj->getUniqueID() == id) return obj;
	}
	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
// NON - STATIC
void Object::_registerSubObject(Object *obj) {
	assert(obj != nullptr);
	assert(isValid());
	assert(obj->isValid());
	onSubObjectRegistering(obj);
	obj->_owner = this;
	_subObjects.insert(obj);
}

void Object::_unregisterSubObject(Object *obj) {
	assert(obj != nullptr);
	assert(isValid());
	assert(obj->isValid());
	onSubObjectUnregistering(obj);
	_subObjects.remove(obj);
	obj->_owner = nullptr;
}

bool Object::isReadyForDestruction() const {
	// only destroy this object if all of its sub-objects have already been destroyed
	return _subObjects.size() == 0;
}

Object::Object() {
	__registerObject(this);

	_uniqueID = __nextUniqueID++;

	_isPendingDestruction = false;
	_owner = nullptr;
	_name = STR("");
}

Object::~Object() {
	__unregisterObject(this);
}

const std::type_info* Object::getTypeInfo() const {
	return _typeInfo;
}

uint32 Object::getUniqueID() const {
	assert(isValid());
	return _uniqueID;
}

Object* Object::getOwner() const {
	assert(isValid());
	return _owner;
}

const engine_string& Object::getName() const {
	assert(isValid());
	return _name;
}

engine_string Object::getFullName() const {
	assert(isValid());
	engine_string fullName = getName();

	Object *curOwner = getOwner();
	while(curOwner != nullptr) {
		fullName = curOwner->getName() + STR("\\") + fullName;
		curOwner = curOwner->_owner;
	}

	return fullName;
}

void Object::setName(const engine_string &name) {
	_name = name;
}

bool Object::isPendingDestruction() const {
	return _isPendingDestruction;
}

void Object::destroy() {
	assert(isValid());

	_isPendingDestruction = true;

	// sub-objects must be destroyed first, so recursively add them to the destruction queue
	for(auto &subObj : _subObjects) {
		subObj->destroy();
	}
	__gcQueue.push(this);
}	

bool Object::isValid() const {
	return !isPendingDestruction();
}

std::vector<Object*> Object::filterSubObjectsByPredicate(std::function<bool(const Object *obj)> &&pred) {
	std::vector<Object*> result;
	for(auto &subObject : _subObjects) if(subObject != nullptr) {
		if(subObject->isValid() && pred(subObject)) result.push_back(subObject);
	}
	return std::move(result);
}

Object* Object::findSubObjectByName(const engine_string &name) const {
	assert(isValid());
	for(auto subObject : _subObjects) {
		if(subObject->isValid() && subObject->getName() == name) return subObject;
	}
	return nullptr;
}

Object* Object::findSubObjectByID(uint32 id) const {
	assert(isValid());
	for(auto subObject : _subObjects) {
		if(subObject->isValid() && subObject->getUniqueID() == id) return subObject;
	}
	return nullptr;
}
