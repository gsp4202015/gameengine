#include "Actor.h"
#include "ActorComponent.h"
#include "physics\RigidBodyComponent.h"
#include "..\AI\AIComponent.h"
#include <algorithm>

Actor::Actor() {
	rigidBody = nullptr;
	_defaultRoot = constructObject<ActorComponent>(this,STR("DefaultRoot"));
	setDefaultRoot();
}

Actor::~Actor() {

}

void Actor::onSubObjectRegistering(Object *obj) {
	// maintain the cache of actor components
	ActorComponent *obj_as_ActorComponent = dynamic_cast<ActorComponent*>(obj);
	if(obj_as_ActorComponent != nullptr) {
		_components.insert(obj_as_ActorComponent);
	}

	RigidBodyComponent* obj_as_RigidBodyComponent = dynamic_cast<RigidBodyComponent*>(obj);
	if(obj_as_RigidBodyComponent != nullptr) {
		assert(rigidBody == nullptr); // cannot have multiple rigid bodies in a single actor
		rigidBody = obj_as_RigidBodyComponent;
		rootComponent = rigidBody;
	}

	AIComponent *tempAIComponent = dynamic_cast<AIComponent*>(obj);
	if(tempAIComponent != nullptr)	{
		assert(AI == nullptr);	//cannot have multiple AIComponent in a sigle actor
		AI = tempAIComponent;
	}
}

void Actor::onSubObjectUnregistering(Object *obj) {
	// maintain the cache of actor components
	ActorComponent *obj_as_ActorComponent = dynamic_cast<ActorComponent*>(obj);
	if(obj_as_ActorComponent != nullptr) {
		_components.remove(obj_as_ActorComponent);
	}

	RigidBodyComponent* obj_as_RigidBodyComponent = dynamic_cast<RigidBodyComponent*>(obj);
	if(obj_as_RigidBodyComponent != nullptr) {
		rigidBody = nullptr;
		setDefaultRoot();
	}
}

void Actor::setDefaultRoot() {
	rootComponent = _defaultRoot;
}

World* Actor::getWorld() {
	return (World*)getOwner();
}
