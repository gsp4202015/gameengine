#include "Tickable.h"

//////////////////////////////////////////////////////////////////////////
// STATIC
pool_vector<Tickable*> Tickable::__tickableRegistry;
ting::shared_mutex Tickable::__mtx_tickableRegistry;

void Tickable::__registerTickableObject(Tickable* object) {
	std::lock_guard<decltype(__mtx_tickableRegistry)> lck(__mtx_tickableRegistry);
	__tickableRegistry.emplace(object);
}

void Tickable::__unregisterTickableObject(Tickable* object) {
	std::lock_guard<decltype(__mtx_tickableRegistry)> lck(__mtx_tickableRegistry);
	__tickableRegistry.remove(object);
}

void Tickable::tickGroups(uint32 groups,engine_duration deltaTime) {
	ting::shared_lock<decltype(__mtx_tickableRegistry)> lck(__mtx_tickableRegistry);
	for(auto &tickable : __tickableRegistry) {
		if(tickable->tickGroup != TickGroup::NONE) {
			const uint32 matchingGroups = tickable->tickGroup & groups;
			if(matchingGroups == tickable->tickGroup) {
				tickable->onTick(matchingGroups,deltaTime);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// NON - STATIC
Tickable::Tickable() {
	tickGroup = TickGroup::NONE;

	__registerTickableObject(this);
}

Tickable::~Tickable() {
	__unregisterTickableObject(this);
}
