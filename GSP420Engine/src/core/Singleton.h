// code from: http://www.codeproject.com/Articles/4750/Singleton-Pattern-A-review-and-analysis-of-existin

#pragma once

#include <mutex>

namespace singleton {
	template<typename T>
	class DefaultLifetime {
	protected:
		inline explicit DefaultLifetime() {}
		inline ~DefaultLifetime() {}

		inline static void OnDeadReference() {
			throw std::logic_error("Dead Reference Detected");
		}
		inline static void ScheduleForDestruction(void(*pFun)()) {
			std::atexit(pFun);
		}

	private:
		inline explicit DefaultLifetime(DefaultLifetime const&) {}
		inline DefaultLifetime& operator=(DefaultLifetime const&) { return *this; }
	};

	template<typename T>
	class CreateUsingNew {
	protected:
		inline explicit CreateUsingNew() {}
		inline ~CreateUsingNew() {}

		inline static T* CreateInstance() { return new T(); }
		inline static void DestroyInstance(T* t) { delete t; }

	private:
		inline explicit CreateUsingNew(CreateUsingNew const&) {}
		inline CreateUsingNew& operator=(CreateUsingNew const&) { return *this; }
	};

	template<typename T,typename CreationPolicy = CreateUsingNew<T>,template <typename> class LifetimePolicy = DefaultLifetime>
	class Singleton : public CreationPolicy,public LifetimePolicy<T> {
	public:
		static T& Instance();
		static void Destroy();

	protected:
		inline explicit Singleton() {
			assert(Singleton::instance_ == 0);
			Singleton::instance_ = static_cast<T*>(this);
			Singleton::destroyed_ = false;
		}
		virtual ~Singleton() {
			Singleton::instance_ = 0;
			Singleton::destroyed_ = true;
		}

	private:
		static T* instance_;
		static bool destroyed_;
		static std::mutex mtx_instance_;

	private:
		inline explicit Singleton(Singleton const&) {}
		inline Singleton& operator=(Singleton const&) { return *this; }
	};    //    end of class Singleton

	template<typename T,typename C,template <typename> class L>
	typename T& Singleton<T,C,L>::Instance() {
		if(Singleton::instance_ == 0) {
			std::lock_guard<std::mutex> lock(mtx_instance_);
			if(Singleton::instance_ == 0) {
				if(Singleton::destroyed_) {
					OnDeadReference();
					Singleton::destroyed_ = false;
				}
				Singleton::instance_ = CreateInstance();
				try {
					ScheduleForDestruction(Singleton::Destroy);
				} catch(...) {
					DestroyInstance(Singleton::instance_);
				}
			}
		}
		return *(Singleton::instance_);
	}

	template<typename T,typename C,template <typename> class L>
	void Singleton<T,C,L>::Destroy() {
		if(Singleton::instance_ != 0) {
			std::lock_guard<std::mutex> lock(mtx_instance_);
			if(Singleton::instance_ != 0) {
				DestroyInstance(Singleton::instance_);
				Singleton::instance_ = 0;
				Singleton::destroyed_ = true;
			}
		}
	}

	template<typename T,typename C,template <typename> class L>
	T* Singleton<T,C,L>::instance_ = 0;
	template<typename T,typename C,template <typename> class L>
	bool Singleton<T,C,L>::destroyed_ = false;
	template<typename T,typename C,template <typename> class L>
	std::mutex Singleton<T,C,L>::mtx_instance_;
} // end namespace singleton