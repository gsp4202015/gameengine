#pragma once

#include "EngineTypes.h"
#include "Singleton.h"

#include <vector>
#include <array>

namespace CachedVariableDomains {
	enum _ : uint32 {
		ACTORCOMPONENT_TRANSFORM,

		NUM_DOMAINS,
		ALL
	};
}

namespace CachedVariableDomainModes {
	enum _ : uint32 {
		COMPARE_COUNTERS = 0,
		ALWAYS_CLEAN,
		ALWAYS_DIRTY,

		NUM_DOMAIN_MODES
	};
}

class CachedVariableCustodian : public singleton::Singleton<CachedVariableCustodian> {
	template<typename T,uint32 DOMAIN>
	friend class CachedVariable;

private:
	std::array<uint32,CachedVariableDomains::NUM_DOMAINS> cleanCounters;
	std::array<uint8,CachedVariableDomains::NUM_DOMAINS> domainModes;

public:
	CachedVariableCustodian() {
		for(auto &cleanCounter : cleanCounters) cleanCounter = 0;
		for(auto &domainMode : domainModes) domainMode = CachedVariableDomainModes::COMPARE_COUNTERS;
	}

	virtual ~CachedVariableCustodian() {

	}

	inline void setDomainDirty(uint32 domainID) {
		assert(domainID < cleanCounters.size() || domainID == CachedVariableDomains::ALL);
		if(domainID != CachedVariableDomains::ALL)
			cleanCounters[domainID]++;
		else
			for(auto &cleanCounter : cleanCounters) cleanCounter++;
	}

	inline void setDomainsDirty(const std::vector<uint32> &domainIDs) {
		for(const auto &domainID : domainIDs) {
			setDomainDirty(domainID);
		}
	}

	inline uint8 getDomainMode(uint32 domainID) {
		assert(domainID < cleanCounters.size());
		return domainModes[domainID];
	}

	// returns the previous value
	inline uint8 setDomainMode(uint32 domainID,uint8 mode) {
		assert(domainID < cleanCounters.size());
		assert(mode < CachedVariableDomainModes::NUM_DOMAIN_MODES);
		uint8 ret = getDomainMode(domainID);
		domainModes[domainID] = mode;
		return ret;
	}
};

template<typename T,uint32 DOMAIN>
class CachedVariable {
	static_assert(DOMAIN < CachedVariableDomains::NUM_DOMAINS,"DOMAIN must be less than CachedVariableDomains::NUM_DOMAINS.");

private:
	T _data;
	uint32 _lastCleanCounter;

public:
	CachedVariable() { setDirty(); }
	CachedVariable(const T &other) : _data(other) { setClean(); }
	CachedVariable(T &&other) : _data(std::move(other)) { setClean(); }

	inline bool isDirty() const {
		switch(CachedVariableCustodian::Instance().getDomainMode(DOMAIN)) {
			case CachedVariableDomainModes::COMPARE_COUNTERS:
				return _lastCleanCounter == CachedVariableCustodian::Instance().cleanCounters[DOMAIN];
			case CachedVariableDomainModes::ALWAYS_CLEAN:
				return false;
			case CachedVariableDomainModes::ALWAYS_DIRTY:
				return true;
		}
		return true;
	}

	inline void setClean() {
		_lastCleanCounter = CachedVariableCustodian::Instance().cleanCounters[DOMAIN];
	}

	inline void setDirty() {
		_lastCleanCounter = CachedVariableCustodian::Instance().cleanCounters[DOMAIN]-1;
	}

	inline void setCleanData(const T &data) {
		_data = data;
		setClean();
	}

	inline void setCleanData(T &&data) {
		_data = std::move(data);
		setClean();
	}

	inline T& operator*() {
		return _data;
	}

	inline const T& operator*() const {
		return _data;
	}

	inline T& get() {
		return _data;
	}

	inline const T& get() const {
		return _data;
	}

	inline 
};
