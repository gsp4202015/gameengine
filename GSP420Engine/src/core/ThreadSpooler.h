#pragma once

#include "EngineTypes.h"

#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <future>
#include <condition_variable>

#include "utility/shared_mutex.h"

#include <vector>
#include <array>

class ThreadTask;
class ThreadTaskReference;
class ThreadTaskGraph;
class WorkerThread;
class ThreadSpooler;

namespace ThreadTaskWorkState {
	enum _ : uint8 {
		AWAITING_SELECTION = 0,
		BEING_SELECTED,
		AWAITING_PROCESSING,
		PROCESSING,
		DONE
	};
}

//////////////////////////////////////////////////////////////////////////
// ThreadTaskReference
//////////////////////////////////////////////////////////////////////////
class ThreadTaskReference {
	friend ThreadTask;

private:
	ThreadTaskGraph* threadTaskGraph;
	uint32 taskID;

	ThreadTaskReference(ThreadTaskGraph* threadTaskGraph,uint32 taskID);

public:
	ThreadTaskReference();
	ThreadTaskReference(const ThreadTaskReference&) = default;
	ThreadTaskReference& operator=(const ThreadTaskReference&) = default;

	operator bool();

	ThreadTask* operator->();
	const ThreadTask* operator->() const;
};

//////////////////////////////////////////////////////////////////////////
// ThreadTask
//////////////////////////////////////////////////////////////////////////
class ThreadTask {
	friend ThreadTaskReference;
	friend ThreadTaskGraph;
	friend WorkerThread;

private:
	ThreadTaskGraph* myTaskGraph;

	std::promise<void> prom_completion;

	std::atomic<uint16> workRemaining;

	uint32 myID;

	uint32 parentID;
	std::array<uint32,5> dependencies;
	uint8 dependencyNum;

	std::atomic<uint8> workState;

	ThreadTaskReference getReference();
	const ThreadTaskReference getReference() const;

public:
	std::atomic<bool> fullyInitialized;
	uint32 priority;
	uint32 affinity;
	std::function<void(ThreadTaskReference thisTask)> workFunction;

	ThreadTask(uint32 id,ThreadTaskGraph* taskGraph);
	ThreadTask(const ThreadTask&) = delete;
	ThreadTask& operator=(const ThreadTask&) = delete;
	ThreadTask(ThreadTask &&l);

	void setDependencies(const std::vector<ThreadTaskReference> &tasks);

	ThreadTaskReference createSubTask();

	std::future<void> getCompletionFuture();
};

//////////////////////////////////////////////////////////////////////////
// ThreadTaskGraph
//////////////////////////////////////////////////////////////////////////
class ThreadTaskGraph {
	friend ThreadTask;
	friend WorkerThread;
	friend ThreadTaskReference;
	friend ThreadSpooler;

private:
	mutable ting::shared_mutex mtx_taskGraph;
	std::vector<ThreadTask> taskGraph;

	std::atomic<uint8> numUsers;

	ThreadTaskGraph();

	ThreadTaskReference createLooseTask();
	uint32 selectNextTask(int32 callingThreadAffinity);

	uint32 getNumTasks() const;
	void reserveSlots(uint32 numSlots);

	void acquire();
	void release();

public:
	ThreadTaskReference getTaskReference(uint32 taskID);
	ThreadTaskReference getRootTaskReference();
};

//////////////////////////////////////////////////////////////////////////
// WorkerThread
//////////////////////////////////////////////////////////////////////////
class WorkerThread {
	friend ThreadSpooler;

private:
	std::atomic<bool> threadRunning;
	std::atomic<bool> threadInterrupt;

	std::promise<void> prom_thredExit;

	ThreadSpooler* spooler;

	ThreadTaskGraph* targetTaskGraph;

	uint32 currentTaskID;
	std::thread myThread;

	WorkerThread(ThreadSpooler* spooler);
	~WorkerThread();

	void threadMain();

public:
	std::atomic<bool> fullyInitialized;
	uint32 affinity;

	void startThread();
	std::future<void> stopThread();
	bool isThreadRunning() const;
};

//////////////////////////////////////////////////////////////////////////
// ThreadSpooler
//////////////////////////////////////////////////////////////////////////
class ThreadSpooler {
	friend WorkerThread;

private:
	std::vector<WorkerThread*> workerThreads;
	mutable std::mutex mtx_workerThreads;

	std::atomic<ThreadTaskGraph*> currentTaskGraph;

	std::array<uint32,10> taskGraphSizeOverTime;
	uint32 taskGraphSizeOverTime_insertInd;

	WorkerThread* createLooseWorkerThread();

public:
	ThreadSpooler();
	~ThreadSpooler();

	// creates worker threads equal to the number of detected processing cores minus the number of explicit threads already in existence
	// if the number of processing cores cannot be determined the fall back number will be used instead.
	uint32 createImplicitWorkerThreads(uint32 fallbackNum = 1);
	WorkerThread* createExplicitWorkerThread();

	std::future<void> stopAllWorkers();
	void startAllWorkers();

	uint32 getNumQualifiedWorkers(uint32 affinity = 0x0) const;
	uint32 getOptiomalNumConcurrentTasks(uint32 affinity = 0x0) const;

	// WARNING: Tasks may still be running in other threads. These threads will continue until exiting naturally.
	void clearTasks();

	ThreadTaskReference getRootTask() const;
};