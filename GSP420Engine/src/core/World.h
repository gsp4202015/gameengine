#pragma once

#include "ThreadSpooler.h"

#include "Object.h"

#include "physics\Contact.h"

#include <vector>
#include "pool_vector.h"

class Actor;
class Transform;
class ThreadTask;

class World : public Object {
private:
	pool_vector<Actor*> _actors;
	engine_time_point _tpLastFrameTime;

	engine_duration _physicsTimeToSimulate;

	bool _isPaused;

	class CameraComponent* _activeCamera;
	class APlayer* _activePlayer;

	class PhysicsIntegrator* _physicsIntegrator;
	class ContactGenerator* _contactGenerator;
	class ContactResolver* _contactResolver;

protected:
	virtual void onSubObjectRegistering(Object *obj);
	virtual void onSubObjectUnregistering(Object *obj);

	void updateAI(ThreadTaskReference thisTask);
	void updateActors(ThreadTaskReference thisTask);
	void updateSpacialPartitions(ThreadTaskReference thisTask);
	void updatePhysics(ThreadTaskReference thisTask);

public:
	engine_duration gameFrameTimeDelta;
	engine_duration physicsFrameTimeDelta;
	engine_duration gameTime;
	engine_duration physicsTime;

	ting::shared_mutex mtx_contacts;
	pool_vector<Contact> contacts;

	void threadProc(ThreadTaskReference thisTask);

	World();
	virtual ~World();

	void pause();
	void unPause();

	bool isPaused() const;

	const pool_vector<Actor*>& getActors() const;

	PhysicsIntegrator* getPhysicsIntegrator() const;
	ContactGenerator* getContactGenerator() const;
	ContactResolver* getContactResolver() const;

	void setActiveCamera(CameraComponent* camera);
	CameraComponent* getActiveCamera() const;

	void setActivePlayer(APlayer* player);
	APlayer* getActivePlayer() const;

	template<typename T>
	void setPhysicsIntegrator();
	template<typename T>
	void setContactGenerator();
	template<typename T>
	void setContactResolver();
};

template<typename T>
void World::setPhysicsIntegrator() {
	static_assert(std::is_base_of<PhysicsIntegrator,T>::value,"T must derive from PhysicsIntegrator");
	if(_physicsIntegrator != nullptr) _physicsIntegrator->destroy();
	_physicsIntegrator = constructObjectNameless<T>(this);
}

template<typename T>
void World::setContactGenerator() {
	static_assert(std::is_base_of<ContactGenerator,T>::value,"T must derive from ContactGenerator");
	if(_contactGenerator != nullptr) _contactGenerator->destroy();
	_contactGenerator = constructObjectNameless<T>(this);
}

template<typename T>
void World::setContactResolver() {
	static_assert(std::is_base_of<ContactResolver,T>::value,"T must derive from ContactResolver");
	if(_contactResolver != nullptr) _contactResolver->destroy();
	_contactResolver = constructObjectNameless<T>(this);
}
