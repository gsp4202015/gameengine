#include "EngineMath.h"

#include "Utility.h"

// OPERATORS

// DOT PRODUCT
float operator|(const Vector3 &l,const Vector3 &r) {
	return l.Dot(r);
}

float operator|(const Vector2 &l,const Vector2 &r) {
	return l.Dot(r);
}

// CROSS PRODUCT
Vector3 operator%(const Vector3 &l,const Vector3 &r) {
	return r.Cross(l);
}

// TRANSFORM VECTOR BY MATRIX
Vector3 operator*(const Vector3 &l,const Matrix &r) {
	return Vector3::Transform(l,r);
}

Vector2 operator*(const Vector2 &l,const Matrix &r) {
	return Vector2::Transform(l,r);
}

namespace MathUtilities {
	// FUNCTIONS
	Vector3 getNormal(const Vector3 &v) {
		Vector3 ret;
		v.Normalize(ret);
		return ret;
	}

	void makeOrthoNormalBasis(Vector3 *x,Vector3 *y,Vector3 *z) {
		x->Normalize();
		(*z) = (*x) % (abs(x->x) > abs(x->y) ? Vector3::UnitY : Vector3::UnitX);
		if(z->LengthSquared() == 0.0) return; // Or generate an error.
		z->Normalize();
		(*y) = (*z) % (*x);
	}

	void orthoNormalize(Matrix *m) {
		Vector3 right = m->Right();
		Vector3 up = m->Up();
		Vector3 forward = m->Forward();

		makeOrthoNormalBasis(&right,&up,&forward);

		m->Right(right);
		m->Up(up);
		m->Forward(forward);
	}

	DirectX::SimpleMath::Quaternion quatAddAngularOffset(const Quaternion &quat,const Vector3 &offset) {
		Quaternion eulerQuat(-offset,1.0f);
		eulerQuat = quat * eulerQuat;
		eulerQuat *= 0.5f;
		return quat + eulerQuat;
	}

	// 	DirectX::SimpleMath::Quaternion quatAddEuler(const Quaternion &quat,const Vector3 &euler) {
	// 		Quaternion eulerQuat(euler,1.0f);
	// 		eulerQuat = quat * eulerQuat;
	// 		eulerQuat *= 0.5f;
	// 		return quat + eulerQuat;
	// 	}

	void barycentric(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c,float *u,float *v,float *w) {
		// code from Crister Erickson's Real-Time Collision Detection
		Vector3 v0 = b - a,v1 = c - a,v2 = p - a;
		float d00 = v0|v0;
		float d01 = v0|v1;
		float d11 = v1|v1;
		float d20 = v2|v0;
		float d21 = v2|v1;
		float denom = d00 * d11 - d01 * d01;
		*v = (d11 * d20 - d01 * d21) / denom;
		*w = (d00 * d21 - d01 * d20) / denom;
		*u = 1.0f - *v - *w;
	}

	float pointDistanceFromLine(const Vector3 &p,const Vector3 &a,const Vector3 &b) {
		return ((b-a)%(a-p)).Length() / (b-a).Length();
	}

	float pointDistanceFromTriangle(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c) {
		const Vector3 plane_n = getNormal((b-a)%(c-a));
		const float plane_d = plane_n | a;

		Vector3 p_ontrianle = p - (plane_n * ((plane_n|p) - plane_d));

		float tri_u,tri_v,tri_w;
		barycentric(p_ontrianle,a,b,c,&tri_u,&tri_v,&tri_w);
		tri_u = CLAMP(tri_u,0,1);
		tri_v = CLAMP(tri_v,0,1);
		tri_w = CLAMP(tri_w,0,1);

		p_ontrianle = p_ontrianle*tri_u + p_ontrianle*tri_v + p_ontrianle*tri_w;

		return (p_ontrianle-p).Length() * SIGN(plane_n | ((p+plane_n)*-plane_d));
	}

	float pointDistanceFromPlane(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c) {
		const Vector3 n = getNormal((b-a)%(c-a));
		return pointDistanceFromPlane(p,n,a);
	}

	float pointDistanceFromPlane(const Vector3 &p,const Vector3 &n,const Vector3 &a) {
		const float d = n | a;
		return pointDistanceFromPlane(p,n,d);
	}

	float pointDistanceFromPlane(const Vector3 &p,const Vector3 &n,float d) {
		return (n|p)-d;
	}

	Vector3 perpendicular(const Vector3 &perpendicularTo) {
		if(abs(perpendicularTo | Vector3::UnitY) > 0.9f) {
			return perpendicularTo % Vector3::UnitX;
		}
		return perpendicularTo % Vector3::UnitY;
	}

	Quaternion rotateTo(const Vector3 &q,const Vector3 &p) {
		Quaternion ret;

		float dot = p | q;
		if(dot <= -0.9995f) {
			ret = Quaternion::CreateFromAxisAngle(Vector3::UnitY,-PI);//left handed, remove negation for right handed
		} else if(dot >= 0.9995f) {
			ret = Quaternion::Identity;
		} else {
			Quaternion tmp = p % q; //left handed, swap q and p for right handed
			ret = Quaternion(tmp.x,tmp.y,tmp.z,1+dot).GetNormal();
		}

		return ret;
	}

	Quaternion nullifyRotationOnAxis(const Quaternion &quat,const Vector3 &axis) {
		// algorithm from: http://www.ogre3d.org/forums/viewtopic.php?f=5&t=76926
		const Vector3 p1 = perpendicular(axis);
		const Vector3 p2 = Vector3::Transform(p1,quat);
		const Vector3 p3 = (p2 - (axis * (p2 | axis))).GetNormal(); // project p2 onto the plane with the normal "axis"
		const float angle = getAngleBetween(p1,p3,axis,true);
		const Quaternion z = Quaternion::CreateFromAxisAngle(axis,angle);
		const Quaternion w = z * quat;
		return w;
	}

	float getAngleBetween(const Vector3 &dir1,const Vector3 &dir2,const Vector3 &norm /*= Vector3::Zero*/,bool signedAngle /*= false*/) {
		// algorithm from: http://www.ogre3d.org/forums/viewtopic.php?f=5&t=76926
		const float dot = CLAMP(dir1 | dir2,-1.0f,1.0f);
		float angle = acos(dot);

		if(signedAngle) {
			assert(norm.LengthSquared() > 0.001f);
			const Vector3 cross = dir1 % dir2;
			const float sign = norm | cross;
			if(sign < 0) angle *= -1.0f;
		}

		return angle;
	}

	Matrix skewSymmetric(const Vector3 &vector) {
		const Vector3 &v = vector;
		return Matrix(
			0,-v.z,v.y,0,
			v.z,0,-v.x,0,
			-v.y,v.x,0,0,
			0,0,0,1
			);
	}

	Matrix lookDirection(const Vector3 &localforward,const Vector3 &targetdir,const Vector3 &localup /*= Vector3::Up*/,const Vector3 &worldup /*= Vector3::Up*/) {
		const Vector3 tdir = targetdir.GetNormal();
		const Vector3 localright = (localup % localforward).GetNormal();
		const Vector3 worldright = (fabs(worldup | tdir)<0.99f ? (worldup % tdir) : (Vector3::UnitX)).GetNormal();
		const Vector3 perpworldup = (tdir % worldright).GetNormal();

		Matrix m1;
		m1.Right(worldright);
		m1.Up(perpworldup);
		m1.Forward(tdir);
		Matrix m2;
		m2.Right(localright);
		m2.Up(localup);
		m2.Forward(localforward);
		m2 = m2 * m1;

		// 	m2[Matrix::ij(0,3)] = -(m2.getColumn(0)*eye);
		// 	m2[Matrix::ij(1,3)] = -(m2.getColumn(1)*eye);
		// 	m2[Matrix::ij(2,3)] = -(m2.getColumn(2)*eye);

		return m2;
	}

	Matrix removeScale(const Matrix &mat) {
		const Vector3 right = mat.Right().GetNormal();
		const Vector3 up = mat.Up().GetNormal();
		const Vector3 forward = mat.Forward().GetNormal();
		const Vector3 translation = mat.Translation();
		return Matrix(VEC3TOARG(right),0,
					  VEC3TOARG(up),0,
					  VEC3TOARG(forward),0,
					  VEC3TOARG(translation),0);
	}

	Vector3 quatToEuler(const Quaternion &quat) {
		Vector3 ret;

		const float &w = quat.w;
		const float &x = quat.x;
		const float &y = quat.y;
		const float &z = quat.z;

		float r11,r21,r31,r32,r33;
		float q00,q11,q22,q33;

		q00 = w*w;
		q11 = x*x;
		q22 = y*y;
		q33 = z*z;

		r11 = q00 + q11 - q22 - q33;
		r21 = 2 * (x*y + w*z);
		r31 = 2 * (x*z - w*y);
		r32 = 2 * (y*z + w*x);
		r33 = q00 - q11 - q22 + q33;

		float absr31 = abs(r31);
		if(absr31 > 0.9999) { // gimbal lock has occurred, try to compensate
			float r12 = 2 * (x*y - w*z);
			float r13 = 2 * (x*z + w*y);

			ret.x = 0;
			ret.y = -((float)(PI/2))*r31/absr31;
			ret.z = atan2(-r12,-r31*r31);
		} else {
			ret.x = atan2(r32,r33);
			ret.y = asin(-r31);
			ret.z = atan2(r21,r11);
		}

		return ret;
	}

	Quaternion quatFromEuler(const Vector3 &euler) {
		return Quaternion::CreateFromYawPitchRoll(euler.y,euler.x,euler.z);
	}
}