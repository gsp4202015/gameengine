#pragma once

#include "EngineTypes.h"
#include "Utility.h"

#include <cassert>
#include <array>
#include <vector>

class CollisionMatrix {
	const static uint8 NUM_COLLISION_LAYERS = 4;

	// get the n-th triangle number, which represents how many values will be required to represent
	// NUM_COLLISION_LAYERS collision layers. then round it up to the nearest 8, representing how many
	// total bits will be in the final memory block. then divide by 8 to convert it to bytes.
	std::array<uint8,NEXT_MULTIPLE_OF_8(TRIANGLE_NUMBER(NUM_COLLISION_LAYERS)) / 8> _collisionLayers;
	std::vector<engine_string> _collisionLayerNames;

	inline static void _getCollisionLayersIndByteBit(uint8 layerA,uint8 layerB,uint8 *byte,uint8 *bit) {
		uint8 layerSmall,layerLarge;
		if(layerA < layerB) {
			layerSmall = layerA;
			layerLarge = layerB;
		} else {
			layerSmall = layerB;
			layerLarge = layerA;
		}

		const uint8 targetInd = layerSmall * (NUM_COLLISION_LAYERS - layerSmall) + layerLarge;

		*byte = targetInd / 8;
		*bit = targetInd % 8;
	}

public:
	CollisionMatrix() :
		_collisionLayerNames(NUM_COLLISION_LAYERS) {
		// initialize all collision layers to true
		for(auto &collisionLayerData : _collisionLayers) {
			collisionLayerData = 0xff;
		}
	}

	// get the index of a collision layer by its name
	inline uint8 getCollisionLayerByName(const engine_string &name) const {
		for(uint8 i = 0; i < NUM_COLLISION_LAYERS; i++) {
			if(_collisionLayerNames[i] == name) return i;
		}
		assert(false);
		return -1;
	}

	// set the name of a collision layer
	inline void setCollisionLayerName(uint8 layer,const engine_string &name) {
		_collisionLayerNames[layer] = name;
	}

	// check if collision between the two given layers is enabled
	inline bool isCollisionEnabled(uint8 layerA,uint8 layerB) const {
		assert(layerA < NUM_COLLISION_LAYERS && layerB < NUM_COLLISION_LAYERS);

		uint8 targetByte,targetBit;
		_getCollisionLayersIndByteBit(layerA,layerB,&targetByte,&targetBit);

		return BIT_IS_SET(_collisionLayers[targetByte],targetBit);
	}

	// set if collision between the two given layers is enabled
	inline void setCollisionEnabled(uint8 layerA,uint8 layerB,bool enabled) {
		assert(layerA < NUM_COLLISION_LAYERS && layerB < NUM_COLLISION_LAYERS);

		uint8 targetByte,targetBit;
		_getCollisionLayersIndByteBit(layerA,layerB,&targetByte,&targetBit);

		BIT_SET_VALUE(_collisionLayers[targetByte],targetBit,enabled);
	}
};