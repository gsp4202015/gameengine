#include "ThreadSpooler.h"

#include <algorithm>
#include <chrono>
#include <numeric>

//////////////////////////////////////////////////////////////////////////
// ThreadTaskReference
//////////////////////////////////////////////////////////////////////////
ThreadTaskReference::ThreadTaskReference(ThreadTaskGraph* threadTaskGraph,uint32 taskID) :
threadTaskGraph(threadTaskGraph),
taskID(taskID) {
}

ThreadTaskReference::ThreadTaskReference() :
threadTaskGraph(nullptr),
taskID(-1) {
}

ThreadTask* ThreadTaskReference::operator->() {
	assert(threadTaskGraph != nullptr);
	assert(taskID < threadTaskGraph->taskGraph.size());

	// ensure that the task graph is not being modified while retrieving the task
	ting::shared_lock<decltype(ThreadTaskGraph::mtx_taskGraph)> lck_taskGraph_tasks(threadTaskGraph->mtx_taskGraph);
	auto taskToReturn = &threadTaskGraph->taskGraph[taskID];
	return taskToReturn;
}

const ThreadTask* ThreadTaskReference::operator->() const {
	assert(threadTaskGraph != nullptr);
	assert(taskID < threadTaskGraph->taskGraph.size());

	// ensure that the task graph is not being modified while retrieving the task
	ting::shared_lock<decltype(ThreadTaskGraph::mtx_taskGraph)> lck_taskGraph_tasks(threadTaskGraph->mtx_taskGraph);
	auto taskToReturn = &threadTaskGraph->taskGraph[taskID];
	return taskToReturn;
}

ThreadTaskReference::operator bool() {
	return threadTaskGraph != nullptr && taskID < threadTaskGraph->taskGraph.size();
}

//////////////////////////////////////////////////////////////////////////
// ThreadTask
//////////////////////////////////////////////////////////////////////////
ThreadTask::ThreadTask(uint32 id,ThreadTaskGraph* taskGraph) :
myTaskGraph(taskGraph),
prom_completion(),
workRemaining(1),
myID(id),
parentID(-1),
dependencies(),
dependencyNum(0),
workState(ThreadTaskWorkState::AWAITING_SELECTION),
fullyInitialized(false),
priority(0),
affinity(0),
workFunction(nullptr) {
}

ThreadTask::ThreadTask(ThreadTask &&l) :
myTaskGraph(std::move(l.myTaskGraph)),
prom_completion(std::move(l.prom_completion)),
workRemaining((uint16)l.workRemaining),
myID(std::move(l.myID)),
parentID(std::move(l.parentID)),
dependencies(std::move(l.dependencies)),
dependencyNum(std::move(l.dependencyNum)),
workState((uint8)l.workState),
fullyInitialized((bool)std::move(l.fullyInitialized)),
priority(std::move(l.priority)),
affinity(std::move(l.affinity)),
workFunction(std::move(l.workFunction)) {
}

ThreadTaskReference ThreadTask::getReference() {
	return ThreadTaskReference(myTaskGraph,myID);
}

const ThreadTaskReference ThreadTask::getReference() const {
	return ThreadTaskReference(myTaskGraph,myID);
}

void ThreadTask::setDependencies(const std::vector<ThreadTaskReference> &tasks) {
	assert(tasks.size() <= dependencies.size());
	std::vector<uint32> taskIDs;
	taskIDs.resize(tasks.size());
	for(uint32 i = 0; i < tasks.size(); i++) {
		dependencies[i] = tasks[i]->myID;
	}
	dependencyNum = tasks.size();
}

ThreadTaskReference ThreadTask::createSubTask() {
	workRemaining += 1;

	auto cache_myID = myID;
	auto cache_affinity = affinity;

	// ThreadTaskGraph::createLooseThread() CAN AND WILL INVALIDATE THE THIS POINTER
	// MUST USE VARIABLES CACHED ON STACK
	auto newTask = myTaskGraph->createLooseTask();
	newTask->parentID = cache_myID;
	newTask->affinity = cache_affinity;
	return newTask->getReference();
}

std::future<void> ThreadTask::getCompletionFuture() {
	return prom_completion.get_future();
}

//////////////////////////////////////////////////////////////////////////
// ThreadTaskGraph
//////////////////////////////////////////////////////////////////////////
ThreadTaskGraph::ThreadTaskGraph() :
numUsers(0) {
}

void ThreadTaskGraph::acquire() {
	numUsers++;
}

void ThreadTaskGraph::release() {
	// are we the last user?
	if(numUsers.fetch_sub(1) == 1) {
		// notify all uncompleted tasks they completed
		for(auto &task : taskGraph) {
			if(task.workRemaining != 0) {
				task.prom_completion.set_value();
			}
		}

		delete this;
	}
}

ThreadTaskReference ThreadTaskGraph::createLooseTask() {
	auto lam_addTask = [this](uint32 id) {
		taskGraph.emplace_back(id,this);
		taskGraph.back().parentID = -1;
	};

	// get a shared lock to the tasks and check if creating a task will require a reallocation
	mtx_taskGraph.lock_shared();

	const uint32 newID = taskGraph.size();
	if(newID == taskGraph.capacity()) {
		// reallocation is required, release the shared lock and acquire a unique lock
		mtx_taskGraph.unlock_shared();
		mtx_taskGraph.lock();

		lam_addTask(newID);

		mtx_taskGraph.unlock();
		mtx_taskGraph.lock_shared();
	} else {
		lam_addTask(newID);
	}

	auto returnRef = taskGraph.back().getReference();
	mtx_taskGraph.unlock_shared();
	return returnRef;
}

uint32 ThreadTaskGraph::selectNextTask(int32 callingThreadAffinity) {
	ting::shared_lock<decltype(mtx_taskGraph)> lck_taskGraph(mtx_taskGraph);

	if(taskGraph.size() == 0) return -1;

	// find the eligible task with the highest priority
	uint32 bestTaskID = -1;
	for(uint32 i = 0; i < taskGraph.size(); i++) [&]() {
		auto &curTask = taskGraph[i];
		if(!curTask.fullyInitialized) return;
		if(curTask.workRemaining == 0) return;
		if(curTask.workState != ThreadTaskWorkState::AWAITING_SELECTION) return;
		if((curTask.affinity & callingThreadAffinity) != curTask.affinity) return;

		if(curTask.parentID != -1) {
			const auto &curTaskParent = taskGraph[curTask.parentID];
			if(!curTaskParent.fullyInitialized) return;
			if(curTaskParent.workState != ThreadTaskWorkState::DONE) return;
		}

		for(uint32 j = 0; j < curTask.dependencyNum; j++) {
			const auto &curTaskDependency = taskGraph[curTask.dependencies[j]];
			if(!curTaskDependency.fullyInitialized) return;
			if(curTaskDependency.workRemaining > 0) return;
		}

		if(bestTaskID == -1 || curTask.priority > taskGraph[bestTaskID].priority) {
			// ensure that we are the only ones to select this task
			uint8 expected = ThreadTaskWorkState::AWAITING_SELECTION;
			if(curTask.workState.compare_exchange_strong(expected,ThreadTaskWorkState::BEING_SELECTED)) {
				if(bestTaskID != -1) taskGraph[bestTaskID].workState = expected;
				bestTaskID = i;
			}
		}
	}();

	if(bestTaskID != -1) {
		taskGraph[bestTaskID].workState = ThreadTaskWorkState::AWAITING_PROCESSING;
	}

	return bestTaskID;
}

ThreadTaskReference ThreadTaskGraph::getTaskReference(uint32 taskID) {
	ting::shared_lock<decltype(mtx_taskGraph)> lck_taskGraph(mtx_taskGraph);
	assert(taskID < taskGraph.size());
	return taskGraph[taskID].getReference();
}

ThreadTaskReference ThreadTaskGraph::getRootTaskReference() {
	if(taskGraph.size() == 0) return createLooseTask()->getReference();
	return taskGraph[0].getReference();
}

uint32 ThreadTaskGraph::getNumTasks() const {
	ting::shared_lock<decltype(mtx_taskGraph)> lck_taskGraph(mtx_taskGraph);
	return taskGraph.size();
}

void ThreadTaskGraph::reserveSlots(uint32 numSlots) {
	std::lock_guard<decltype(mtx_taskGraph)> lck_taskGraph(mtx_taskGraph);
	taskGraph.reserve(numSlots);
}

//////////////////////////////////////////////////////////////////////////
// WorkerThread
//////////////////////////////////////////////////////////////////////////
WorkerThread::WorkerThread(ThreadSpooler* spooler) :
fullyInitialized(false),
threadRunning(false),
threadInterrupt(false),
spooler(spooler),
targetTaskGraph(nullptr) {
}

WorkerThread::~WorkerThread() {
	// DO NOT DESTROY THREAD OBJECT WHILE THE THREAD IS RUNNING
	auto fut_stopThread = stopThread();
	fut_stopThread.get();
}

void WorkerThread::threadMain() {
	threadRunning = true;
	prom_thredExit.swap(std::promise<void>());

	static const auto aWhile = std::chrono::nanoseconds(50);

	while(!threadInterrupt) {
		if(!fullyInitialized ||
		   targetTaskGraph == nullptr) {
			std::this_thread::sleep_for(aWhile);
		}

		// ensure the task graph we are using is the most current one
		// if there is a new graph, release the old graph and acquire the new one
		; {
			ThreadTaskGraph* currentGraph = spooler->currentTaskGraph.load();
			if(targetTaskGraph != currentGraph) {
				if(targetTaskGraph != nullptr) targetTaskGraph->release();
				targetTaskGraph = currentGraph;
				if(targetTaskGraph != nullptr) targetTaskGraph->acquire();
			}
		}

		// get the next valid task and execute it
		if((currentTaskID = targetTaskGraph->selectNextTask(affinity)) != -1) {
			auto curTask = targetTaskGraph->getTaskReference(currentTaskID);

			curTask->workState = ThreadTaskWorkState::PROCESSING;

			// call the work function
			if(curTask->workFunction != nullptr) try {
				curTask->workFunction(curTask);
			} catch(...) {
				assert(false); // ERROR IN USER CODE!
			}

			// recursively notify tasks of their completion
			const std::function<void(uint32)> lam_notifyTaskOfCompletion = [&](uint32 taskID)->void {
				auto &curNotifyTask = targetTaskGraph->getTaskReference(taskID);

				if(curNotifyTask->workRemaining.fetch_sub(1) == 1) {
					curNotifyTask->prom_completion.set_value();
					if(curNotifyTask->parentID != -1) lam_notifyTaskOfCompletion(curNotifyTask->parentID);
				}
			};

			curTask->workState = ThreadTaskWorkState::DONE;

			lam_notifyTaskOfCompletion(currentTaskID);
		} else {
			// there is nothing to do right now, wait a while before trying again
			std::this_thread::sleep_for(aWhile);
		}

		// allow other threads to execute
		std::this_thread::yield();
	}

	threadRunning = false;
	prom_thredExit.set_value();
}

void WorkerThread::startThread() {
	assert(!threadRunning);
	myThread = std::thread(std::bind(&WorkerThread::threadMain,this));
}

std::future<void> WorkerThread::stopThread() {
	assert(threadRunning);
	threadInterrupt = true;
	return prom_thredExit.get_future();
}

bool WorkerThread::isThreadRunning() const {
	return threadRunning;
}

//////////////////////////////////////////////////////////////////////////
// ThreadSpooler
//////////////////////////////////////////////////////////////////////////
ThreadSpooler::ThreadSpooler() :
currentTaskGraph(nullptr) {
	taskGraphSizeOverTime_insertInd = 0;
	for(auto &item : taskGraphSizeOverTime) {
		item = 0;
	}
	clearTasks();
}

ThreadSpooler::~ThreadSpooler() {
	if(currentTaskGraph.load() != nullptr) delete currentTaskGraph.load();
}

WorkerThread* ThreadSpooler::createLooseWorkerThread() {
	return new WorkerThread(this);
}

uint32 ThreadSpooler::createImplicitWorkerThreads(uint32 fallbackNum /*= 1*/) {
	uint32 processorsAvailable = std::thread::hardware_concurrency();

	// zero indicates an unknown number of cores, use fall back
	if(processorsAvailable == 0) {
		processorsAvailable = fallbackNum;
	}

	std::lock_guard<decltype(mtx_workerThreads)> lck(mtx_workerThreads);

	uint32 threadsToMake = std::max(0,(signed int)(processorsAvailable - workerThreads.size()));

	for(uint32 i = 0; i < threadsToMake; i++) {
		workerThreads.emplace_back(createLooseWorkerThread());
		workerThreads.back()->fullyInitialized = true;
		workerThreads.back()->startThread();
	}

	return threadsToMake;
}

WorkerThread* ThreadSpooler::createExplicitWorkerThread() {
	std::lock_guard<decltype(mtx_workerThreads)> lck(mtx_workerThreads);
	workerThreads.emplace_back(createLooseWorkerThread());
	workerThreads.back()->startThread();
	return workerThreads.back();
}

std::future<void> ThreadSpooler::stopAllWorkers() {
	return std::async(std::launch::async,[this]() {
		std::unique_lock<decltype(mtx_workerThreads)> lck_explicitWorkerThreads(mtx_workerThreads);

		std::vector<std::future<void>> ftr_threadsStopped;

		// signal all workers to stop
		for(uint32 i = 0; i < workerThreads.size(); i++) ftr_threadsStopped.emplace_back(workerThreads[i]->stopThread());
		// wait until they have all stopped
		for(uint32 i = 0; i < workerThreads.size(); i++) ftr_threadsStopped[i].get();
	});
}

void ThreadSpooler::startAllWorkers() {
	for(uint32 i = 0; i < workerThreads.size(); i++) workerThreads[i]->startThread();
}

uint32 ThreadSpooler::getNumQualifiedWorkers(uint32 affinity) const {
	std::unique_lock<decltype(mtx_workerThreads)> lck_explicitWorkerThreads(mtx_workerThreads);

	unsigned numQualifiedWorkers = 0;

	// test all threads against the supplied affinity
	for(auto const &thread : workerThreads) {
		if((affinity & thread->affinity) == affinity) numQualifiedWorkers++;
	}

	return numQualifiedWorkers;
}

uint32 ThreadSpooler::getOptiomalNumConcurrentTasks(uint32 affinity) const {
	return (uint32)std::ceil(getNumQualifiedWorkers(affinity) * 1.5f);
}

void ThreadSpooler::clearTasks() {
	// create a new task graph within a mutex, the old one will be destroyed when the last object using it calls its release function
	ThreadTaskGraph* graphNew = new ThreadTaskGraph;
	graphNew->acquire();
	ThreadTaskGraph* graphOld = currentTaskGraph.exchange(graphNew);
	if(graphOld != nullptr) {
		taskGraphSizeOverTime[taskGraphSizeOverTime_insertInd++ % taskGraphSizeOverTime.size()] = graphOld->getNumTasks();
		graphOld->release();
	}

	const uint32 averageNumTasksOverLastFewFrames = std::accumulate(taskGraphSizeOverTime.begin(),taskGraphSizeOverTime.end(),0,std::plus<uint32>()) / taskGraphSizeOverTime.size();
	graphNew->reserveSlots(averageNumTasksOverLastFewFrames);
}

ThreadTaskReference ThreadSpooler::getRootTask() const {
	return (*currentTaskGraph).getRootTaskReference();
}