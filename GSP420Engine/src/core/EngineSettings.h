#pragma once

#include "EngineMath.h"

#include "Singleton.h"

#include <vector>
#include <array>
#include <unordered_map>

#include "graphics\DX11\DirectX11.h"

#include "CollisionMatrix.h"
#include "ThreadSpooler.h"
#include "input\Input.h"

namespace ThreadFlags {
	enum _ : uint32 {
		ENTITY_UPDATE = 0,
		COM_GRAPHICS = 1<<1,
		COM_WINDOW = 1<<2,
	};
}

class EngineSettings : public singleton::Singleton<EngineSettings> {
public:
	struct {
		ThreadSpooler threadSpooler;
		Input input;
		class DXApp* application;
		class World* activeWorld;
		class HDXUIMenuMap* activeMenuMap;
	} system;

	struct _physics {
		friend EngineSettings;

	private:
		std::unordered_map<engine_string,class PhysicalMaterial*> _physicalMaterials;

		void _loadPhysicalMaterials();

	public:
		float fixedTimeStep;
		CollisionMatrix collisionMatrix;
		float collisionSkinWidth;
		float minimumVelocityForRestitution;
		Vector3 gravity;

		struct {
			float linear;
			float angular;
		} damping;

		struct {
			uint16 maximumVelocityIterations;
			uint16 maximumPenetrationIterations;

			float epsilon_penetration;
			float epsilon_velocity;
		} solver;

		PhysicalMaterial* getPhysicalMaterial(const engine_string &name) const;
	} physics;

	struct {
		float timeScale;
		float maximumTimeStep;

		engine_time_point tpLastFrameTime;
		engine_duration timeDeltaLastFrame;
	} time;

	struct _graphics {
		class TextureManager* textureManager;
		class MeshManager* meshManager;
		class ShaderManager* shaderManager;

		struct ID3D11DeviceContext* deviceContext;
		struct ID3D11Device* device;
		
		class DirectX::SpriteBatch* spriteBatch;
		class DirectX::SpriteFont* spriteFont;
	} graphics;

	EngineSettings() {
		system.application = nullptr;

		physics.fixedTimeStep = 1.0f / 50.0f;

		physics.collisionMatrix.setCollisionLayerName(0,STR("default"));
		physics.collisionMatrix.setCollisionLayerName(1,STR("terrain"));
		physics.collisionMatrix.setCollisionLayerName(2,STR("player"));
		physics.collisionMatrix.setCollisionLayerName(3,STR("enemy"));

		physics.minimumVelocityForRestitution = 0.5f;
		physics.collisionSkinWidth = 0.0f;
		//physics.collisionSkinWidth = 0.1f; disabled for now, before re-enabling fix or dismiss possible issues with the collision point being off
		physics.gravity = Vector3::Down * 60.0f;

		physics.damping.linear = 0.985f;
		physics.damping.angular = 0.96f;

		physics.solver.maximumVelocityIterations = 10;
		physics.solver.maximumPenetrationIterations = 10;
		physics.solver.epsilon_penetration = 0.01f;
		physics.solver.epsilon_velocity = 0.025f;

		physics._loadPhysicalMaterials();

		time.timeScale = 1.0f;
		time.maximumTimeStep = 0.3f;
	}
};
