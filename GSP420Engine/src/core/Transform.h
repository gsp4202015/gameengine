#pragma once

#include "EngineMath.h"

class Transform {
private:
	mutable Matrix _cache_getMatrix_ret;
	mutable Vector3 _cache_getMatrix_position;
	mutable Quaternion _cache_getMatrix_rotation;
	mutable Vector3 _cache_getMatrix_scale;

public:
	Vector3 position;
	Quaternion rotation;
	Vector3 scale;

	Transform();
	Transform(const Transform &other);
	Transform(Matrix &matrix);
	Transform(Transform &&other);

	Transform& operator=(const Transform &other);
	Transform& operator=(Transform &&other);

	Matrix getMatrix() const;
	Matrix getMatrixNoScale() const;
};