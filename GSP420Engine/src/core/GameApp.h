#pragma once

#include "ThreadSpooler.h"

#include "graphics\Graphics.h"
#include "gui\GUI.h"
#include "sound\soundManager.h"

#include <atomic>
#include <memory>

class GameApp : public DXApp
{
private:
	std::atomic<bool> isGameRunning;

	class DirectShow *mShow;
	class Frustrum *mFrustrum;
	class GameHUD *mHUD;
	class World *mWorld;

	class APlayer* playerActor;
	class MouseLookCameraComponent* freeFlyCamera;

	std::unique_ptr<HDXUIMenuMap> menuMap;

	engine_time_point tp_lastFrameRatePollTime;
	uint32 framesThisSecond;
	uint32 framesPerSecond;

	std::vector<class LightSource*> mLightVec;
	std::vector<class Object*> mRenderableEntitiesToRender;

	std::unique_ptr<TextureManager> mngrTexture;
	std::unique_ptr<MeshManager> mngrMesh;
	std::unique_ptr<ShaderManager> mngrShader;

	void processQueuedWindowsMessages(ThreadTaskReference thisTask);

	void initializeMenus();

	void beginFrame();
	void endFrame();
	void recreateTaskGraph();

public:
	GameApp(bool windowed,bool vSync,HINSTANCE hInstance,string_normal title);
	~GameApp();

	void toggleCameraMode();
	void setCameraFreeFly();
	void setCameraPlayer();

	void RenderFrameBegin();
	void RenderGUI();
	void RenderHUD();
	void RenderWorld();
	void RenderFrameEnd();

	bool Initialize(cstring_wide fontFile);


	void BeginGame();
	void RenderFullFrame();
	void RenderSceneToTexture();
	void CheckInput();
	void Release();
	bool IsGameStillRunning();

	bool LoadAssetsFile(const string_normal &assetsFile);
	bool LoadEntitiesFile(const string_normal &assetsFile);
};