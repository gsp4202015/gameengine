#pragma once

class BoolVector3 {
	//////////////////////////////////////////////////////////////////////////
	// STATIC
public:
	static const BoolVector3 FFF;
	static const BoolVector3 TTT;

	//////////////////////////////////////////////////////////////////////////
	// NON - STATIC
public:
	bool data[3];
	bool &x,&y,&z;

	inline BoolVector3() :
		x(data[0]),
		y(data[1]),
		z(data[2]) {
		x = y = z = false;
	}

	inline BoolVector3(bool x,bool y,bool z) : BoolVector3() {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	inline BoolVector3(const BoolVector3 &other) : BoolVector3() {
		x = other.x;
		y = other.y;
		z = other.z;
	}

	inline BoolVector3& operator=(const BoolVector3 &l) {
		x = l.x;
		y = l.y;
		z = l.z;
		return *this;
	}

	bool operator==(const BoolVector3 &l) const {
		return x==l.x && y==l.y && z==l.z;
	}

	bool operator!=(const BoolVector3 &l) const {
		return !(*this == l);
	}
};
