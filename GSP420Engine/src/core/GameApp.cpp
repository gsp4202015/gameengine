#include "GameApp.h"

#include "Engine.h"
#include "graphics\Graphics.h"
#include "physics\Physics.h"
#include "AI\AI.h"
#include "sound\soundManager.h"

// content
#include "content\actors\APlayer.h"
#include "content\actors\ASkybox.h"
//

#include <iostream>

GameApp::GameApp(bool windowed,bool vSync,HINSTANCE hInstance,string_normal title)
	: DXApp(windowed,vSync,hInstance,title) {
	shouldQuit = false;
	isGameRunning = false;
}

GameApp::~GameApp() {
	Release();

	CoUninitialize();
}

bool GameApp::Initialize(cstring_wide fontFile) {
	// PUBLICIZE DATA
	EngineSettings::Instance().system.application = this;

	//////// Initialize Threads //////////////
	//create an explicit worker thread with specific flags
	auto thrd_entityUpdate_and_input_and_window = EngineSettings::Instance().system.threadSpooler.createExplicitWorkerThread();
	thrd_entityUpdate_and_input_and_window->affinity |= ThreadFlags::ENTITY_UPDATE | ThreadFlags::COM_GRAPHICS | ThreadFlags::COM_WINDOW;
	thrd_entityUpdate_and_input_and_window->fullyInitialized = true;

	// create enough implicit threads to occupy every logical core
	unsigned implicitThreadNum = EngineSettings::Instance().system.threadSpooler.createImplicitWorkerThreads();
	///////////////////////////////////

	//////// Initialize COMs //////////////
	; {
		EngineSettings::Instance().system.threadSpooler.clearTasks();

		// get a handle to the root task
		auto rootTask = EngineSettings::Instance().system.threadSpooler.getRootTask();

		auto tsk_init_grpahics = rootTask->createSubTask();
		tsk_init_grpahics->workFunction = [=](ThreadTaskReference thisTask) {
			DXApp::Initialize(fontFile);
		};
		tsk_init_grpahics->affinity |= ThreadFlags::COM_GRAPHICS | ThreadFlags::COM_WINDOW;
		tsk_init_grpahics->fullyInitialized = true;

		rootTask->fullyInitialized = true;

		rootTask->getCompletionFuture().get(); // wait until all above tasks are processed
	}

	mShow = new DirectShow();

	mShow->PlayVideo(mApphWnd,STR("data\\assets\\videos\\GSP420_Intro.mp4"),&EngineSettings::Instance().system.input);

	mngrTexture.reset(new TextureManager);
	mngrMesh.reset(new MeshManager);
	mngrShader.reset(new ShaderManager);

	EngineSettings::Instance().graphics.textureManager = mngrTexture.get();
	EngineSettings::Instance().graphics.meshManager = mngrMesh.get();
	EngineSettings::Instance().graphics.shaderManager = mngrShader.get();

	//////// Initialize GUI System //////////////
	; {
		EngineSettings::Instance().system.threadSpooler.clearTasks();

		// get a handle to the root task
		auto rootTask = EngineSettings::Instance().system.threadSpooler.getRootTask();

		auto tsk_init_gui = rootTask->createSubTask();
		tsk_init_gui->workFunction = [=](ThreadTaskReference thisTask) {
			initializeMenus();
		};
		tsk_init_gui->affinity |= ThreadFlags::COM_GRAPHICS;
		tsk_init_gui->fullyInitialized = true;

		rootTask->fullyInitialized = true;

		rootTask->getCompletionFuture().get(); // wait until all above tasks are processed
	}

	//////// Load Assets AND Initalize Level //////////////
	; {
		mWorld = constructObjectNameless<World>(nullptr); // REQUIRED
		EngineSettings::Instance().system.activeWorld = mWorld;
		mWorld->pause(); // do not have the game time running while loading assets

		//////////////////////////////////////////////////////////////////////////
		// CAMERA ENTITY
		//////////////////////////////////////////////////////////////////////////
		; {
			auto cameraEntity = constructObjectNameless<Actor>(mWorld);
			auto cameraComponent = constructObjectNameless<MouseLookCameraComponent>(cameraEntity);
			// set movement speed, buttons, and vertical range
			cameraComponent->initialize(0.001f, // x sensitivity
										0.001f, // y sensitivity
										90.0f, // vertical range
										15.0f, // movement speed
										'W', // key forward
										'A', // key right
										'S', // key back
										'D', // key left
										VK_SPACE, // key up
										VK_MENU, // key down
										VK_CONTROL, // key slow
										VK_SHIFT, // key fast
										VK_RBUTTON); // key mouse look
			cameraEntity->rootComponent = cameraComponent;
			cameraEntity->rootComponent->transform.position = Vector3(100.0f,11.0f,0.0f);
			cameraComponent->setLookAt(Vector3(0,100.0f,50.0f));

			// set this to be the active camera
			freeFlyCamera = cameraComponent;
			mWorld->setActiveCamera(freeFlyCamera);

			//cameraComponent->setInputEnabled(false);
		}

		EngineSettings::Instance().system.threadSpooler.clearTasks();

		// get a handle to the root task
		auto rootTask = EngineSettings::Instance().system.threadSpooler.getRootTask();

		std::atomic<bool> assetsFullyLoaded = false;
		auto tsk_keepWindowUpdating = rootTask->createSubTask();
		tsk_keepWindowUpdating->workFunction = [&](ThreadTaskReference thisTask) {
			while(assetsFullyLoaded == false) {
				processQueuedWindowsMessages(thisTask);
				std::this_thread::sleep_for(std::chrono::milliseconds(5));
			}
		};
		tsk_keepWindowUpdating->affinity |= ThreadFlags::COM_WINDOW;
		tsk_keepWindowUpdating->fullyInitialized = true;

		rootTask->fullyInitialized = true;

		LoadAssetsFile(STR("data\\assets\\json\\level1assets.json"));
		/*{
		auto renderEntity = constructObjectNameless<Actor>(mWorld);
		auto terrain = constructObjectNameless<RenderableComponent>(renderEntity,new FlatTerrain(mngrTexture->GetTexture(STR("grass")),mngrMesh->GetMesh(STR("terrain")),mDevice,20000));
		renderEntity->rootComponent = terrain;
		renderEntity->rootComponent->transform.position = Vector3(0.0f,0.0f,0.0f);
		mRenderableEntitiesToRender.push_back(terrain);
		}*/

		// CREATE INTERIM FLOOR
		; {
			auto renderEntity = constructObjectNameless<Actor>(mWorld);
			auto testRenderableComponent = constructObjectNameless<RenderableComponent>(renderEntity,new Cube(mngrTexture->GetTexture("grass"),mngrMesh->GetMesh(STR("cube"))));
			auto collider = constructObjectNameless<BoxColliderComponent>(renderEntity,Vector3(1.0f,1.0f,1.0f));
			collider->transform.position.y = -4.f;
			renderEntity->rootComponent = testRenderableComponent;

			renderEntity->rootComponent->transform.position = Vector3(512,0.0f,512);
			renderEntity->rootComponent->transform.scale = Vector3(512,1,512);
		}

		LoadEntitiesFile(STR("data\\assets\\json\\level1entities.json"));

		mngrShader->LoadAndRegisterShader(mDevice,mApphWnd,STR("depth_shader"),new DepthShader(STR("data\\shaders\\DepthVertexShader.hlsl"),STR("data\\shaders\\DepthPixelShader.hlsl")));
		mngrShader->LoadAndRegisterShader(mDevice,mApphWnd,STR("skybox_shader"),new SkyboxShader(STR("data\\shaders\\SkyboxVertexShader.hlsl"),STR("data\\shaders\\SkyboxPixelShader.hlsl")));
		mngrShader->LoadAndRegisterShader(mDevice,mApphWnd,STR("shadow_shader"),new ShadowShader(STR("data\\shaders\\ShadowVertexShader.hlsl"),STR("data\\shaders\\ShadowPixelShader.hlsl")));

		//////// D E B U G //////////////
		mFrustrum = new Frustrum(mDevice);

		mHUD = new GameHUD(mClientWidth,mClientHeight,"data\\assets\\json\\gamehud.json",mngrTexture.get());

		//////////////////////////////////////////////////////////////////////////
		// SKYBOX ENTITY
		//////////////////////////////////////////////////////////////////////////
		auto skyboxEntity = constructObjectNameless<ASkybox>(mWorld,new SkyboxMesh(),mDevice);

		LightSource *sun = new LightSource(mDevice,Vector4(0.3f,0.3f,0.3f,1.0f),Vector4(0.4f,0.4f,0.4f,1.0f),Vector3(-200.0f,350.0f,512.0f),Vector3(512.0f,0.0f,512.0f),2000,2000,2000.0f,4.0f);
		mLightVec.push_back(sun);
		///////////////////////////////////

		// Artificial Intelligence TESTING
		//; {
		//	// rigid body
		//	auto renderEntity = constructObjectNameless<Actor>(mWorld);
		//	auto testRenderableComponent1 = constructObjectNameless<RenderableComponent>(renderEntity,new Cube(mngrTexture->GetTexture("stone"),mngrMesh->GetMesh("cube")));
		//	auto rigidBodyComponent = constructObjectNameless<RigidBodyComponent>(renderEntity,0.5f);
		//	auto colliderComponent = constructObjectNameless<BoxColliderComponent>(renderEntity,Vector3(0.5f,0.5f,0.5f));
		//	//auto colliderComponent = constructObjectNameless<SphereColliderComponent>(renderEntity,0.5f);
		//	rigidBodyComponent->setCollider(colliderComponent);
		//	renderEntity->rootComponent = rigidBodyComponent;
		//	renderEntity->rootComponent->transform.position = Vector3(100.0f, 0.0f, 75.0f);
		//	renderEntity->rootComponent->transform.scale = Vector3(10,10.0f,10.0f);
		//	rigidBodyComponent->angularVelocity.x = -0.2f;
		//	//rigidBodyComponent->density = 60;
		//	auto aiComponent = constructObjectNameless<AIComponent>(renderEntity);
		//	aiComponent->SetState(new WanderStateAI(mWorld->getActivePlayer()));
		//	//aiComponent->SetState(new PursueStateAI(mWorld->getActivePlayer()));
		//}

		// SOUND
		soundManager::Instance().initialize();

		assetsFullyLoaded = true;
		rootTask->getCompletionFuture().get(); // wait until all above tasks are processed
	}

	framesThisSecond = framesPerSecond = 0;
	tp_lastFrameRatePollTime = engine_clock::now();
	EngineSettings::Instance().time.tpLastFrameTime = engine_clock::now();

	return true;
}

void GameApp::processQueuedWindowsMessages(ThreadTaskReference thisTask) {
	MSG msg = {0};
	while(PeekMessage(&msg,NULL,NULL,NULL,PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	shouldQuit = WM_QUIT == msg.message;
}

void GameApp::beginFrame() {
	// TIMING
	engine_time_point tpCurrentTime = engine_clock::now();
	EngineSettings::Instance().time.timeDeltaLastFrame = tpCurrentTime - EngineSettings::Instance().time.tpLastFrameTime;
	EngineSettings::Instance().time.tpLastFrameTime = tpCurrentTime;

	// TASKS
	recreateTaskGraph();
}

void GameApp::endFrame() {
	if(!shouldQuit) {
		framesThisSecond++;
		engine_time_point tp_now = engine_clock::now();
		if((tp_now - tp_lastFrameRatePollTime).count() >= 1.0) {
			tp_lastFrameRatePollTime = tp_now;
			framesPerSecond = framesThisSecond;
			framesThisSecond = 0;
		}
		beginFrame();
	} else {
		isGameRunning = false;
	}
}

void GameApp::recreateTaskGraph() {
	//////////////////////////////////////////////////////////////////////////
	// begin building task graph
	EngineSettings::Instance().system.threadSpooler.clearTasks();

	// get a handle to the root task
	auto rootTask = EngineSettings::Instance().system.threadSpooler.getRootTask();

	; {
		auto tsk_thisFrame = rootTask->createSubTask();

		; {
			// WINDOW MESSAGES
			auto tsk_processWindowMessages = tsk_thisFrame->createSubTask();
			tsk_processWindowMessages->workFunction = [this](ThreadTaskReference thisTask) {
				processQueuedWindowsMessages(thisTask);
			};
			tsk_processWindowMessages->fullyInitialized = true;

			// GARBAGE COLLECTION
			auto tsk_objectGarbageCollection = tsk_thisFrame->createSubTask();
			tsk_objectGarbageCollection->workFunction = [](ThreadTaskReference thisTask) {
				Object::doGarbageCollection();
			};
			tsk_objectGarbageCollection->setDependencies({tsk_processWindowMessages});
			tsk_objectGarbageCollection->fullyInitialized = true;

			// INPUT
			auto tsk_sys_input = tsk_thisFrame->createSubTask();
			tsk_sys_input->workFunction = [this](ThreadTaskReference thisTask) {
				EngineSettings::Instance().system.input.updateInput();
				CheckInput();
			};
			tsk_sys_input->setDependencies({tsk_objectGarbageCollection});
			tsk_sys_input->fullyInitialized = true;

			// SOUND
			auto tsk_sys_sound = tsk_thisFrame->createSubTask();
			tsk_sys_sound->workFunction = [](ThreadTaskReference thisTask) {
				soundManager::Instance().update();
			};
			tsk_sys_sound->setDependencies({tsk_objectGarbageCollection});
			tsk_sys_sound->fullyInitialized = true;

			// UI
			auto tsk_sys_ui = tsk_thisFrame->createSubTask();
			tsk_sys_ui->workFunction = [this](ThreadTaskReference thisTask) {
				// UI is updated off real time, not connected to the world
				Tickable::tickGroups(TickGroup::UI,EngineSettings::Instance().time.timeDeltaLastFrame);
			};
			tsk_sys_ui->setDependencies({tsk_sys_input});
			tsk_sys_ui->fullyInitialized = true;

			// world
			auto tsk_sys_world = rootTask->createSubTask();
			tsk_sys_world->workFunction = [this](ThreadTaskReference thisTask) {
				// call update of world system here

				mWorld->threadProc(thisTask);
			};
			tsk_sys_world->setDependencies({tsk_sys_input});
			tsk_sys_world->fullyInitialized = true;

			// rendering
			auto tsk_sys_rendering = tsk_thisFrame->createSubTask();
			; {
				// pre-render tick
				auto tsk_preRenderTickEntities = tsk_sys_rendering->createSubTask();
				tsk_preRenderTickEntities->workFunction = [this](ThreadTaskReference thisTask) {
					Tickable::tickGroups(TickGroup::PRE_GRAPHICS,mWorld->gameFrameTimeDelta);
				};
				tsk_sys_input->affinity |= ThreadFlags::ENTITY_UPDATE;
				tsk_preRenderTickEntities->fullyInitialized = true;

				// actually render
				auto tsk_renderEntities = tsk_sys_rendering->createSubTask();
				tsk_renderEntities->workFunction = [this](ThreadTaskReference thisTask) {
					RenderFullFrame();
				};
				tsk_renderEntities->affinity |= ThreadFlags::COM_GRAPHICS;
				tsk_renderEntities->setDependencies({tsk_preRenderTickEntities});
				tsk_renderEntities->fullyInitialized = true;

				// post-render tick
				auto tsk_postRenderTickEntities = tsk_sys_rendering->createSubTask();
				tsk_postRenderTickEntities->workFunction = [this](ThreadTaskReference thisTask) {
					Tickable::tickGroups(TickGroup::POST_GRAPHICS,mWorld->gameFrameTimeDelta);
				};
				tsk_sys_input->affinity |= ThreadFlags::ENTITY_UPDATE;
				tsk_postRenderTickEntities->setDependencies({tsk_renderEntities});
				tsk_postRenderTickEntities->fullyInitialized = true;
			}
			tsk_sys_rendering->setDependencies({tsk_sys_world,tsk_sys_ui});
			tsk_sys_rendering->fullyInitialized = true;
		}

		tsk_thisFrame->fullyInitialized = true;

		auto tsk_nextFrame = rootTask->createSubTask();
		tsk_nextFrame->workFunction = [this](ThreadTaskReference thisTask) {
			endFrame();
		};
		tsk_nextFrame->setDependencies({tsk_thisFrame});
		tsk_nextFrame->fullyInitialized = true;
	}

	rootTask->fullyInitialized = true;

	//////////////////////////////////////////////////////////////////////////
}

void GameApp::BeginGame() {
	shouldQuit = false;
	isGameRunning = true;
	beginFrame();
}

void GameApp::CheckInput() {
	// camera toggle
	if(EngineSettings::Instance().system.input.getKeyDown(VK_TAB)) {
		toggleCameraMode();
	}

	for(uint32 i = 0; i < mLightVec.size(); i++) {
		mLightVec[i]->Update((float)mWorld->gameFrameTimeDelta.count(),"");
	}
}

void GameApp::RenderSceneToTexture() {
	const auto &camera = mWorld->getActiveCamera();
	assert(camera != nullptr);

	const Matrix &viewMatrix = camera->getViewMatrix();
	const Matrix &projectionMatrix = camera->getProjectionMatrix();

	for(uint32 j = 0; j < mLightVec.size(); j++) {
		mLightVec[j]->GetRenderTexture()->SetRenderTarget(mDeviceContext);
		mLightVec[j]->GetRenderTexture()->ClearRenderTarget(mDeviceContext,0.0f,0.0f,0.0f,1.0f);

		for(auto &renderableComponent : mRenderableEntitiesToRender) {
			RenderableComponent *asComponent = (RenderableComponent*)renderableComponent;
			asComponent->graphicsEntity->RenderToTexture(mDeviceContext,
														 mFrustrum,
														 mngrShader.get(),
														 viewMatrix,
														 projectionMatrix,
														 mLightVec[j]);
		}
	}

	SetBackBufferRenderTarget();
	ResetViewport();
}

void GameApp::RenderFullFrame() {
	RenderFrameBegin();

	if(EngineSettings::Instance().system.activeMenuMap->getCurrentMenu() == nullptr) {
		RenderWorld();
		RenderHUD();
	} else {
		RenderGUI();
	}

	RenderFrameEnd();
}

void GameApp::RenderFrameBegin() {
	mDeviceContext->ClearRenderTargetView(mRenderTargetView,DirectX::Colors::Black);
	mDeviceContext->ClearDepthStencilView(mDepthStencilView,D3D11_CLEAR_DEPTH,1.0f,NULL);
}

void GameApp::RenderGUI() {
	mSpriteBatch->Begin();
	// render the active menu map
	auto activeMenuMap = EngineSettings::Instance().system.activeMenuMap;
	if(activeMenuMap != nullptr) {
		activeMenuMap->render();
	}
	mSpriteBatch->End();
}

void GameApp::RenderHUD() {
	TurnZBufferOff();

	//2D Rendering
	mHUD->Render(mSpriteBatch.get(),mSpriteFont.get());

	TurnZBufferOn();
}

void GameApp::RenderWorld() {
	mRenderableEntitiesToRender = Object::filterObjectsByPredicate([](const Object *obj)->bool {
		if(dynamic_cast<const RenderableComponent*>(obj)) return true;
		return false;
	});

	const auto &camera = mWorld->getActiveCamera();
	assert(camera != nullptr);

	const Matrix &viewMatrix = camera->getViewMatrix();
	const Matrix &projectionMatrix = camera->getProjectionMatrix();

	mFrustrum->ConstructFrustum(1600.0f,projectionMatrix,viewMatrix);

	//3D Rendering
	RenderSceneToTexture();

	mDeviceContext->ClearRenderTargetView(mRenderTargetView,DirectX::Colors::LightBlue);
	mDeviceContext->ClearDepthStencilView(mDepthStencilView,D3D11_CLEAR_DEPTH,1.0f,NULL);

	for(uint32 j = 0; j < mLightVec.size(); j++) {
		// iterate over all RenderableComponent's and render them
		for(auto &renderableComponent : mRenderableEntitiesToRender) {
			RenderableComponent *asComponent = (RenderableComponent*)renderableComponent;
			asComponent->graphicsEntity->Render(mDeviceContext,mFrustrum,mngrShader.get(),viewMatrix,projectionMatrix,mLightVec[j]);
		}
	}

	TurnZBufferOff();

	//2D Rendering
	mHUD->Render(mSpriteBatch.get(),mSpriteFont.get());

	TurnZBufferOn();
}

void GameApp::RenderFrameEnd() {
	if(mVsyncEnabled) {
		HR(mSwapChain->Present(1,0));
	} else {
		HR(mSwapChain->Present(0,0));
	}
}


void GameApp::Release() {
	DXApp::Release();

	/*for (uint32 i = 0; i < mRenderableEntitiesToRender.size(); i++)
	Memory::SafeRelease(mRenderableEntitiesToRender[i]);*/

	// THE WORKER THREADS MUST BE EXPLICITLY STOPPED BEFORE THE MAIN THREAD EXITS!!!
	// OTHERWISE THE THREADS WILL HANG AND NEVER EXIT.
	EngineSettings::Instance().system.threadSpooler.stopAllWorkers();

	EngineSettings::Destroy();
}

bool GameApp::IsGameStillRunning() {
	return isGameRunning;
}

bool GameApp::LoadAssetsFile(const string_normal &assetsFile) {
	json assets = json::parse_file(assetsFile);

	json textures = assets["texture_files"];
	json meshes = assets["mesh_files"];

	for(uint32 i = 0; i < textures.size(); i++) {
		mngrTexture->LoadAndRegisterTexture(mDevice,textures[i]["key"].as<string_normal>(),textures[i]["file_path"].as<string_normal>());
	}

	for(uint32 i = 0; i < meshes.size(); i++) {
		mngrMesh->LoadAndRegisterMesh(mDevice,meshes[i]["key"].as<string_normal>(),meshes[i]["file_path"].as<string_normal>());
	}

	return true;
}

bool GameApp::LoadEntitiesFile(const string_normal &assetsFile) {
	json entities = json::parse_file(assetsFile);

	json player = entities["player"];
	{
		auto actor = constructObjectNameless<APlayer>(mWorld,mngrTexture->GetTexture(player[0]["texture_key"].as<string_normal>()),mngrMesh->GetMesh(player[0]["mesh_key"].as<string_normal>()));

		actor->rootComponent->transform.position.x = (float)player[0]["position_x"].as<double>();
		actor->rootComponent->transform.position.y = (float)player[0]["position_y"].as<double>();
		actor->rootComponent->transform.position.z = (float)player[0]["position_z"].as<double>();

		actor->rootComponent->transform.scale.x = (float)player[0]["scale_x"].as<double>();
		actor->rootComponent->transform.scale.y = (float)player[0]["scale_y"].as<double>();
		actor->rootComponent->transform.scale.z = (float)player[0]["scale_z"].as<double>();

		mWorld->setActivePlayer(actor);
	}

	json enemies = entities["enemy"];
	{
		for(uint32 i = 0; i < enemies.size(); i++) {
			auto renderEntity = constructObjectNameless<Actor>(mWorld);
			string_normal meshName = enemies[i]["mesh_key"].as<string_normal>(); 
			auto testRenderableComponent = constructObjectNameless<RenderableComponent>(renderEntity,new Sphere(mngrTexture->GetTexture(enemies[i]["texture_key"].as<string_normal>()),mngrMesh->GetMesh(meshName)));
			auto rigidBodyComponent = constructObjectNameless<RigidBodyComponent>(renderEntity,0.5f);
			renderEntity->rootComponent = rigidBodyComponent;

			renderEntity->rootComponent->transform.position.x = (float)enemies[i]["position_x"].as<double>();
			renderEntity->rootComponent->transform.position.y = (float)enemies[i]["position_y"].as<double>();
			renderEntity->rootComponent->transform.position.z = (float)enemies[i]["position_z"].as<double>();

			renderEntity->rootComponent->transform.scale.x = (float)enemies[i]["scale_x"].as<double>();
			renderEntity->rootComponent->transform.scale.y = (float)enemies[i]["scale_y"].as<double>();
			renderEntity->rootComponent->transform.scale.z = (float)enemies[i]["scale_z"].as<double>();

			ColliderComponent* collider = nullptr;
			if(meshName == "sphere") {
				collider = constructObjectNameless<SphereColliderComponent>(renderEntity,2.0f);
			} else if(meshName == "cube") {
				collider = constructObjectNameless<BoxColliderComponent>(renderEntity,Vector3(1,1,1));
			} else { assert(false); }

			rigidBodyComponent->setCollider(collider);

			auto aiComponent = constructObjectNameless<AIComponent>(renderEntity);
			//aiComponent->SetState(new WanderStateAI(mWorld->getActivePlayer()));
			aiComponent->SetState(new PursueStateAI(mWorld->getActivePlayer()));
		}
	}

	json barriers = entities["barrier"];
	{
		for(uint32 i = 0; i < barriers.size(); i++) {
			auto renderEntity = constructObjectNameless<Actor>(mWorld);
			auto testRenderableComponent = constructObjectNameless<RenderableComponent>(renderEntity,new Cube(mngrTexture->GetTexture(barriers[i]["texture_key"].as<string_normal>()),mngrMesh->GetMesh(barriers[i]["mesh_key"].as<string_normal>())));
			auto collider = constructObjectNameless<BoxColliderComponent>(renderEntity,Vector3(1.0f,1.0f,1.0f));
			renderEntity->rootComponent = testRenderableComponent;

			renderEntity->rootComponent->transform.position.x = (float)barriers[i]["position_x"].as<double>();
			renderEntity->rootComponent->transform.position.y = (float)barriers[i]["position_y"].as<double>();
			renderEntity->rootComponent->transform.position.z = (float)barriers[i]["position_z"].as<double>();

			renderEntity->rootComponent->transform.scale.x = (float)barriers[i]["scale_x"].as<double>();
			renderEntity->rootComponent->transform.scale.y = (float)barriers[i]["scale_y"].as<double>();
			renderEntity->rootComponent->transform.scale.z = (float)barriers[i]["scale_z"].as<double>();
		}
	}

	return true;
}

void GameApp::toggleCameraMode() {
	if(mWorld->getActiveCamera() == freeFlyCamera) {
		setCameraPlayer();
	} else {
		setCameraFreeFly();
	}
}

void GameApp::setCameraFreeFly() {
	EngineSettings::Instance().system.input.setMouseFPSMode(false);
	freeFlyCamera->setInputEnabled(true);
	freeFlyCamera->getActor()->rootComponent->transform = mWorld->getActiveCamera()->getComponentToWorld();
	mWorld->setActiveCamera(freeFlyCamera);
}

void GameApp::setCameraPlayer() {
	EngineSettings::Instance().system.input.setMouseFPSMode(true);
	freeFlyCamera->setInputEnabled(false);
	mWorld->setActiveCamera(mWorld->getActivePlayer()->myCamera);
}

void GameApp::initializeMenus() {
	menuMap.reset(new HDXUIMenuMap);

	EngineSettings::Instance().system.activeMenuMap = menuMap.get();

	string_normal tex_menubackground("data/assets/textures/ui/menubackground.png");

	string_normal tex_basebutton("data/assets/textures/ui/basebutton.png");
	string_normal tex_basebutton_hover("data/assets/textures/ui/basebutton_hover.png");

	string_normal tex_basecheckbox("data/assets/textures/ui/basecheckbox.png");
	string_normal tex_basecheckbox_hover("data/assets/textures/ui/basecheckbox_hover.png");
	string_normal tex_basecheckbox_check("data/assets/textures/ui/basecheckbox_check.png");

	string_normal tex_baseslider("data/assets/textures/ui/baseslider.png");
	string_normal tex_baseslider_handle("data/assets/textures/ui/baseslider_handle.png");

	uint32 ww = GetWindowSize().x;
	uint32 wh = GetWindowSize().y;

	// *************
	//
	// Main Menu
	//
	// *************

	{
		HDXUIMenu *menu_mainmenu = menuMap->createMenu("menu_main");

		menu_mainmenu->registerCallbackOnTransitionEnter([=](bool dotransition)->void {
			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_enter,"gui");
			//HDX_MUSICMAN->setTrackCrossfade(snd_music_menu,3.0f);
		});

		menu_mainmenu->registerCallbackOnTransitionExit([=](bool dotransition)->void {
			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_exit,"gui");
		});

		auto menu_mainmenu_texturedraw = menu_mainmenu->createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																									 tex_menubackground);

		menu_mainmenu->setActionSize(512.0f,
									 384.0f);

		menu_mainmenu->setPosition(ww+menu_mainmenu->getActionWidth()*0.6f,
								   wh/2.0f);

		auto menu_mainmenu_transition_lerp_position = menu_mainmenu->createEffect<HDXUIElementEffect_Transition_LERP<Vector2>>("LERP_position",
																															   [=](void) {return menu_mainmenu->getPosition(); },
																															   [=](const Vector2 &var) {return menu_mainmenu->setPosition(var); },
																															   HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																															   0.75f,
																															   Vector2(ww/2.0f,wh/2.0f));

		auto menu_mainmenu_transition_lerp_rotation = menu_mainmenu->createEffect<HDXUIElementEffect_Transition_LERP<float>>("LERP_rotation",
																															 [=](void) {return menu_mainmenu->getRotation(); },
																															 [=](const float &var) {return menu_mainmenu->setRotation(var); },
																															 HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																															 0.75f,
																															 PI*2.0f);

		auto menu_mainmenu_oscillation_rotation = menu_mainmenu->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_rotation",
																													   [=](void) {return menu_mainmenu->getRotation(); },
																													   [=](const float &var) {return menu_mainmenu->setRotation(var); },
																													   PI/32.0f,
																													   5.0f);

		auto alignment_center = menu_mainmenu->createChildElement<HDXUIElement_Alignment>("alignment_center",
																						  HDXAL_CENTER,
																						  VEC2TOARG(Vector2::UnitY),
																						  0.0f,
																						  0.0f,
																						  10.0f);

		alignment_center->setPosition(0,
									  -menu_mainmenu->getActionHeight()/4+alignment_center->getPadding()*2);

		auto button_play = alignment_center->createChildElement<HDXUIElement_Button>("button_demos",
																					 tex_basebutton,
																					 tex_basebutton_hover,
																					 "DX:Arial_15",
																					 "Play",
																					 0xffffffff);
		button_play->registerCallbackOnMouseEnter([=](void)->void {
			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_play->registerCallbackOnMousePress([=](WPARAM key)->void {
			mWorld->unPause(); // resume game time
			setCameraPlayer();
			menuMap->navigateToMenu("menu_demos",true);
			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		// 		auto button_options = alignment_center->createChildElement<HDXUIElement_Button>("button_options",
		// 																						tex_basebutton,
		// 																						tex_basebutton_hover,
		// 																						"DX:Arial_15",
		// 																						"Options",
		// 																						0xffffffff);
		// 		button_options->registerCallbackOnMouseEnter([=](void)->void {
		// 			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		// 		});
		// 		button_options->registerCallbackOnMousePress([=](WPARAM key)->void {
		// 			menuMap->navigateToMenu("menu_options",true);
		// 			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		// 		});
		//
		// 		auto button_credits = alignment_center->createChildElement<HDXUIElement_Button>("button_credits",
		// 																						tex_basebutton,
		// 																						tex_basebutton_hover,
		// 																						"DX:Arial_15",
		// 																						"Credits",
		// 																						0xffffffff);
		// 		button_credits->registerCallbackOnMouseEnter([=](void)->void {
		// 			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		// 		});
		// 		button_credits->registerCallbackOnMousePress([=](WPARAM key)->void {
		// 			menuMap->navigateToMenu("menu_credits",true);
		// 			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		// 		});

		alignment_center->jumpDistance(alignment_center->getPadding()*10.0f);

		auto button_quit = alignment_center->createChildElement<HDXUIElement_Button>("button_quit",
																					 tex_basebutton,
																					 tex_basebutton_hover,
																					 "DX:Arial_15",
																					 "Quit",
																					 0xffff0000);
		button_quit->registerCallbackOnMouseEnter([=](void)->void {
			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_quit->registerCallbackOnMousePress([=](WPARAM key)->void {
			PostQuitMessage(0);
			//HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_quit_oscillation_position_x = button_quit->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_position_x",
																													 [=](void) {return button_quit->getPosition().x; },
																													 [=](const float &var) {return button_quit->setPosition(var,button_quit->getPosition().y); },
																													 50.0f,
																													 5.0f,
																													 0.0f);

		auto button_quit_oscillation_position_y = button_quit->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_position_y",
																													 [=](void) {return button_quit->getPosition().y; },
																													 [=](const float &var) {return button_quit->setPosition(button_quit->getPosition().x,var); },
																													 10.0f,
																													 1.5f,
																													 0.5f);
	}

	menuMap->navigateToMenu("menu_main",true);
}
