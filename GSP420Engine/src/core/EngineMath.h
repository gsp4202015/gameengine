#pragma once

#include <SimpleMath.h>
using namespace DirectX::SimpleMath;

#define PI 3.14159265359f

#define RAD_TO_DEG (180.0f / PI)
#define DEG_TO_RAD (PI / 180.0f)

// OPERATORS

// DOT PRODUCT
float operator|(const Vector3 &l,const Vector3 &r);
float operator|(const Vector2 &l,const Vector2 &r);

// CROSS PRODUCT
Vector3 operator%(const Vector3 &l,const Vector3 &r);

// TRANSFORM VECTOR BY MATRIX
Vector3 operator*(const Vector3 &l,const Matrix &r);
Vector2 operator*(const Vector2 &l,const Matrix &r);

namespace MathUtilities {
	// FUNCTIONS
	inline float lerp(float a,float b,float t) { return a*(1.0f - t) + b*(t); }

	Vector3 getNormal(const Vector3 &v);
	void makeOrthoNormalBasis(Vector3 *x,Vector3 *y,Vector3 *z);
	void orthoNormalize(Matrix *m);
	Quaternion quatFromEuler(const Vector3 &euler);
	//Quaternion quatAddEuler(const Quaternion &quat,const Vector3 &euler);

	Vector3 perpendicular(const Vector3 &perpendicularTo);

	Quaternion rotateTo(const Vector3 &q,const Vector3 &p);

	Quaternion nullifyRotationOnAxis(const Quaternion &quat,const Vector3 &axis);
	float getAngleBetween(const Vector3 &dir1,const Vector3 &dir2,const Vector3 &norm = Vector3::Zero,bool signedAngle = false);

	Matrix skewSymmetric(const Vector3 &vector);
	Matrix lookDirection(const Vector3 &localforward,const Vector3 &targetdir,const Vector3 &localup = Vector3::Up,const Vector3 &worldup = Vector3::Up);

	Matrix removeScale(const Matrix &mat);

	Quaternion quatAddAngularOffset(const Quaternion &quat,const Vector3 &offset);

	Vector3 quatToEuler(const Quaternion &quat);
	Quaternion quatFromEuler(const Vector3 &euler);

	void barycentric(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c,float *u,float *v,float *w);
	float pointDistanceFromLine(const Vector3 &p,const Vector3 &a,const Vector3 &b);
	float pointDistanceFromTriangle(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c);
	float pointDistanceFromPlane(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c);
	float pointDistanceFromPlane(const Vector3 &p,const Vector3 &n,const Vector3 &a);
	float pointDistanceFromPlane(const Vector3 &p,const Vector3 &n,float d);
}

using namespace MathUtilities;
