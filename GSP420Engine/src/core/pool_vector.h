#pragma once

#include <vector>
#include <tuple>

#include "custom_iterator.h"

template<typename T>
class pool_vector {
private:
	template<bool isconst = false>
	class _iterator : public custom_iterator<T,isconst> {
		friend class pool_vector;

	private:
		typedef typename choose<isconst,const pool_vector<T>*,pool_vector<T>*>::type container_ptr;

		container_ptr _container;
		unsigned int _index;

		_iterator(container_ptr container,unsigned int index,bool isAlreadyValid) : _container(container),_index(index) {
			if(!isAlreadyValid) _moveToFirstValidIndex();
		}

		void _moveToFirstValidIndex() {
			while(*this != _container->end()) {
				if(std::get<0>(_container->_data[_index])) break;
				_index++;
			}
		}

	public:
		_iterator() : _container(nullptr),_index(0) {}
		_iterator(const custom_iterator<T,false> &other) : _container(other._container),_index(other._index) {}

		reference operator*() const { return std::get<1>(_container->_data[_index]); }
		pointer operator->() const { return &std::get<1>(_container->_data[_index]); }
		_iterator& operator++() {
			_index++;
			_moveToFirstValidIndex();
			return *this;
		}

		_iterator operator++(int) {
			_iterator tmp(*this);
			++*this;
			return tmp;
		}

		operator bool() const {
			return _container != nullptr && *this != _container->end();
		}

		friend bool operator==(const _iterator &l,
							   const _iterator &r) {
			return (l._container == r._container) && (l._index == r._index);
		}

		friend bool operator!=(const _iterator &l,
							   const _iterator &r) {
			return !(l==r);
		}
	};

public:
	typedef _iterator<false> iterator;
	typedef _iterator<true>  const_iterator;

private:
	std::vector<std::tuple<bool,T>> _data;
	unsigned int _size;

	inline unsigned int _getFreeSlot() {
		for(unsigned int i = 0; i < _data.size(); i++) {
			if(!std::get<0>(_data[i])) return i;
		}

		return _data.size();
	}

public:
	pool_vector() {
		_size = 0;
	}

	pool_vector(const pool_vector &other) : _data(other._data),_size(other.size) {}
	pool_vector(pool_vector &&other) : _data(std::move(other._data)),_size(std::move(other.size)) {}

	pool_vector& operator=(const pool_vector &l) { _data = l._data; _size = l._size; }
	pool_vector& operator=(pool_vector &&l) { _data = std::move(l._data); _size = std::move(l._size); }

	inline const_iterator begin() const {
		return const_iterator(this,0,false);
	}
	inline iterator begin() {
		return iterator(this,0,false);
	}

	inline const_iterator end() const {
		return const_iterator(this,_data.size(),true);
	}
	inline iterator end() {
		return iterator(this,_data.size(),true);
	}

	inline iterator insert(const T &item) {
		const auto freeSlot = _getFreeSlot();
		if(freeSlot == _data.size()) {
			_data.emplace_back(true,item);
		} else {
			std::get<1>(_data[freeSlot]) = item;
			std::get<0>(_data[freeSlot]) = true;
		}
		_size++;
		return iterator(this,freeSlot,true);
	}

	template<typename ...Args>
	inline iterator emplace(Args ...args) {
		const auto freeSlot = _getFreeSlot();
		if(freeSlot == _data.size()) {
			_data.emplace_back(true,T(args...));
		} else {
			std::get<1>(_data[freeSlot]) = std::move(T(args...));
			std::get<0>(_data[freeSlot]) = true;
		}
		_size++;
		return iterator(this,freeSlot,true);
	}

	inline void remove(const iterator &it) {
		assert(std::get<0>(_data[it._index])); // the iterator must be valid
		std::get<0>(_data[it._index]) = false;
		_size--;
		_size--;
	}

	inline void remove(const T &item) {
		for(auto it = begin(); it != end(); it++) {
			if(item == *it) {
				remove(it);
				break;
			}
		}
	}

	inline void clear() {
		_data.clear();
	}

	inline void reserve(unsigned int capacity) {
		_data.reserve(capacity);
	}

	inline unsigned int capacity() const {
		return _data.capacity();
	}

	inline unsigned int size() const {
		return _size;
	}
};