#include "World.h"
#include "Actor.h"
#include "Engine.h"

#include "physics\integrator\EulerIntegrator.h"
#include "physics\contact_generator\GJKEPAGenerator.h"
#include "physics\contact_resolver\ImpulseContactResolver.h"

#include "graphics\camera\CameraComponent.h"
#include "content\actors\APlayer.h"

#include "..\AI\AIComponent.h"

#include <algorithm>
#include <functional>

World::World() {
	contacts.reserve(1000);

	_activeCamera = nullptr;
	_activePlayer = nullptr;

	_physicsIntegrator = nullptr;
	_contactGenerator = nullptr;
	_contactResolver = nullptr;

	_physicsTimeToSimulate = engine_duration(0);

	setPhysicsIntegrator<EulerIntegrator>();
	setContactGenerator<GJKEPAGenerator>();
	setContactResolver<ImpulseContactResolver>();

	unPause();
}

World::~World() {
}

void World::onSubObjectRegistering(Object *obj) {
	// maintain the cache of actors
	Actor *obj_as_Actor = dynamic_cast<Actor*>(obj);
	if(obj_as_Actor != nullptr) {
		_actors.insert(obj_as_Actor);
	}
}

void World::onSubObjectUnregistering(Object *obj) {
	// maintain the cache of actors
	Actor *obj_as_Actor = dynamic_cast<Actor*>(obj);
	if(obj_as_Actor != nullptr) {
		_actors.remove(obj_as_Actor);
	}
}

void World::pause() {
	_isPaused = true;
}

void World::unPause() {
	if(isPaused()) {
		_tpLastFrameTime = engine_clock::now();
		_isPaused = false;
	}
}

bool World::isPaused() const {
	return _isPaused;
}

void World::updateAI(ThreadTaskReference thisTask) {
	// TODO: Jorge
	float deltaTime = (float)gameFrameTimeDelta.count();
	for(auto actor : _actors)
	{
		if(actor->AI == nullptr)
			continue;

		actor->AI->tick(deltaTime);
	}	
}

void World::updateActors(ThreadTaskReference thisTask) {
	engine_time_point tpCurrentTime = engine_clock::now();

	float gameFrameTimeDeltaSeconds = (float)(tpCurrentTime - _tpLastFrameTime).count() * EngineSettings::Instance().time.timeScale;
	gameFrameTimeDeltaSeconds = (std::min)(gameFrameTimeDeltaSeconds,EngineSettings::Instance().time.maximumTimeStep);
	gameFrameTimeDelta = engine_duration(gameFrameTimeDeltaSeconds);
	gameTime += gameFrameTimeDelta;
	_tpLastFrameTime = tpCurrentTime;

	_physicsTimeToSimulate += gameFrameTimeDelta;

	auto tsk_tickActors = thisTask->createSubTask();
	tsk_tickActors->affinity |= ThreadFlags::ENTITY_UPDATE; // note: must update all entities using the same thread

	tsk_tickActors->workFunction = [this](ThreadTaskReference thisTask) {
		Tickable::tickGroups(TickGroup::ENTITY,gameFrameTimeDelta);
	};

	tsk_tickActors->fullyInitialized = true;
}

void World::updateSpacialPartitions(ThreadTaskReference thisTask) {
}

void World::updatePhysics(ThreadTaskReference thisTask) {
	// enqueue enough physics updates to maintain a 1 to 1 ratio between actual time passed and the time passed within
	// the physics engine, in fixed time steps

	physicsFrameTimeDelta = engine_duration(EngineSettings::Instance().physics.fixedTimeStep * EngineSettings::Instance().time.timeScale);

	const float timeToStepThisFrame = (float)_physicsTimeToSimulate.count();
	const uint8 numPhysicsUpdatesThisFrame = (uint8)(timeToStepThisFrame / physicsFrameTimeDelta.count());
	ThreadTaskReference tsk_sys_physicsFramePrev;
	
	// enqueue all physics frames
	for(uint8 i = 0; i < numPhysicsUpdatesThisFrame; i++) {
		auto tsk_physicsFrame = thisTask->createSubTask();
		if(tsk_sys_physicsFramePrev) {
			tsk_physicsFrame->setDependencies({tsk_sys_physicsFramePrev});
		}
		; {
			// update frame time
			auto tsk_updateFrameTime = tsk_physicsFrame->createSubTask();
			tsk_updateFrameTime->workFunction = [this](ThreadTaskReference thisTask) {
				physicsTime += physicsFrameTimeDelta;
			};
			tsk_updateFrameTime->fullyInitialized = true;

			// pre-update tick actors
			auto tsk_prePhysicsTickActors = tsk_physicsFrame->createSubTask();
			tsk_prePhysicsTickActors->workFunction = [this](ThreadTaskReference thisTask) {
				Tickable::tickGroups(TickGroup::PRE_PHYSICS,physicsFrameTimeDelta);
			};
			tsk_prePhysicsTickActors->affinity |= ThreadFlags::ENTITY_UPDATE;
			tsk_prePhysicsTickActors->setDependencies({tsk_updateFrameTime});
			tsk_prePhysicsTickActors->fullyInitialized = true;

			// integrate bodies
			auto tsk_intigrateBodies = tsk_physicsFrame->createSubTask();
			tsk_intigrateBodies->workFunction = [this](ThreadTaskReference thisTask) {
				if(_physicsIntegrator != nullptr) {
					_physicsIntegrator->threadProc(thisTask,this);
				}
			};
			tsk_intigrateBodies->setDependencies({tsk_prePhysicsTickActors});
			tsk_intigrateBodies->fullyInitialized = true;

			// update persistent contacts
			auto tsk_updatePersistantContacts = tsk_physicsFrame->createSubTask();
			tsk_updatePersistantContacts->workFunction = [this](ThreadTaskReference thisTask) {
			};
			tsk_updatePersistantContacts->setDependencies({tsk_intigrateBodies});
			tsk_updatePersistantContacts->fullyInitialized = true;

			// generate new contacts
			auto tsk_generateNewContacts = tsk_physicsFrame->createSubTask();
			tsk_generateNewContacts->workFunction = [this](ThreadTaskReference thisTask) {
				if(_contactGenerator != nullptr) {
					_contactGenerator->threadProc(thisTask,this);
				}
			};
			tsk_generateNewContacts->setDependencies({tsk_intigrateBodies});
			tsk_generateNewContacts->fullyInitialized = true;

			// resolve contacts
			auto tsk_resolveContacts = tsk_physicsFrame->createSubTask();
			tsk_resolveContacts->workFunction = [this](ThreadTaskReference thisTask) {
				if(_contactResolver != nullptr) {
					_contactResolver->threadProc(thisTask,this);
				}
			};
			tsk_resolveContacts->setDependencies({tsk_updatePersistantContacts,tsk_generateNewContacts});
			tsk_resolveContacts->fullyInitialized = true;

			// post-update tick actors
			auto tsk_postPhysicsTickActors = tsk_physicsFrame->createSubTask();
			tsk_postPhysicsTickActors->workFunction = [this](ThreadTaskReference thisTask) {
				Tickable::tickGroups(TickGroup::POST_PHYSICS,physicsFrameTimeDelta);
			};
			tsk_postPhysicsTickActors->affinity |= ThreadFlags::ENTITY_UPDATE;
			tsk_postPhysicsTickActors->setDependencies({tsk_resolveContacts});
			tsk_postPhysicsTickActors->fullyInitialized = true;
		}

		tsk_physicsFrame->fullyInitialized = true;
		tsk_sys_physicsFramePrev = tsk_physicsFrame;
	}

	_physicsTimeToSimulate -= engine_duration(numPhysicsUpdatesThisFrame * physicsFrameTimeDelta);
}

void World::threadProc(ThreadTaskReference thisTask) {
	if(!isPaused()) {
		auto tsk_sys_world = thisTask->createSubTask();
		; {
			auto tsk_updateAI = tsk_sys_world->createSubTask();
			tsk_updateAI->workFunction = std::bind(&World::updateAI,this,tsk_updateAI);
			tsk_updateAI->fullyInitialized = true;

			auto tsk_updateEntities = tsk_sys_world->createSubTask();
			tsk_updateEntities->workFunction = std::bind(&World::updateActors,this,tsk_updateEntities);
			tsk_updateEntities->setDependencies({tsk_updateAI});
			tsk_updateEntities->fullyInitialized = true;

			auto tsk_updateSpacialPartitions = tsk_sys_world->createSubTask();
			tsk_updateSpacialPartitions->workFunction = std::bind(&World::updateSpacialPartitions,this,tsk_updateSpacialPartitions);
			tsk_updateSpacialPartitions->setDependencies({tsk_updateEntities});
			tsk_updateSpacialPartitions->fullyInitialized = true;

			auto tsk_updatePhysics = tsk_sys_world->createSubTask();
			tsk_updatePhysics->workFunction = std::bind(&World::updatePhysics,this,tsk_updatePhysics);
			tsk_updatePhysics->setDependencies({tsk_updateSpacialPartitions});
			tsk_updatePhysics->fullyInitialized = true;
		}
		tsk_sys_world->fullyInitialized = true;
	}
}

const pool_vector<Actor*>& World::getActors() const {
	return _actors;
}

void World::setActiveCamera(CameraComponent* camera) {
	_activeCamera = camera;
}

CameraComponent* World::getActiveCamera() const {
	if(_activeCamera == nullptr) {
		// try to find an existing camera
		auto potentialCameras = Object::filterObjectsByPredicate([](const Object* obj)->bool {
			return dynamic_cast<const CameraComponent*>(obj) != nullptr;
		});

		if(potentialCameras.size() > 0) return (CameraComponent*)potentialCameras[0];
	}
	return _activeCamera;
}

void World::setActivePlayer(APlayer* player) {
	_activePlayer = player;
}

APlayer* World::getActivePlayer() const {
	if(_activePlayer == nullptr) {
		// try to find an existing camera
		auto potentialPlayers = Object::filterObjectsByPredicate([](const Object* obj)->bool {
			return dynamic_cast<const APlayer*>(obj) != nullptr;
		});

		if(potentialPlayers.size() > 0) return (APlayer*)potentialPlayers[0];
	}
	return _activePlayer;
}

PhysicsIntegrator* World::getPhysicsIntegrator() const {
	return _physicsIntegrator;
}

ContactGenerator* World::getContactGenerator() const {
	return _contactGenerator;
}

ContactResolver* World::getContactResolver() const {
	return _contactResolver;
}
