#pragma once

#include <vector>

#include <fmod.hpp>
#include "core\Engine.h"
#include "graphics\Graphics.h"

#include "core\Singleton.h"

class soundManager : public singleton::Singleton<soundManager>
{
private:
	FMOD::System * m_FMOD;

	FMOD::Channel * m_musicChannel;
	FMOD::Channel * m_FXChannel;

	std::vector<FMOD::Sound*> m_songs;
	std::vector<FMOD::Sound*> m_sounds;

	bool m_isPaused;
	double m_volume;
	int m_currentSong;

public:
	soundManager();

	void initialize();
	void loadSounds();

	void update();
	void playCurrentSong();
	void playSong(int songNum);
	void playSoundAt(Vector3 * pos, int songIndex);

	void switchPaused();

	// SETTERS
	void setVolume(double vol) { m_volume = vol; }
	void setPaused(bool paused) { m_isPaused = paused; }

	// GETTERS
	bool isPaused() { return m_isPaused; }
	FMOD::System * getSystem() { return m_FMOD; }
	FMOD::Sound * getSong(int songNum) { return m_songs[songNum]; }
	FMOD::Channel * getMusicChannel() { return m_musicChannel; }
};