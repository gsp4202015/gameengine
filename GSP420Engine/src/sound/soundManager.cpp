#include "soundManager.h"

soundManager::soundManager()
{

}

void soundManager::initialize()
{
	FMOD::System_Create(&m_FMOD);
	m_FMOD->init(32, FMOD_INIT_NORMAL, 0);
	
	m_isPaused = false;
	m_volume = 0.09f;
	m_currentSong = 0;
	
	loadSounds();
	playSong(m_currentSong);
	m_musicChannel->setVolume(m_volume);
	m_musicChannel->setPaused(m_isPaused);
}

void soundManager::loadSounds()
{
	// Go to www.freesound.org
	FMOD::Sound * tempSound;

	m_FMOD->createStream("data/assets/music/So Sick Instrumental.mp3", FMOD_SOFTWARE | FMOD_LOOP_NORMAL, 0, &tempSound);
	m_songs.push_back(tempSound);

	m_FMOD->createStream("data/assets/music/The Moment We Come Alive.wma", FMOD_SOFTWARE | FMOD_LOOP_NORMAL, 0, &tempSound);
	m_songs.push_back(tempSound);

	m_FMOD->createStream("data/assets/music/Reckoning.mp3", FMOD_SOFTWARE | FMOD_LOOP_NORMAL, 0, &tempSound);
	m_songs.push_back(tempSound);


	m_FMOD->createSound("data/assets/music/Reckoning.mp3", FMOD_LOOP_NORMAL | FMOD_3D, 0, &tempSound);
}

void soundManager::update()
{
	m_FMOD->update();

	const CameraComponent* earComponent = EngineSettings::Instance().system.activeWorld->getActiveCamera();
	const Matrix earTransform = earComponent->getComponentToWorld();
	const Vector3 earPosition = earTransform.Translation();
	const Vector3 earForward = earTransform.Forward();
	const Vector3 earUp = earTransform.Up();
	const Vector3 earVelocity = earComponent->getExtrapolatedVelocity();

	FMOD_VECTOR fm_earPosition = {VEC3TOARG(earPosition)};
	FMOD_VECTOR fm_earForward = {VEC3TOARG(earForward)};
	FMOD_VECTOR fm_earUp = {VEC3TOARG(earUp)};
	FMOD_VECTOR fm_earVelocity = {VEC3TOARG(earVelocity)};

	m_FMOD->set3DListenerAttributes(0,&fm_earPosition,&fm_earVelocity,&fm_earForward,&fm_earUp);
}

void soundManager::playSong(int songNum)
{
	m_musicChannel->stop();
	m_FMOD->playSound(FMOD_CHANNEL_FREE, m_songs[songNum], m_isPaused, &m_musicChannel);
}

void soundManager::playCurrentSong()
{
	m_musicChannel->stop();
	m_FMOD->playSound(FMOD_CHANNEL_FREE, m_songs[m_currentSong], m_isPaused, &m_musicChannel);
}

void soundManager::playSoundAt(Vector3 * pos, int songIndex)
{
	FMOD_VECTOR vel = {0,0,0};
	FMOD_VECTOR pos1 = { pos->x, pos->y, pos->z };
	m_FXChannel->set3DAttributes(&pos1, &vel);
	
	m_FMOD->playSound(FMOD_CHANNEL_FREE, m_sounds[songIndex], false, &m_FXChannel);
}

void soundManager::switchPaused()
{
	if (m_isPaused)
		m_isPaused = false;

	else
		m_isPaused = true;

	m_musicChannel->setPaused(m_isPaused);
}