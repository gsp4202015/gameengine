#include "ASkybox.h"

#include "core\Engine.h"
#include "graphics\Graphics.h"

ASkybox::ASkybox(SkyboxMesh *mesh,ID3D11Device *device) {
	auto texture = EngineSettings::Instance().graphics.textureManager->GetTexture("skymap");
	mesh->InitializeBuffers(device);

	auto skyboxRenderer = constructObject<RenderableComponent>(this,STR("render"),new Skybox(texture,mesh));
	rootComponent = skyboxRenderer;
	//rootComponent->transform.scale = Vector3(-1,-1,-1);

	tickGroup = TickGroup::PRE_GRAPHICS;
}

ASkybox::~ASkybox() {

}

void ASkybox::onTick(uint32 groups,engine_duration deltaTime) {
	auto activeCamera = getWorld()->getActiveCamera();
	if(activeCamera != nullptr) {
		rootComponent->transform.position = activeCamera->getComponentToWorld().Translation();
	}
}
