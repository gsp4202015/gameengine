#include "APlayer.h"

#include "core\Engine.h"
#include "graphics\Graphics.h"
#include "physics\Physics.h"

#include <iostream>

APlayer::APlayer(class DXTexture2D* texture,class Mesh* mesh) {
	auto renderComponent = constructObject<RenderableComponent>(this,STR("render"),new Cube(texture,mesh));

// 	auto renderComponent2 = constructObject<RenderableComponent>(this,STR("render"),new Cube(texture,mesh));
// 	renderComponent2->transform.position = Vector3(0,50,-100);
// 	renderComponent2->transform.rotation = Quaternion::CreateFromRotationMatrix(lookDirection(Vector3::UnitZ,-renderComponent2->transform.position));
// 	renderComponent2->transform.scale = Vector3(10,10,10);
// 
// 	auto renderComponent3 = constructObject<RenderableComponent>(this,STR("render"),new Cube(texture,mesh));
// 	renderComponent3->absoluteScale = true;
// 	renderComponent3->transform.position = Vector3(0,50,-100);
// 	renderComponent3->transform.scale = Vector3(10,10,10);

	auto collider = constructObject<SphereColliderComponent>(this,STR("collider"),2.0f);
	collider->physicalMaterial = EngineSettings::Instance().physics.getPhysicalMaterial(STR("entity"));

	auto rigidBody = constructObject<RigidBodyComponent>(this,STR("rigidBody"),1.5f);
	rootComponent = rigidBody;
	rigidBody->setCollider(collider);
	//rigidBody->gravityMultiplier = 0.0f;
	//rigidBody->angularVelocity.y = 0.2f;

	controlTransformComponent = constructObject<ActorComponent>(this,STR("movementTransform"));
	controlTransformComponent->absoluteScale = BoolVector3::TTT;
	controlTransformComponent->absoluteRotation.x = true;
	controlTransformComponent->absoluteRotation.z = true;

	myCamera = constructObject<CameraComponent>(this,STR("camera"));
	myCamera->attachTo(controlTransformComponent);
	myCamera->transform.position = Vector3(0,50,-100);
	myCamera->setLookAt(Vector3(0,0,0));

	// SET PLAYER DATA
	playerData.acceleration = 90.0f;
	playerData.burnBarPercent = 0.0f;
	playerData.healthPercent = 1.0f;

	tickGroup = TickGroup::ENTITY;
}

APlayer::~APlayer() {

}

#include "core\Engine.h"
#include "graphics\Graphics.h"

void APlayer::onTick(uint32 groups,engine_duration deltaTime) {
	auto &input = EngineSettings::Instance().system.input;

	const float dt = (float)deltaTime.count();

	// if our input is enabled
	if(myCamera == getWorld()->getActiveCamera()) {
		const Matrix matMovement = controlTransformComponent->getComponentToWorld();

		Quaternion test = Quaternion::CreateFromRotationMatrix(removeScale(rootComponent->getComponentToWorld()));

		if(input.getKeyIsPressed('W')) {
			rigidBody->velocity += playerData.acceleration * dt * matMovement.Forward() * -1.0f;
		}

		if(input.getKeyIsPressed('S')) {
			rigidBody->velocity += playerData.acceleration * dt * matMovement.Backward() * -1.0f;
		}

		if(input.getKeyIsPressed('D')) {
			rigidBody->velocity += playerData.acceleration * dt * matMovement.Right();
		}

		if(input.getKeyIsPressed('A')) {
			rigidBody->velocity += playerData.acceleration * dt * matMovement.Left();
		}

		auto mouseMotion = input.getMouseMotion();

		if(mouseMotion != MousePoint::Zero) {
			const Quaternion frameYaw = Quaternion::CreateFromAxisAngle(Vector3::UnitY,mouseMotion.x * 0.0025f);

			controlTransformComponent->transform.rotation = controlTransformComponent->transform.rotation * frameYaw;
		}
	}
}
