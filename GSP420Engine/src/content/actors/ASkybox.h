#pragma once

#include "core\Actor.h"
#include "graphics\DX11\Mesh\SkyboxMesh.h"
#include "core\Tickable.h"

class ASkybox : public Actor, public Tickable {
public:
	ASkybox(SkyboxMesh *mesh,ID3D11Device *device);
	virtual ~ASkybox();

	virtual void onTick(uint32 groups,engine_duration deltaTime);
};
