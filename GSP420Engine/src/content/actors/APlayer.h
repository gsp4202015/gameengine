#pragma once

#include "core\Actor.h"
#include "core\Tickable.h"

class APlayer : public Actor, public Tickable {
public:
	struct {
		float acceleration;

		float burnBarPercent;
		float healthPercent;
	} playerData;

	class CameraComponent* myCamera;
	ActorComponent* controlTransformComponent;

	APlayer(class DXTexture2D* texture,class Mesh* mesh);
	virtual ~APlayer();

	virtual void onTick(uint32 groups,engine_duration deltaTime);
};
