#ifndef AVOIDSTATEAI_H
#define AVOIDSTATEAI_H
#include "BaseAIState.h"
class Actor;

class AvoidStateAI : public BaseAIState<Actor>
{
	//Holds the Actor that the player is currently controlling 
	Actor *mPlayerActor;
public:
	AvoidStateAI();
	AvoidStateAI(Actor *playerActor);
	~AvoidStateAI();

	void Enter(Actor *owner);
	void Execute(Actor *owner, float dt);
	void Exit(Actor *owner);

};

#endif