#include "AI.h"
#include "..\core\Actor.h"
#include "..\physics\RigidBodyComponent.h"

AvoidStateAI::AvoidStateAI()
{
	//Insert code to where we can automatically grab the player actor from the list of actors
}

AvoidStateAI::AvoidStateAI(Actor *playerActor)
{
	mPlayerActor = playerActor;
}

AvoidStateAI::~AvoidStateAI()
{
	mPlayerActor = nullptr;
}

void AvoidStateAI::Enter(Actor *owner)
{
	assert(mPlayerActor != nullptr);
}

void AvoidStateAI::Execute(Actor *owner, float dt)
{
	Vector3 runAway = owner->rigidBody->transform.position - mPlayerActor->rigidBody->transform.position;
	runAway.Normalize();
	runAway *= 20;

	float distance = Vector3::DistanceSquared(owner->rigidBody->transform.position, mPlayerActor->rigidBody->transform.position);

	if(distance >= 625.0f)
		owner->AI->ChangeState(new WanderStateAI(mPlayerActor));
}

void AvoidStateAI::Exit(Actor *owner)
{
	mPlayerActor = nullptr;
}