#ifndef PURSUESTATEAI_H
#define PURSUESTATEAI_H
#include "BaseAIState.h"
class Actor;

class PursueStateAI : public BaseAIState<Actor>
{
	Actor *mPlayerActor;
	float mTimeWithinCloseField;
	bool mWillAlwaysAttack;

public:
	PursueStateAI();
	PursueStateAI(Actor *playerActor);
	~PursueStateAI();

	void Enter(Actor *owner);
	void Execute(Actor *owner, float dt);
	void Exit(Actor *owner);

	bool ShouldAttack(Actor *owner, float dt);
};


#endif