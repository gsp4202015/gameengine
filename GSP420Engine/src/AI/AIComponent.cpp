#include "AIComponent.h"
#include "..\core\Actor.h"

void AIComponent::SetState(BaseAIState<Actor> *state)
{
	//if the currentState is null then just go ahead and set the state
	if(mCurrentState == nullptr)
	{
		mCurrentState = state;
		mCurrentState->Enter(dynamic_cast<Actor*>(getOwner()));
	}

	//Otherwise, exit out of the current state and assign the state to it
	else
	{
		mCurrentState->Exit(dynamic_cast<Actor*>(getOwner()));
		delete mCurrentState;
		mCurrentState = state;
		mCurrentState->Enter(dynamic_cast<Actor*>(getOwner()));
	}
}

void AIComponent::tick(float dt)
{
	mCurrentState->Execute(dynamic_cast<Actor*>(getOwner()), dt);
}

void AIComponent::ChangeState(BaseAIState<Actor> *state)
{
	mCurrentState->Exit(dynamic_cast<Actor*>(getOwner()));
	delete mCurrentState;
	mCurrentState = state;
	mCurrentState->Enter(dynamic_cast<Actor*>(getOwner()));
}