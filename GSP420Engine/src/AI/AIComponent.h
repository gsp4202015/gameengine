#ifndef AICOMPONENT_H
#define AICOMPONENT_H
#include "..\core\ActorComponent.h"
#include "BaseAIState.h"
class Actor;

class AIComponent : public ActorComponent
{

	//Holds the current State the Actor is in
	BaseAIState<Actor> *mCurrentState;

public:

	AIComponent() { mCurrentState = nullptr; };
	~AIComponent() { mCurrentState = nullptr; };

	void SetState(BaseAIState<Actor> *state);
	void tick(float dt);
	void ChangeState(BaseAIState<Actor> *state);

	//Accessors 
	BaseAIState<Actor>* GetCurrentState() { return mCurrentState; };

};

#endif