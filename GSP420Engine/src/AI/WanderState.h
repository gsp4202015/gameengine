#ifndef WANDERSTATE_H
#define WANDERSTATE_H
#include "BaseAIState.h"
class Actor;

class WanderStateAI : public BaseAIState<Actor>
{
	Actor *mPlayerActor;
	float mWanderAngle;		//This will hold the objects current angle change
	float mAngleChange;		//This will hold how much the angle every frame

public:

	WanderStateAI(Actor *player);
	~WanderStateAI();

	void Enter(Actor *owner);
	void Execute(Actor *owner, float dt);
	void Exit(Actor *owner);

	bool CheckToChangeState(Actor *owner);
};

#endif