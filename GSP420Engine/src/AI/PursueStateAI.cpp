#include "AI.h"
#include "..\core\Actor.h"
#include "..\physics\RigidBodyComponent.h"
#include "..\core\World.h"

PursueStateAI::PursueStateAI()
{

}

PursueStateAI::PursueStateAI(Actor *playerActor)
{
	mPlayerActor = playerActor;
}

PursueStateAI::~PursueStateAI()
{
	mPlayerActor = nullptr;
}

void PursueStateAI::Enter(Actor *owner)
{
	assert(mPlayerActor != nullptr);	//Make sure that there is a player actor attached to the pointer
	mTimeWithinCloseField = 0.0f;
	mWillAlwaysAttack = false;
}

void PursueStateAI::Execute(Actor *owner, float dt)
{
	////We grab the normals 
	Vector3 towardsPlayer = mPlayerActor->rigidBody->transform.position - owner->rigidBody->transform.position;
	Vector3 towardsEnemy = towardsPlayer * -1;

	towardsPlayer.Normalize();
	towardsEnemy.Normalize();

	////We will check to see if they are close enough In the Far Field
	float distance = Vector3::DistanceSquared(mPlayerActor->rigidBody->transform.position, owner->rigidBody->transform.position);
	float radiusSq = 60 + 60;
	radiusSq *= radiusSq;

	//If we are too far then we will change back to wander
	if(distance >= radiusSq)
		owner->AI->ChangeState(new WanderStateAI(mPlayerActor));

	if(distance <= radiusSq)
	{
		radiusSq = 30 + 25;
		radiusSq *= radiusSq;

		if(distance <= radiusSq)
		{
			radiusSq = 15 + 10;
			radiusSq *= radiusSq;

			if(distance <= radiusSq)
				if(ShouldAttack(owner, dt))
					owner->rigidBody->velocity += towardsPlayer * (7 * 3) * dt;
				else;
			else
				owner->rigidBody->velocity += towardsPlayer * (7 * 2) * dt;
			
			owner->rigidBody->velocity += towardsPlayer * (7 * 1) * dt;
		}
	}
}

void PursueStateAI::Exit(Actor *owner)
{
	mPlayerActor = nullptr;
	mWillAlwaysAttack = false;
}

bool PursueStateAI::ShouldAttack(Actor *owner, float dt)
{
	//We will add an accumalated time. It should only increment if it stays within 
	mTimeWithinCloseField += dt;

	//Once that accumulated time goes over 3 seconds, meaning that the enemy was within the 
	//Players field for a time of greater than 3 seoncds
	if(mTimeWithinCloseField >= 3.0f)
	{
		//then we give the order to attack the player
		//mTimeWithinCloseField = 0.0f;
		return true;
	}


// 	owner->rigidBody->velocity = Vector3(0.0f, 0.0f, 0.0f);
// 
// 	Vector3 towardsPlayer = mPlayerActor->rigidBody->transform.position - owner->rigidBody->transform.position;
// 	
// 	//Otherwise we will update the position so that it doesn't go flying away within the 3 seconds
// 	//And that it will jitter in place as to warn the player that it will be attacked
// 	owner->rigidBody->velocity = towardsPlayer * (25 * .3f);

	return mWillAlwaysAttack;;
}