#ifndef BASEAISTATE_H
#define BASEAISTATE_H

template<typename EntityType>
class BaseAIState
{

public:

	virtual void Enter(EntityType*) = 0;
	virtual void Execute(EntityType*, float dt) = 0;
	virtual void Exit(EntityType*) = 0;
};

#endif