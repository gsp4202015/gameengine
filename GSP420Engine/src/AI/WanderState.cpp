#include "AI.h"
#include "..\core\Actor.h"
#include "..\physics\RigidBodyComponent.h"
#include <math.h>
#include <time.h>

WanderStateAI::WanderStateAI(Actor *player)
{
	mPlayerActor = player;
}

WanderStateAI::~WanderStateAI()
{
	mPlayerActor = nullptr;
}

void WanderStateAI::Enter(Actor *owner)
{
	mWanderAngle = 8  / 3.14;
	mAngleChange = 1 / 3.14;
	srand(900);
}

void WanderStateAI::Execute(Actor *owner, float dt)
{
	//Calculate the circle Center
	Vector3 circleCenter = owner->rigidBody->velocity;
	circleCenter.Normalize();
	circleCenter *= 50.0f;

	//displacement Angle
	Vector3 displacement = Vector3(1.0f, 0.0f, -1.0f);
	displacement *= 10.0f;


	float length = displacement.Length();
	displacement.x = cos(mWanderAngle) * length;
	displacement.z = sin(mWanderAngle) * length;

	mWanderAngle += rand() * mAngleChange - mAngleChange * .5f;

	Vector3 wanderForce = circleCenter + displacement;

	//Before we add a force to the object, we will add a force to influence towards the player object.
	Vector3 towardsPlayer = mPlayerActor->rigidBody->transform.position - owner->rigidBody->transform.position;
	towardsPlayer.Normalize();
	towardsPlayer *= 10;

	wanderForce += towardsPlayer;

	owner->rigidBody->velocity += wanderForce * dt * 0.25f;

	//If we are close enough we will start pursuring the target
	if(CheckToChangeState(owner))
		owner->AI->ChangeState(new PursueStateAI(mPlayerActor));
}

void WanderStateAI::Exit(Actor *owner)
{
	mPlayerActor = nullptr;
}

bool WanderStateAI::CheckToChangeState(Actor *owner)
{
	float distance = Vector3::DistanceSquared(mPlayerActor->rigidBody->transform.position, owner->rigidBody->transform.position);
	float radiusSq = 120 * 120;

	return distance <= radiusSq;
}