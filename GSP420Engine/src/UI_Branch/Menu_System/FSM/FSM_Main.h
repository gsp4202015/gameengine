#pragma once

#include <string>
#include <vector>
#include <memory>
#include "../../../core/Engine.h"

class FSM;

class FSM_State {
private:


public:
	FSM_State() {};
	FSM_State(FSM *fsm) {};
	virtual ~FSM_State() {};

	//Virtual Methods that each inherited state must update
	virtual void InitializeState() = 0;
	virtual void UpdateScene(float dt) {};
	virtual void RenderScene() {};
	virtual void onResetDevice() {};
	virtual void onLostDevice() {};
	virtual void LeaveState() {};

	
	std::string stateName;  //used to switch between states

	//creates the FSM Variable
	FSM *myFsm;
};

//A vector of shared pointers housing all the states in the machine
typedef std::vector< std::tr1::shared_ptr<FSM_State> > StateBank;

class FSM {
public:
	FSM();
	~FSM();
	void Update(const float& dt);

	void TransitionTo(std::string stateName);
	void AddState(FSM_State *newState, bool makeCurrent);
	std::string GetState();

public:
	FSM_State *currentState;

	//Bank to house list of states
	StateBank stateBank;
	std::vector< std::tr1::shared_ptr<FSM_State> >::iterator iter;
};
