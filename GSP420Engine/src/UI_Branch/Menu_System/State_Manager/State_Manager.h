#pragma once

#include <map>
#include <vector>
#include <string>
#include "../FSM/FSM_Main.h"


class State_Manager {
private:
	std::vector<FSM_State*> m_VectorOfStates;
	std::map<std::string, FSM_State*> m_MapOfStates;

public:
	State_Manager(void);
	~State_Manager(void);

	FSM *m_pMyFSM;

	//registers the states
	void switchState(std::string);

};

