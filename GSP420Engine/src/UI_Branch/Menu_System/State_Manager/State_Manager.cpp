#include "State_Manager.h"
#include "../FSM/FSM_Main.h"
#include "../States/Menu_Main.h"
#include "../States/Menu_Options.h"


State_Manager::State_Manager(void) {
	m_pMyFSM = new FSM();
}


State_Manager::~State_Manager(void) {
	delete m_pMyFSM;
	for(auto it : m_VectorOfStates) {
		delete it;
	}
}

void State_Manager::switchState(std::string name) {
	auto retVal = m_MapOfStates.find(name);
	if(retVal == m_MapOfStates.end()) {
		if(m_pMyFSM->currentState != NULL) {
			m_pMyFSM->currentState->LeaveState();
		}
		//add states here to make them work
		if(name == "main_Menu") {
			FSM_State* temp = new Menu_Main();
			m_MapOfStates.insert(std::make_pair(name, temp));
			m_VectorOfStates.push_back(temp);
			temp->InitializeState();
			m_pMyFSM->currentState = temp;
		}

		
		if(name == "Options_Menu") {
			FSM_State* temp = new Menu_Options();
			m_MapOfStates.insert(std::make_pair(name, temp));
			m_VectorOfStates.push_back(temp);
			temp->InitializeState();
			m_pMyFSM->currentState = temp;
		}


	}


	else {
		if(retVal->second != m_pMyFSM->currentState) {
			if(m_pMyFSM->currentState != NULL) {
				m_pMyFSM->currentState->LeaveState();
			}
			retVal->second->InitializeState();
			m_pMyFSM->currentState = retVal->second;
		}
	}
}

