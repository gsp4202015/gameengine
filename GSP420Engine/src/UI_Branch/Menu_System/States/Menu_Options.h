#pragma once

#include "../FSM/FSM_Main.h"


class Menu_Options : public FSM_State {
private:
	//creates a vector to store the Options Buttons


public:
	Menu_Options(void);
	Menu_Options(FSM *fsm);
	~Menu_Options(void);

	void onResetDevice();
	void onLostDevice();

	void UpdateScene(float dt);
	void RenderScene();

	void InitializeState();
	void LeaveState();



};
