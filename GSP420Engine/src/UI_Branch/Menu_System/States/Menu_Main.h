#pragma once
#include "../FSM/FSM_Main.h"




class Menu_Main : public FSM_State {
private:
	
public:

	Menu_Main(void);
	Menu_Main(FSM *fsm);
	~Menu_Main(void);

	void onResetDevice();
	void onLostDevice();

	void UpdateScene(float dt);
	void RenderScene();

	void InitializeState();
	void LeaveState();

	

};

