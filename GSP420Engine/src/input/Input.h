#pragma once

#include <windows.h>
#include <chrono>

#include <array>

#include "core\EngineTypes.h"

class MousePoint {
public:// STATIC CONSTNATS
	static MousePoint Zero;

public:
	int32 x,y;

	inline MousePoint() : x(0),y(0) {}
	inline MousePoint(int32 x,int32 y) : x(x),y(y) {}
	inline MousePoint(const MousePoint &point) : x(point.x),y(point.y) {}
	inline MousePoint(const POINT &point) : x(point.x),y(point.y) {}

	inline operator POINT() const { POINT ret; ret.x = x; ret.y = y; return ret; }
	inline operator Vector2() const { return Vector2(static_cast<float>(x),static_cast<float>(y)); }

	inline bool operator==(const MousePoint &r) const { return x==r.x && y==r.y; }
	inline bool operator!=(const MousePoint &r) const { return !(*this == r); }

	inline MousePoint& operator+=(const MousePoint &r) { x += r.x; y += r.y; return *this; }
	inline MousePoint& operator-=(const MousePoint &r) { x -= r.x; y -= r.y; return *this; }
	inline MousePoint& operator*=(uint32 r) { x *= r; y *= r; return *this; }
	inline MousePoint& operator/=(uint32 r) { x /= r; y /= r; return *this; }

	inline MousePoint operator+(const MousePoint &r) const { MousePoint ret(*this); ret += r; return ret; }
	inline MousePoint operator-(const MousePoint &r) const { MousePoint ret(*this); ret -= r; return ret; }
	inline MousePoint operator*(uint32 r) const { MousePoint ret(*this); ret *= r; return ret; }
	inline MousePoint operator/(uint32 r) const { MousePoint ret(*this); ret /= r; return ret; }
};

class Input {
private:
	struct _KeyData {
		bool pressedThisFrame;
		bool isPressed;
		bool releasedThisFrame;
		engine_time_point tpLastPressed;
		engine_time_point tpLastReleased;
	};

	std::array<_KeyData,256> keyData;

	struct {
		MousePoint previousPosition;
		MousePoint currentPosition;
		bool isFpsMode;
		MousePoint fpsModePosition;
	} mouse;

public:
	void updateInput();

	Input();
	~Input();

	//////////////////////////////////////////////////////////////////////////
	// GENEREAL
	//////////////////////////////////////////////////////////////////////////

	// FPS mode keeps the mouse cursor within the game window and hides it
	void setMouseFPSMode(bool fpsmode);
	bool getMouseFPSMode() const;

	//////////////////////////////////////////////////////////////////////////
	// KEYS
	//////////////////////////////////////////////////////////////////////////

	// gets whether or not the given key is currently pressed
	bool getKeyIsPressed(WPARAM key) const;

	// gets whether or not the given key was pressed during this frame 
	bool getKeyDown(WPARAM key) const;

	// gets whether or not the given key was held down during this frame, DOES NOT FIRE ON THE FIRST FRAME THAT A KEY IS PRESSED
	bool getKeyHeld(WPARAM key) const;

	// gets the duration that the given key has been held
	engine_duration getKeyHeldTime(WPARAM key) const;

	// gets whether or not the given key was released during this frame 
	bool getKeyUp(WPARAM key) const;

	//////////////////////////////////////////////////////////////////////////
	// MOUSE
	//////////////////////////////////////////////////////////////////////////

	// gets the screen-space coordinates of the mouse cursor
	MousePoint getMousePostion() const;

	// gets the mouse movement over the last frame
	MousePoint getMouseMotion() const;

	// sets the screen-space coordinates of the mouse
	void setMousePosition(const MousePoint &pos);
};
