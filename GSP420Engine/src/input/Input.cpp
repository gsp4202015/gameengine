#include "Input.h"

#include "core\EngineSettings.h"
#include "graphics\DX11\System\DXApp.h"

MousePoint MousePoint::Zero(0,0);

void Input::updateInput() {
	; {
		POINT tmp = getMousePostion();
		GetCursorPos(&tmp);
		ScreenToClient(EngineSettings::Instance().system.application->GetWindowHWND(),&tmp);

		mouse.previousPosition = mouse.currentPosition;
		mouse.currentPosition = tmp;
	}

	if(mouse.isFpsMode) {
		ShowCursor(FALSE);
	} else {
		ShowCursor(TRUE);
	}

	if(mouse.currentPosition != mouse.previousPosition) {
		if(mouse.isFpsMode) {
			setMousePosition((MousePoint)EngineSettings::Instance().system.application->GetWindowSize() / 2);
		}
	}

	for(uint32 i = 0; i < keyData.size(); i++) {
		auto &key = keyData[i];

		key.pressedThisFrame = key.releasedThisFrame = false;

		const bool isKeyPressed = (GetAsyncKeyState(i)&0x8000) != 0;
		if(isKeyPressed) {
			if(!key.isPressed) {
				key.tpLastPressed = engine_clock::now();
				key.pressedThisFrame = true;
			}
		} else {
			if(key.isPressed) {
				key.tpLastReleased = engine_clock::now();
				key.releasedThisFrame = true;
			}
		}
		key.isPressed = isKeyPressed;
	}
}

Input::Input() {
	mouse.isFpsMode = false;

	// init key data
	for(auto &key : keyData) {
		key.pressedThisFrame = key.isPressed = key.releasedThisFrame = false;
	}
}

Input::~Input() {

}

bool Input::getMouseFPSMode() const {
	return mouse.isFpsMode;
}

void Input::setMouseFPSMode(bool fpsmode) {
	if(mouse.isFpsMode != fpsmode) {
		if(fpsmode) {
			mouse.fpsModePosition = getMousePostion();
			setMousePosition((MousePoint)EngineSettings::Instance().system.application->GetWindowSize() / 2);
		} else {
			setMousePosition(mouse.fpsModePosition);
		}
	}
	mouse.isFpsMode = fpsmode;
}

bool Input::getKeyIsPressed(WPARAM key) const {
	assert(key >= 0 && key < keyData.size());
	return keyData[key].isPressed;
}

bool Input::getKeyDown(WPARAM key) const {
	assert(key >= 0 && key < keyData.size());
	return keyData[key].pressedThisFrame;
}

bool Input::getKeyHeld(WPARAM key) const {
	assert(key >= 0 && key < keyData.size());
	return !keyData[key].pressedThisFrame && keyData[key].isPressed;
}

engine_duration Input::getKeyHeldTime(WPARAM key) const {
	assert(key >= 0 && key < keyData.size());
	if(!keyData[key].isPressed) return keyData[key].tpLastReleased - keyData[key].tpLastPressed;
	return engine_clock::now() - keyData[key].tpLastPressed;
}

bool Input::getKeyUp(WPARAM key) const {
	assert(key >= 0 && key < keyData.size());
	return keyData[key].releasedThisFrame;
}

MousePoint Input::getMousePostion() const {
	return mouse.currentPosition;
}

MousePoint Input::getMouseMotion() const {
	return mouse.currentPosition - mouse.previousPosition;
}

void Input::setMousePosition(const MousePoint &pos) {
	POINT pos_screen = pos;
	ClientToScreen(EngineSettings::Instance().system.application->GetWindowHWND(),&pos_screen);
	SetCursorPos(pos_screen.x,pos_screen.y);

	mouse.previousPosition = pos - getMouseMotion();
	mouse.currentPosition = pos;
}
