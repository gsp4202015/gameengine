#include "ImpulseContactResolver.h"

#include "physics\RigidBodyComponent.h"
#include "physics\collider\ColliderComponent.h"

#include "core\World.h"

float ImpulseContactResolver::calculateDesiredDeltaVelocity(Contact* contact) {
	float velocityFromAcceleration = 0;

	for(uint32 i = 0; i < 2; i++) {
		const float sign = (float)(i==0 ? 1 : -1);

		const RigidBodyComponent *rigidBody = contact->colliders[i]->rigidBody;
		const Vector3 &bodyLastFrameAcceleration = (rigidBody != nullptr ? (rigidBody->getLastFrameAcceleration()) : (Vector3::Zero));

		velocityFromAcceleration += bodyLastFrameAcceleration | contact->normal;
	}

	return -contact->velocityAtContact_contact.x - (contact->coefRestitution * (contact->velocityAtContact_contact.x - velocityFromAcceleration));
}

Vector3 ImpulseContactResolver::calculateImpulse(Contact* contact) {
	const MassProperties* massProperties[2] = {
		&contact->colliders[0]->getMassProperties(),
		&contact->colliders[1]->getMassProperties()
	};

	// accumulators
	float totalMass_inverse = 0;
	Matrix deltaVel_world(0,0,0,0,
						  0,0,0,0,
						  0,0,0,0,
						  0,0,0,0);

	// accumulate data
	for(uint32 i = 0; i < 2; i++) {
		if(massProperties[i]->inverseMass == 0) continue;

		const Matrix impulseToTorque = skewSymmetric(contact->relativeContactPosition[i]); // matrix form of a vector cross-product
		const Matrix myDeltaVelWorld = -1.0f * impulseToTorque * massProperties[i]->inertialTensor_inverse * impulseToTorque;

		deltaVel_world += myDeltaVelWorld;

		totalMass_inverse += massProperties[i]->inverseMass;
	}

	// currently represents the change in velocity due to rotation only
	Matrix deltaVel_contact = contact->matContactToWorld * deltaVel_world * contact->matWorldToContact;
	// include the change due to linear motion
	deltaVel_contact._11 += totalMass_inverse;
	deltaVel_contact._22 += totalMass_inverse;
	deltaVel_contact._33 += totalMass_inverse;

	const Matrix impulsePerUnitVelocity = deltaVel_contact.Invert();

	// find the velocity to kill
	const Vector3 velocityToKill(contact->resolution_impulse.desiredDeltaVelocity,
								 -contact->velocityAtContact_contact.y,
								 -contact->velocityAtContact_contact.z);

	// find the impulse required to kill this velocity
	Vector3 impulse_contact = velocityToKill * impulsePerUnitVelocity;

	// check to see if our tangential impulse is large enough to overcome static friction
	const float planarImpulse = sqrt(SQ(impulse_contact.y) + SQ(impulse_contact.z));
	if(false && planarImpulse > impulse_contact.x * contact->coefFriction) {
		// it is, calculate impulse with dynamic friction
		impulse_contact.y /= planarImpulse;
		impulse_contact.z /= planarImpulse;
		impulse_contact.x =
			impulsePerUnitVelocity._11 +
			impulsePerUnitVelocity._12 * contact->coefFriction * impulse_contact.x +
			impulsePerUnitVelocity._13 * contact->coefFriction * impulse_contact.x;

		impulse_contact.x = contact->resolution_impulse.desiredDeltaVelocity / impulse_contact.x;
		impulse_contact.y *= contact->coefFriction * impulse_contact.x;
		impulse_contact.z *= contact->coefFriction * impulse_contact.x;
	}

	return impulse_contact * contact->matContactToWorld;
}

void ImpulseContactResolver::resolveContactCluster(ThreadTaskReference thisTask,ProcItemsCont &cluster) {
	// TODO: resolve contacts in the order from their distance from a static object and from the direction of gravity
	// TODO: implement "weak welds" for contacts with static objects

	// calculate the internal information required for velocity iterations
	for(auto &contact : cluster) {
		contact->calculateDerivedData();
		contact->resolution_impulse.desiredDeltaVelocity = calculateDesiredDeltaVelocity(contact);
	}

	// first fix the inter-penetration
	for(uint16 iteration = 0; iteration < EngineSettings::Instance().physics.solver.maximumPenetrationIterations; iteration++) {
		// find the contact with the highest penetration
		Contact* contactWithHighestePenetration = nullptr;
		for(auto &contact : cluster) {
			if(contactWithHighestePenetration == nullptr || contact->penetration > contactWithHighestePenetration->penetration) {
				contactWithHighestePenetration = contact;
			}
		}

		// if a contact with a large enough penetration was not found, we are done
		if(contactWithHighestePenetration == nullptr || contactWithHighestePenetration->penetration <= EngineSettings::Instance().physics.solver.epsilon_penetration) break;

		const MassProperties* massProperties[2] = {
			&contactWithHighestePenetration->colliders[0]->getMassProperties(),
			&contactWithHighestePenetration->colliders[1]->getMassProperties()
		}

		; {
			float totalInertia = 0;
			float linearInertia[2];
			float angularInertia[2];

			// calculate inertia of objects in normal direction
			for(uint32 i = 0; i < 2; i++) {
				if(contactWithHighestePenetration->colliders[i]->rigidBody == nullptr) continue;

				Vector3 angularInertiaWorld = contactWithHighestePenetration->relativeContactPosition[i] % contactWithHighestePenetration->normal;
				angularInertiaWorld = angularInertiaWorld * massProperties[i]->inertialTensor_inverse;
				angularInertiaWorld = angularInertiaWorld % contactWithHighestePenetration->normal;
				angularInertia[i] = angularInertiaWorld | contactWithHighestePenetration->normal;

				linearInertia[i] = massProperties[i]->inverseMass;

				totalInertia += linearInertia[i] + angularInertia[i];
			}

			for(uint32 i = 0; i < 2; i++) {
				auto* rigidBody = contactWithHighestePenetration->colliders[i]->rigidBody;
				if(rigidBody == nullptr) continue;

				const float sign = (float)(i==0 ? 1 : -1);
				float linearMovementMagnitude = sign * contactWithHighestePenetration->penetration * (linearInertia[i] / totalInertia);
				float angularMovementMagnitude = sign * contactWithHighestePenetration->penetration * (angularInertia[i] / totalInertia);

				// limit angular movement to keep high mass/low angular inertia objects from behaving strangely
				float angularMovementLimit = 0.2f * contactWithHighestePenetration->relativeContactPosition[i].Length();
				if(abs(angularMovementMagnitude) > angularMovementLimit) {
					float totalMove = linearMovementMagnitude + angularMovementMagnitude;
					angularMovementMagnitude = CLAMP(angularMovementMagnitude,-angularMovementLimit,angularMovementLimit);
					linearMovementMagnitude = totalMove - angularMovementMagnitude;
				}

				// calculate linear and angular deltas
				Vector3 linearDelta = contactWithHighestePenetration->normal * linearMovementMagnitude;
				Vector3 angularDelta = Vector3::Zero;
				if(angularMovementMagnitude != 0) {
					const Vector3 rotationDirection = contactWithHighestePenetration->relativeContactPosition[i] % contactWithHighestePenetration->normal;
					angularDelta = (rotationDirection * massProperties[i]->inertialTensor_inverse) * (angularMovementMagnitude / angularInertia[i]);
				}

				// apply the changes
				rigidBody->transform.position += linearDelta;
				rigidBody->transform.rotation = MathUtilities::quatAddAngularOffset(rigidBody->transform.rotation,angularDelta);
				rigidBody->transform.rotation.Normalize();

				// propagate the changes among all effected contacts
				for(auto &contact : cluster) {
					for(uint32 colliderInd = 0; colliderInd < 2; colliderInd++) {
						if(contact->colliders[colliderInd] == contactWithHighestePenetration->colliders[i]) {
							const Vector3 deltaPosition = (linearDelta + (angularDelta % contact->relativeContactPosition[colliderInd]));
							const float sign = (float)(colliderInd==0 ? -1 : 1);
							contact->penetration += sign * (deltaPosition | contact->normal);
							if(colliderInd == 0) {
								contact->point += deltaPosition;
							}
						}
					}
				}
			}
		}
	}

	// fix velocities
	for(uint16 iteration = 0; iteration < EngineSettings::Instance().physics.solver.maximumVelocityIterations; iteration++) {
		// find the contact with the lowest contact velocity
		Contact* contactWithHighestDesiredDeltaVelocity = nullptr;
		for(auto &contact : cluster) {
			if(contactWithHighestDesiredDeltaVelocity == nullptr || contact->resolution_impulse.desiredDeltaVelocity < contactWithHighestDesiredDeltaVelocity->resolution_impulse.desiredDeltaVelocity) {
				contactWithHighestDesiredDeltaVelocity = contact;
			}
		}

		// if a contact with a large enough penetration was not found, we are done
		if(contactWithHighestDesiredDeltaVelocity == nullptr || contactWithHighestDesiredDeltaVelocity->resolution_impulse.desiredDeltaVelocity >= -EngineSettings::Instance().physics.solver.epsilon_velocity) break;

		const MassProperties* massProperties[2] = {
			&contactWithHighestDesiredDeltaVelocity->colliders[0]->getMassProperties(),
			&contactWithHighestDesiredDeltaVelocity->colliders[1]->getMassProperties()
		}

		; {
			const Vector3 impulse = calculateImpulse(contactWithHighestDesiredDeltaVelocity);

			for(uint32 i = 0; i < 2; i++) {
				RigidBodyComponent* body = contactWithHighestDesiredDeltaVelocity->colliders[i]->rigidBody;
				if(body == nullptr) continue;

				const float sign = (i == 0 ? 1.0f : -1.0f);

				const Vector3 velDelta = sign * (impulse * massProperties[i]->inverseMass);
				body->velocity += velDelta;

				const Vector3 avelDelta = sign * ((contactWithHighestDesiredDeltaVelocity->relativeContactPosition[i] % impulse) * massProperties[i]->inertialTensor_inverse);
				body->angularVelocity += avelDelta;

				// propagate the changes among all effected contacts
				for(auto &contact : cluster) {
					bool contactEffected = false;
					for(uint32 colliderInd = 0; colliderInd < 2; colliderInd++) {
						if(contact->colliders[colliderInd] == contactWithHighestDesiredDeltaVelocity->colliders[i]) {
							contactEffected = true;
							const Vector3 deltaPosition = velDelta + (avelDelta % contact->relativeContactPosition[colliderInd]);
							contact->velocityAtContact_world += (float)(colliderInd==0 ? -1 : 1) * deltaPosition;
							contact->velocityAtContact_contact = contact->velocityAtContact_world * contact->matWorldToContact;
							contact->resolution_impulse.desiredDeltaVelocity = calculateDesiredDeltaVelocity(contact);
						}
					}
				}
			}
		}
	}
}

void ImpulseContactResolver::buildContactClusters() {
	contactClusters.clear();

	// do a pass over the contacts to null all of the collider's contact cluster pointers
	for(auto &contact : myWorld->contacts) {
		for(uint32 i = 0; i < 2; i++) contact.colliders[i]->myContactCluster = nullptr;
	}

	// build the contact clusters
	for(auto &contact : myWorld->contacts) {
		// if either collider's cluster is null, and it is dynamic, a new cluster is created and assigned
		for(uint32 i = 0; i < 2; i++) {
			if(contact.colliders[i]->myContactCluster == nullptr && contact.colliders[i]->isDynamic()) {
				contactClusters.emplace_back();
				contact.colliders[i]->myContactCluster = &contactClusters.back();
			}
		}

		ColliderComponent* const colliderA = contact.colliders[0];
		ColliderComponent* const colliderB = contact.colliders[1];

		std::list<Contact*>* clusterA = reinterpret_cast<std::list<Contact*>*>(colliderA->myContactCluster);
		std::list<Contact*>* clusterB = reinterpret_cast<std::list<Contact*>*>(colliderB->myContactCluster);

		if(clusterA == nullptr || clusterB == nullptr) {
			// at least one of the colliders is static, thus no splicing will occur
			if(clusterA != nullptr) clusterA->push_back(&contact);
			if(clusterB != nullptr) clusterB->push_back(&contact);
		} else {
			if(clusterA != clusterB) {
				// both colliders are dynamic and in separate clusters, merge both clusters
				clusterA->splice(clusterA->end(),*clusterB);
				colliderB->myContactCluster = clusterB = clusterA;
			}
			clusterA->push_back(&contact);
		}
	}

	// remove all empty clusters
	contactClusters.remove_if([](const std::list<Contact*> &item)->bool {
		return item.size() == 0;
	});
}

ImpulseContactResolver::ImpulseContactResolver() {
}

ImpulseContactResolver::~ImpulseContactResolver() {
}

void ImpulseContactResolver::threadProc(ThreadTaskReference thisTask,World* world) {
	__super::threadProc(thisTask,world);

	buildContactClusters();

	// make a separate thread task for every contact cluster
	// TODO: distribute the contact clusters around into a more optimal number of
	//       thread tasks based on the number of contacts in each one

	for(auto &cluster : contactClusters) {
		auto tsk_worker = thisTask->createSubTask();
		ProcItemsCont procItems(cluster.begin(),cluster.end());
		tsk_worker->workFunction = std::bind(&ImpulseContactResolver::resolveContactCluster,this,std::placeholders::_1,std::move(procItems));
		tsk_worker->fullyInitialized = true;
	}
}