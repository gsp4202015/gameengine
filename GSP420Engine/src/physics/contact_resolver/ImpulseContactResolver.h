#pragma once

#include "ContactResolver.h"

#include <vector>
#include <list>

class Contact;

class ImpulseContactResolver : public ContactResolver {
protected:
	typedef std::vector<Contact*> ProcItemsCont;

	std::list<std::list<Contact*>> contactClusters;

	float calculateDesiredDeltaVelocity(Contact* contact);
	Vector3 calculateImpulse(Contact* contact);

	void buildContactClusters();
	void resolveContactCluster(ThreadTaskReference thisTask,ProcItemsCont &cluster);
public:
	ImpulseContactResolver();
	virtual ~ImpulseContactResolver();

	virtual void threadProc(ThreadTaskReference thisTask,World* world);
};
