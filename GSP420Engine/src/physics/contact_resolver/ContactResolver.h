#pragma once

#include "core\ThreadSpooler.h"
#include "core\Object.h"

#include <vector>

class World;

class ContactResolver : public Object {
protected:
	World* myWorld;

public:
	ContactResolver();
	virtual ~ContactResolver();

	virtual void threadProc(ThreadTaskReference thisTask,World* world);
};
