#pragma once

#include "core\EngineTypes.h"

#include <algorithm>

class ColliderComponent;

class Contact {
private:
	bool derivedDataCalculated : 1;

public:
	bool active : 1;

	// INDEPENDENT
	Vector3 point; // point is on the surface of A
	Vector3 normal; // points from B to A
	float penetration;
	ColliderComponent *colliders[2];

	// DERIVED
	Matrix matContactToWorld;
	Matrix matWorldToContact;
	Vector3 velocityAtContact_world;
	Vector3 velocityAtContact_contact;
	Vector3 relativeContactPosition[2];
	float coefRestitution;
	float coefFriction;

	// USED IN IMPULSE CONTACT RESOLUTION
	struct {
		float desiredDeltaVelocity;
	} resolution_impulse;

	void invert();
	void calculateDerivedData();
};