#include "RigidBodyComponent.h"

#include "collider\ColliderComponent.h"

void RigidBodyComponent::clearAccumulators() {
	accumulatedForce = Vector3::Zero;
	accumulatedTorque = Vector3::Zero;
}

RigidBodyComponent::RigidBodyComponent(float density) :
density(density),
gravityMultiplier(1.0f),
_awake(true) {
}

RigidBodyComponent::~RigidBodyComponent() {

}

void RigidBodyComponent::setCollider(ColliderComponent* collider) {
	assert(collider->rigidBody == nullptr);
	if(myCollider != nullptr) {
		myCollider->detach();
		myCollider->rigidBody = nullptr;
	}
	myCollider = collider;
	myCollider->rigidBody = this;
}

ColliderComponent* RigidBodyComponent::getCollider() const {
	return myCollider;
}

DirectX::SimpleMath::Vector3 RigidBodyComponent::getLastFrameAcceleration() const {
	return lastFrameAcceleration;
}

bool RigidBodyComponent::isAwake() const {
	return _awake;
}

void RigidBodyComponent::addForce(const Vector3 &force) {
	accumulatedForce += force;
}

void RigidBodyComponent::addForceAtPointWorld(const Vector3 &force,const Vector3 &point) {
	const Transform componentToWorld = getComponentToWorld();
	const Vector3 comToPoint = point - componentToWorld.position;

	accumulatedForce += force;
	accumulatedTorque += comToPoint % force;
}

void RigidBodyComponent::addForceAtPointBody(const Vector3 &force,const Vector3 &point) {
	const Vector3 point_world = point * getComponentToWorld();
	addForceAtPointWorld(force,point_world);
}

void RigidBodyComponent::addTorque(const Vector3 &torque) {
	accumulatedTorque += torque;
}
