#include "Contact.h"

#include "collider\ColliderComponent.h"
#include "material\PhysicalMaterial.h"
#include "RigidBodyComponent.h"

void Contact::invert() {
	normal *= -1.0f;
	point += normal * penetration;
	std::swap(colliders[0],colliders[1]);

	if(derivedDataCalculated) calculateDerivedData();
}

void Contact::calculateDerivedData() {
	assert(colliders[0] != nullptr && colliders[1] != nullptr);
	assert(colliders[0]->physicalMaterial != nullptr && colliders[1]->physicalMaterial != nullptr);

	matContactToWorld = Matrix::Identity;
	matContactToWorld.Right(normal);
	MathUtilities::orthoNormalize(&matContactToWorld);
	matContactToWorld.Transpose(matWorldToContact);

	auto lam_calculateRelativeContactPosition = [&](uint8 colliderInd) {
		const Vector3 colliderCenterOfMass = colliders[colliderInd]->getComponentToWorld().Translation();
		relativeContactPosition[colliderInd] = point - colliderCenterOfMass;
	};

	auto lam_calculateLocalVelocity = [&](uint8 colliderInd) {
		const RigidBodyComponent *rigidBody = colliders[colliderInd]->rigidBody;
		if(rigidBody == nullptr) return;

		const Vector3 &bodyLinearVelocity = rigidBody->velocity;
		const Vector3 &bodyAngularVelocity = rigidBody->angularVelocity;
		const Vector3 &bodyAccelerationLastFrame = rigidBody->getLastFrameAcceleration();
		const Vector3 &localVelocity_world =
			(bodyAngularVelocity % relativeContactPosition[colliderInd]) + bodyLinearVelocity -
			((bodyAccelerationLastFrame | normal) * normal);

		velocityAtContact_world += (colliderInd == 0 ? -1.0f : 1.0f) * localVelocity_world;
	};

	const float restitutionLow = (std::min)(colliders[0]->physicalMaterial->restitution,colliders[1]->physicalMaterial->restitution);
	const float restitutionHigh = (std::max)(colliders[0]->physicalMaterial->restitution,colliders[1]->physicalMaterial->restitution);

	const float frictionLow = (std::min)(colliders[0]->physicalMaterial->friction,colliders[1]->physicalMaterial->friction);
	const float frictionHigh = (std::max)(colliders[0]->physicalMaterial->friction,colliders[1]->physicalMaterial->friction);

	const float targetRestitution = lerp(restitutionLow,restitutionHigh,0.15f);
	const float targetFriction = lerp(frictionLow,frictionHigh,0.15f);

	auto lam_calculateCoefficients = [&](uint8 colliderInd) {
		// is the velocity so low that restitution should be ignored (to reduce jitter)
		if(velocityAtContact_contact.x < EngineSettings::Instance().physics.minimumVelocityForRestitution) {
			coefRestitution = 0.0f;
		} else {
			coefRestitution = targetRestitution;
		}

		coefFriction = targetFriction;
	};

	// initialize values
	velocityAtContact_world = Vector3::Zero;
	velocityAtContact_contact = Vector3::Zero;

	// calculate derived data
	for(uint8 i = 0; i < 2; i++) {
		lam_calculateRelativeContactPosition(i);
		lam_calculateLocalVelocity(i);
		velocityAtContact_contact = velocityAtContact_world * matWorldToContact;
		lam_calculateCoefficients(i);
	}

	derivedDataCalculated = true;
}
