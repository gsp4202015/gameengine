#include "EulerIntegrator.h"
#include "physics\RigidBodyComponent.h"

#include "physics\collider\ColliderComponent.h"
#include "core\Actor.h"
#include "core\World.h"

void EulerIntegrator::integrateBodies(ThreadTaskReference thisTask,ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd) {
	const float deltaTime = (float)myWorld->physicsFrameTimeDelta.count();
	for(auto it = itBegin; it != itEnd; it++) {
		RigidBodyComponent* body = *it;
		if(body->myCollider == nullptr) continue;

		const auto &massProperties = body->myCollider->getMassProperties();

		if(massProperties.inverseMass != 0 && body->isAwake()) {
			// helper data for contact resolution
			body->lastFrameAcceleration = EngineSettings::Instance().physics.gravity * body->gravityMultiplier;
			body->lastFrameAcceleration += body->accumulatedForce * massProperties.inverseMass;

			body->lastFrameAcceleration *= deltaTime;

			// euler integration
			const Vector3 accelerationThisFrame = body->lastFrameAcceleration;
			const Vector3 angularAccelerationThisFrame = body->accumulatedTorque * massProperties.inertialTensor_inverse;

			body->velocity += accelerationThisFrame;
			body->angularVelocity += angularAccelerationThisFrame;

			// damping
			body->velocity *= pow(EngineSettings::Instance().physics.damping.linear,deltaTime);
			body->angularVelocity *= pow(EngineSettings::Instance().physics.damping.angular,deltaTime);

			// apply transform changes
			body->transform.position += body->velocity * deltaTime;
			body->transform.rotation = MathUtilities::quatAddAngularOffset(body->transform.rotation,body->angularVelocity * deltaTime);
			body->transform.rotation.Normalize();

			body->clearAccumulators();
		}
	}
}

void EulerIntegrator::threadProc(ThreadTaskReference thisTask,World* world) {
	__super::threadProc(thisTask,world);

	// find all rigid bodies in world
	procItems.clear();
	for(auto actor : world->getActors()) {
		if(actor->rigidBody != nullptr) procItems.push_back(actor->rigidBody);
	}

	const uint32 numItems = (uint32)procItems.size();
	const unsigned numWorkers = (std::min)(EngineSettings::Instance().system.threadSpooler.getOptiomalNumConcurrentTasks(),numItems);
	//assert(numWorkers > 0);

	if(numItems > 0) {
		const uint32 blockSize = (uint32)std::ceil((float)numItems / (float)numWorkers);
		for(uint32 i = 0; i < numWorkers; i++) {
			const uint32 indStart = blockSize * i;
			const uint32 indEnd = (std::min)(numItems-1,indStart + blockSize) + 1;

			auto tsk_worker = thisTask->createSubTask();
			tsk_worker->workFunction = std::bind(&EulerIntegrator::integrateBodies,this,std::placeholders::_1,procItems.begin()+indStart,procItems.begin()+indEnd);
			tsk_worker->fullyInitialized = true;
		}
	}
}
