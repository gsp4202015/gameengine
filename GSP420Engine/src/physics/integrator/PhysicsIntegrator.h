#pragma once

#include "core\ThreadSpooler.h"
#include "core\Object.h"

#include <vector>

class World;

class PhysicsIntegrator : public Object {
protected:
	World* myWorld;

public:
	PhysicsIntegrator();
	virtual ~PhysicsIntegrator();

	virtual void threadProc(ThreadTaskReference thisTask,World* world);
};
