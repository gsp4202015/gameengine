#pragma once

#include "PhysicsIntegrator.h"

class World;
class RigidBodyComponent;

class EulerIntegrator : public PhysicsIntegrator {
private:
	typedef std::vector<RigidBodyComponent*> ProcItemsCont;
	ProcItemsCont procItems;

	void integrateBodies(ThreadTaskReference thisTask,ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd);

public:
	virtual void threadProc(ThreadTaskReference thisTask,World* world);
};