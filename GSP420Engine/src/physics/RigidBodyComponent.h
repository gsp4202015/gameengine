#pragma once

#include "core\ActorComponent.h"

class ColliderComponent;

class RigidBodyComponent : public ActorComponent {
	friend class EulerIntegrator;

private:
	bool _awake : 1;
	Vector3 lastFrameAcceleration;
	Vector3 accumulatedForce;
	Vector3 accumulatedTorque;
	ColliderComponent* myCollider;

	void clearAccumulators();

public:
	float density;
	float gravityMultiplier;
	Vector3 velocity;
	Vector3 angularVelocity;

	RigidBodyComponent(float density);
	virtual ~RigidBodyComponent();

	// accessors / mutators
	void setCollider(ColliderComponent* collider);
	ColliderComponent* getCollider() const;

	Vector3 getLastFrameAcceleration() const;
	bool isAwake() const;

	// physical interaction
	void addForce(const Vector3 &force);
	void addForceAtPointWorld(const Vector3 &force,const Vector3 &point);
	void addForceAtPointBody(const Vector3 &force,const Vector3 &point);
	void addTorque(const Vector3 &torque);
};
