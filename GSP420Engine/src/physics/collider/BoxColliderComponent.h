#pragma once

#include "ColliderComponent.h"

class BoxColliderComponent : public ColliderComponent {
private:
	Vector3 _halfSize;

protected:
	virtual void calculateMassProperties(MassProperties *massProperties) const;

public:
	BoxColliderComponent(const Vector3 &halfSize);
	virtual ~BoxColliderComponent();
	void resetCollider(Vector3 halfSize);

	inline Vector3 getHalfSize() const { return _halfSize; }
	inline void setRadius(const Vector3 &halfSize) { resetCollider(halfSize); }

	virtual Vector3 getFurthestPointInDirection(const Vector3 &direction) const;
	virtual float getMaximumUnscaledRadius() const;
};
