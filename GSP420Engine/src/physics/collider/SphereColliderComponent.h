#pragma once

#include "ColliderComponent.h"

class SphereColliderComponent : public ColliderComponent {
private:
	float _radius;

protected:
	virtual void calculateMassProperties(MassProperties *massProperties) const;

public:
	SphereColliderComponent(float radius);
	virtual ~SphereColliderComponent();
	void resetCollider(float radius);

	inline float getRadius() const { return _radius; }
	inline void setRadius(float radius) { resetCollider(radius); }

	virtual Vector3 getFurthestPointInDirection(const Vector3 &direction) const;
	virtual float getMaximumUnscaledRadius() const;
};
