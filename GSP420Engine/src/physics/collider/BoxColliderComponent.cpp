#include "BoxColliderComponent.h"

#include "core\Utility.h"
#include "..\RigidBodyComponent.h"

BoxColliderComponent::BoxColliderComponent(const Vector3 &halfSize) {
	resetCollider(halfSize);
}

BoxColliderComponent::~BoxColliderComponent() {

}

void BoxColliderComponent::resetCollider(Vector3 halfSize) {
	_massPropertiesDirty = true;
	_halfSize = halfSize;
}

Vector3 BoxColliderComponent::getFurthestPointInDirection(const Vector3 &direction) const {
	const float skinWidth = EngineSettings::Instance().physics.collisionSkinWidth;
	const Transform toWorld = getComponentToWorld();
	const Vector3 actualDirection = Vector3::Transform(direction,toWorld.rotation.Inverse()).GetNormal();

	const Vector3 extentInDirection(
		SIGN(actualDirection.x)*_halfSize.x,
		SIGN(actualDirection.y)*_halfSize.y,
		SIGN(actualDirection.z)*_halfSize.z);

	const Vector3 ret = Vector3::Transform(extentInDirection + actualDirection*skinWidth,toWorld.getMatrix());

	return ret;
}

void BoxColliderComponent::calculateMassProperties(MassProperties *massProperties) const {
	float density;

	const Transform componentToWorld = getComponentToWorld();
	const Vector3 sizes = componentToWorld.scale * 2 * _halfSize;

	// if this collider is not paired with a rigid body is is considered STR("static") and thus has infinite mass
	if(rigidBody != nullptr) {
		density = rigidBody->density;
	} else {
		density = 0;
	}

	float mass;

	// calculate mass based on density
	if(density > 0) {
		const float volume = sizes.x * sizes.y * sizes.z;
		mass = volume * density;
		massProperties->inverseMass = 1.0f / mass;
	} else {
		mass = 0;
		massProperties->inverseMass = 0;
	}

	massProperties->centerOfMass = Vector3::Zero;

	if(mass > 0) {
		massProperties->inertialTensor_inverse = Matrix::Identity;
		const Vector3 squares(
			SQ(sizes.x),
			SQ(sizes.y),
			SQ(sizes.z)
			);
		massProperties->inertialTensor_inverse._11 = 1.f/12.f * mass * (squares.y + squares.z);
		massProperties->inertialTensor_inverse._22 = 1.f/12.f * mass * (squares.x + squares.z);
		massProperties->inertialTensor_inverse._33 = 1.f/12.f * mass * (squares.x + squares.y);

		massProperties->inertialTensor_inverse = massProperties->inertialTensor_inverse.Invert();
	} else {
		massProperties->inertialTensor_inverse = Matrix(
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,1
			);
	}
}

float BoxColliderComponent::getMaximumUnscaledRadius() const {
	return _halfSize.Length();
}

