#pragma once

#include "core\ActorComponent.h"
#include "core\Engine.h"

#include <unordered_map>

class PhysicalMaterial;
class RigidBodyComponent;

struct MassProperties {
	float inverseMass;
	Vector3 centerOfMass;
	Matrix inertialTensor_inverse;
};

class ColliderComponent : public ActorComponent {
private:
	mutable MassProperties _massProperties;

protected:
	mutable bool _massPropertiesDirty : 1;
	mutable float _cache_getMassProperties_rigidBody_density;

	virtual void calculateMassProperties(MassProperties *massProperties) const = 0;

public:
	bool isTrigger : 1;
	bool broadcastCollisionEvents : 1;
	uint8 collisionLayer;
	RigidBodyComponent *rigidBody;
	PhysicalMaterial *physicalMaterial;
	void* myContactCluster;

	ColliderComponent();
	virtual ~ColliderComponent();

	const MassProperties& getMassProperties() const;
	virtual bool isStatic() const;
	virtual bool isDynamic() const;

	virtual Vector3 getFurthestPointInDirection(const Vector3 &direction) const = 0;
	virtual float getMaximumUnscaledRadius() const = 0;
};