#include "SphereColliderComponent.h"

#include "..\RigidBodyComponent.h"

SphereColliderComponent::SphereColliderComponent(float radius) {
	resetCollider(radius);
}

SphereColliderComponent::~SphereColliderComponent() {

}

void SphereColliderComponent::resetCollider(float radius) {
	_massPropertiesDirty = true;
	_radius = radius;
}

DirectX::SimpleMath::Vector3 SphereColliderComponent::getFurthestPointInDirection(const Vector3 &direction) const {
	const float skinWidth = EngineSettings::Instance().physics.collisionSkinWidth;
	const Transform toWorld = getComponentToWorld();
	const Vector3 actualDirection = Vector3::Transform(direction,toWorld.rotation.Inverse()).GetNormal();

	const Vector3 ret = Vector3::Transform(_radius*actualDirection + actualDirection*skinWidth,toWorld.getMatrix());

	return ret;
}

void SphereColliderComponent::calculateMassProperties(MassProperties *massProperties) const {
	float density;

	const Transform componentToWorld = getComponentToWorld();
	const Vector3 radii = Vector3(_radius,_radius,_radius) * componentToWorld.scale;

	// if this collider is not paired with a rigid body is is considered STR("static") and thus has infinite mass
	if(rigidBody != nullptr) {
		density = rigidBody->density;
	} else {
		density = 0;
	}

	float mass;

	// calculate mass based on density
	if(density > 0) {
		const float volume = 4.0f/3.0f * PI * radii.x * radii.y * radii.z;
		mass = volume * density;
		massProperties->inverseMass = 1.0f / mass;
	} else {
		mass = 0;
		massProperties->inverseMass = 0;
	}

	massProperties->centerOfMass = Vector3::Zero;

	if(mass > 0) {
		massProperties->inertialTensor_inverse = Matrix::Identity;
		const float s = 0.4f * mass * _radius * _radius;
		massProperties->inertialTensor_inverse._11 = 0.2f * mass * (SQ(radii.y) + SQ(radii.z));
		massProperties->inertialTensor_inverse._22 = 0.2f * mass * (SQ(radii.x) + SQ(radii.z));
		massProperties->inertialTensor_inverse._33 = 0.2f * mass * (SQ(radii.x) + SQ(radii.y));

		massProperties->inertialTensor_inverse = massProperties->inertialTensor_inverse.Invert();
	} else {
		massProperties->inertialTensor_inverse = Matrix(
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,1
			);
	}
}

float SphereColliderComponent::getMaximumUnscaledRadius() const {
	return _radius;
}
