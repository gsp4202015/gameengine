#include "ColliderComponent.h"

#include "..\RigidBodyComponent.h"

ColliderComponent::ColliderComponent() {
	isTrigger = false;
	broadcastCollisionEvents = false;
	collisionLayer = EngineSettings::Instance().physics.collisionMatrix.getCollisionLayerByName(STR("default"));
	_massPropertiesDirty = true;
	physicalMaterial = EngineSettings::Instance().physics.getPhysicalMaterial(STR("default"));
	rigidBody = nullptr;
	_cache_getMassProperties_rigidBody_density = 0;
}

ColliderComponent::~ColliderComponent() {

}

const MassProperties& ColliderComponent::getMassProperties() const {
	; {
		const float currentDensity = (rigidBody!=nullptr ? rigidBody->density : 0);
		if(_cache_getMassProperties_rigidBody_density != currentDensity) {
			_cache_getMassProperties_rigidBody_density = currentDensity;
			_massPropertiesDirty = true;
		}
	}
	if(_massPropertiesDirty) {
		_massPropertiesDirty = false;
		calculateMassProperties(&_massProperties);
	}
	return _massProperties;
}

bool ColliderComponent::isStatic() const {
	return rigidBody == nullptr || rigidBody->getCollider() != this || getMassProperties().inverseMass == 0;
}

bool ColliderComponent::isDynamic() const {
	return !isStatic();
}
