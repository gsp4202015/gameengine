#include "GJKEPAGenerator.h"

#include "core\World.h"
#include "physics\Contact.h"
#include "physics\collider\ColliderComponent.h"
#include "physics\RigidBodyComponent.h"

#include <numeric>

void GJKEPAGenerator::processCollisionTests(ThreadTaskReference thisTask,ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd) {
	decltype(myWorld->contacts.begin()) currentTargetContact;

	auto lam_freshenTargetContact = [&]() {
		std::lock_guard<decltype(myWorld->mtx_contacts)> lck(myWorld->mtx_contacts);
		currentTargetContact = myWorld->contacts.emplace();
	};

	for(auto it = itBegin; it != itEnd; it++) {
		auto &pendingTest = *it;

		if(!currentTargetContact) lam_freshenTargetContact();

		ting::shared_lock<decltype(myWorld->mtx_contacts)> lck_noModify(myWorld->mtx_contacts);
		if(performTest(pendingTest[0],pendingTest[1],&*currentTargetContact)) {
			currentTargetContact = decltype(currentTargetContact)(); // void the current target contact
		}
	}

	// if the target contact iterator is still valid that means that no contact was generated during the final test, we must clear it from the contact vector
	if(currentTargetContact) {
		std::lock_guard<decltype(myWorld->mtx_contacts)> lck(myWorld->mtx_contacts);
		myWorld->contacts.remove(currentTargetContact);
	}
}

GJKEPAGenerator::GJKEPAGenerator() {
	procItemsSizeOverTime_insertInd = 0;
}

GJKEPAGenerator::~GJKEPAGenerator() {

}

void GJKEPAGenerator::threadProc(ThreadTaskReference thisTask,World* world) {
	__super::threadProc(thisTask,world);

	// try to avoid unnecessary reallocation of the procItems vector
	procItemsSizeOverTime[procItemsSizeOverTime_insertInd++ % procItemsSizeOverTime.size()] = procItems.size();
	const uint32 averageNumProcItemsOverLastFewFrames = std::accumulate(procItemsSizeOverTime.begin(),procItemsSizeOverTime.end(),0,std::plus<uint32>()) / procItemsSizeOverTime.size();
	// build a list of tests to perform
	// TODO: implement octree culling
	procItems.clear();
	procItems.reserve((uint32)(averageNumProcItemsOverLastFewFrames * 1.25f));

	// clear all existing contacts
	// TODO: implement persistant contacts
	myWorld->contacts.clear();

	auto allColliderComponents = Object::filterObjectsByPredicate([](const Object* obj)->bool {
		return dynamic_cast<const ColliderComponent*>(obj) != nullptr;
	});

	//procItems.reserve(N_CHOOSE_R(allColliderComponents.size(),2));

	for(uint32 i = 0; i < allColliderComponents.size(); i++) {
		ColliderComponent* colliderA = (ColliderComponent*)allColliderComponents[i];
		for(uint32 j = i+1; j < allColliderComponents.size(); j++) {
			ColliderComponent* colliderB = (ColliderComponent*)allColliderComponents[j];

			// are both colliders static?
			if(colliderA->isStatic() && colliderB->isStatic()) continue;

			// are both colliders dynamic, but asleep?
			if((colliderA->isDynamic() && !colliderA->rigidBody->isAwake()) &&
			   (colliderB->isDynamic() && !colliderB->rigidBody->isAwake())) continue; // ignore collisions where neither party is dynamic and not asleep

			// is collision between the two collider's layers disabled?
			if(!EngineSettings::Instance().physics.collisionMatrix.isCollisionEnabled(colliderA->collisionLayer,colliderB->collisionLayer)) continue;

			// are the bounding spheres around both colliders intersecting?
			; {
				const Transform transA = colliderA->getComponentToWorld();
				const Transform transB = colliderB->getComponentToWorld();
				const float radiusA = colliderA->getMaximumUnscaledRadius() * VEC3_MAX_ABS_SCALAR(transA.scale);
				const float radiusB = colliderB->getMaximumUnscaledRadius() * VEC3_MAX_ABS_SCALAR(transB.scale);

				if((transB.position - transB.position).Length() > (radiusA + radiusB + EngineSettings::Instance().physics.collisionSkinWidth * 2.0f)) continue;
			}

			procItems.emplace_back();
			procItems.back()[0] = colliderA;
			procItems.back()[1] = colliderB;
		}
	}

	const uint32 numItems = (uint32)procItems.size();
	const unsigned numWorkers = (std::min)(EngineSettings::Instance().system.threadSpooler.getOptiomalNumConcurrentTasks(),numItems);
	//assert(numWorkers > 0);

	if(numItems > 0) {
		const uint32 blockSize = (uint32)std::ceil((float)numItems / (float)numWorkers);
		for(uint32 i = 0; i < numWorkers; i++) {
			const uint32 indStart = blockSize * i;
			const uint32 indEnd = (std::min)(numItems-1,indStart + blockSize) + 1;

			auto tsk_worker = thisTask->createSubTask();
			tsk_worker->workFunction = std::bind(&GJKEPAGenerator::processCollisionTests,this,std::placeholders::_1,procItems.begin()+indStart,procItems.begin()+indEnd);
			tsk_worker->fullyInitialized = true;
		}
	}
}

#define gjk_simtest(v) (((v)|(ao)) > 0)
bool GJKEPAGenerator::performTest(ColliderComponent* colliderA,ColliderComponent* colliderB,Contact* outContact) {
	try {
		struct SupportPoint {
			Vector3 v;
			Vector3 sup_a;
			Vector3 sup_b;

			BOOL operator==(const SupportPoint &r) const { return v == r.v; }
		};

		struct Simplex {
		public:
			SupportPoint _simplex[4];
			int num;
			SupportPoint &a;
			SupportPoint &b;
			SupportPoint &c;
			SupportPoint &d;

			Simplex() : a(_simplex[0]),b(_simplex[1]),c(_simplex[2]),d(_simplex[3]) { clear(); }

			void clear() { num = 0; }

			void set(SupportPoint a,SupportPoint b,SupportPoint c,SupportPoint d) { num = 4; this->a = a; this->b = b; this->c = c; this->d = d; }
			void set(SupportPoint a,SupportPoint b,SupportPoint c) { num = 3; this->a = a; this->b = b; this->c = c; }
			void set(SupportPoint a,SupportPoint b) { num = 2; this->a = a; this->b = b; }
			void set(SupportPoint a) { num = 1; this->a = a; }

			void push(SupportPoint p) { num = (std::min)(num+1,4); for(int i = num-1; i > 0; i--) _simplex[i] = _simplex[i-1]; _simplex[0] = p; }
		};

		Simplex sim;

		auto lam_support = [&](Vector3 d)->SupportPoint {
			d.Normalize(); // to quell numerical instability
			SupportPoint ret;
			ret.sup_a = colliderA->getFurthestPointInDirection(d);
			ret.sup_b = colliderB->getFurthestPointInDirection(-d);
			ret.v = ret.sup_a - ret.sup_b;
			return ret;
		};

		//GJK algorithm, as explained by Casey here: http://mollyrocket.com/849, and here: http://vec3.ca/gjk/implementation/
		; {
			const unsigned _EXIT_ITERATION_LIMIT = 75;
			unsigned _EXIT_ITERATION_NUM = 0;

			// build the initial simplex, a single support point in an arbitrary position
			sim.clear();
			Vector3 dir = Vector3::UnitX;
			SupportPoint s = lam_support(dir);
			if(fabs(dir|s.v)>=s.v.Length()*0.8f) {
				// the chosen direction is invalid, will produce (0,0,0) for a subsequent direction later
				dir = Vector3::UnitY;
				s = lam_support(dir);
			}
			sim.push(s);
			dir = -s.v;

			// iteratively attempt to build a simplex that encloses the origin
			while(true) {
				if(_EXIT_ITERATION_NUM++ >= _EXIT_ITERATION_LIMIT) return false;

				// error, for some reason the direction vector is broken
				if(dir.LengthSquared()<=0.0001f) return false;

				// get the next point in the direction of the origin
				SupportPoint a = lam_support(dir);

				// early out: if a.v dot d is less than zero that means that the new point did not go past the origin
				// and thus there is no intersection
				if((a.v | dir) < 0) return false;
				sim.push(a);

				const Vector3 ao = -sim.a.v;

				// simplex tests
				if(sim.num == 2) {
					// simplex is a line, being here means that the early out was passed, and thus
					// the origin must be between point a and b
					// search direction is perpendicular to ab and coplanar with ao
					const Vector3 ab = (sim.b.v-sim.a.v);
					dir = ab % ao % ab;
					continue;
				} else if(sim.num == 3) {
					// simplex is a triangle, meaning that the origin must be
					const Vector3 ab = (sim.b.v-sim.a.v);
					const Vector3 ac = (sim.c.v-sim.a.v);
					const Vector3 ad = (sim.d.v-sim.a.v);
					const Vector3 abc = ab % ac;

					if(gjk_simtest(ab % abc)) {
						// origin is outside the triangle, near the edge ab
						// reset the simplex to the line ab and continue
						// search direction is perpendicular to ab and coplanar with ao
						sim.set(sim.a,sim.b);
						dir = ab % ao % ab;
						continue;
					}

					if(gjk_simtest(abc % ac)) {
						// origin is outside the triangle, near the edge ac
						// reset the simplex to the line ac and continue
						// search direction is perpendicular to ac and coplanar with ao
						sim.set(sim.a,sim.c);
						dir = ac % ao % ac;
						continue;
					}

					// origin is within the triangular prism defined by the triangle
					// determine if it is above or below
					if(gjk_simtest(abc)) {
						// origin is above the triangle, so the simplex is not modified,
						// the search direction is the triangle's face normal
						dir = abc;
						continue;
					}

					// origin is below the triangle, so the simplex is rewound the opposite direction
					// the search direction is the new triangle's face normal
					sim.set(sim.a,sim.c,sim.b);
					dir = -abc;
					continue;
				} else { // == 4
					// the simplex is a tetrahedron, must check if it is outside any of the side triangles, (abc, acd, adb)
					// if it is then set the simplex equal to that triangle and continue, otherwise we know
					// there is an intersection and exit

					// check the triangles (abc,acd,adb), scoped as the temporary variables used here
					// will no longer be valid afterward
					; {
						const Vector3 ab = (sim.b.v-sim.a.v);
						const Vector3 ac = (sim.c.v-sim.a.v);

						if(gjk_simtest(ab % ac)) {
							// origin is in front of triangle abc, simplex is already what it needs to be
							// go to jmp_face
							goto jmp_face;
						}

						const Vector3 ad = (sim.d.v-sim.a.v);

						if(gjk_simtest(ac % ad)) {
							// origin is in front of triangle acd, simplex is set to this triangle
							// go to jmp_face
							sim.set(sim.a,sim.c,sim.d);
							goto jmp_face;
						}

						if(gjk_simtest(ad % ab)) {
							// origin is in front of triangle adb, simplex is set to this triangle
							// go to jmp_face
							sim.set(sim.a,sim.d,sim.b);
							goto jmp_face;
						}

						// intersection confirmed, break from the loop
						break;
					}

					jmp_face:
					// the simplex is equal to the triangle that the origin is in front of
					// this is exactly the same as the triangular simplex test except that we know
					// that the origin is not behind the triangle
					const Vector3 ab = (sim.b.v-sim.a.v);
					const Vector3 ac = (sim.c.v-sim.a.v);
					const Vector3 abc = ab % ac;

					if(gjk_simtest(ab % abc)) {
						sim.set(sim.a,sim.b);
						dir = ab % ao % ab;
						continue;
					}

					if(gjk_simtest(abc % ac)) {
						sim.set(sim.a,sim.c);
						dir = ac % ao % ac;
						continue;
					}

					sim.set(sim.a,sim.b,sim.c);
					dir = abc;
					continue;
				}
			}

			// collision was detected
			if(outContact != nullptr) {
				//Expanding Polytope Algorithm (EPA) as described here: http://allenchou.net/2013/12/game-physics-contact-generation-epa/
				struct Triangle {
					SupportPoint points[3];
					Vector3 n;

					Triangle(const SupportPoint &a,const SupportPoint &b,const SupportPoint &c) {
						points[0] = a;
						points[1] = b;
						points[2] = c;
						n = getNormal((b.v-a.v) % (c.v-a.v));
					}
				};
				struct Edge {
					SupportPoint points[2];

					Edge(const SupportPoint &a,const SupportPoint &b) {
						points[0] = a;
						points[1] = b;
					}
				};

				const float _EXIT_THRESHOLD = 0.0001f;
				const unsigned _EXIT_ITERATION_LIMIT = 50;
				unsigned _EXIT_ITERATION_CUR = 0;

				std::list<Triangle> lst_triangles;
				std::list<Edge> lst_edges;

				// process the specified edge, if another edge with the same points in the
				// opposite order exists then it is removed and the new point is also not added
				// this ensures only the outermost ring edges of a cluster of triangles remain
				// in the list
				auto lam_addEdge = [&](const SupportPoint &a,const SupportPoint &b)->void {
					for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
						if(it->points[0]==b && it->points[1]==a) {
							//opposite edge found, remove it and do not add new one
							lst_edges.erase(it);
							return;
						}
					}
					lst_edges.emplace_back(a,b);
				};

				// add the GJK simplex triangles to the list
				lst_triangles.emplace_back(sim.a,sim.b,sim.c);
				lst_triangles.emplace_back(sim.a,sim.c,sim.d);
				lst_triangles.emplace_back(sim.a,sim.d,sim.b);
				lst_triangles.emplace_back(sim.b,sim.d,sim.c);

				while(true) {
					if(_EXIT_ITERATION_NUM++ >= _EXIT_ITERATION_LIMIT) return false;

					// find closest triangle to origin
					std::list<Triangle>::iterator entry_cur_triangle_it = lst_triangles.begin();
					float entry_cur_dst = FLT_MAX;
					for(auto it = lst_triangles.begin(); it != lst_triangles.end(); it++) {
						float dst = fabs(it->n | it->points[0].v);
						if(dst < entry_cur_dst) {
							entry_cur_dst = dst;
							entry_cur_triangle_it = it;
						}
					}

					const SupportPoint entry_cur_support = lam_support(entry_cur_triangle_it->n);
					if(((entry_cur_triangle_it->n|entry_cur_support.v) - entry_cur_dst < _EXIT_THRESHOLD)) {
						// calculate the barycentric coordinates of the closest triangle with respect to
						// the projection of the origin onto the triangle
						float bary_u,bary_v,bary_w;
						barycentric(entry_cur_triangle_it->n * entry_cur_dst,
									entry_cur_triangle_it->points[0].v,
									entry_cur_triangle_it->points[1].v,
									entry_cur_triangle_it->points[2].v,
									&bary_u,
									&bary_v,
									&bary_w);

						// barycentric can fail and generate invalid coordinates, if this happens return false
						if(!is_valid(bary_u) || !is_valid(bary_v) || !is_valid(bary_w)) return false;

						// collision point on object a in world space
						const Vector3 wcolpoint((bary_u*entry_cur_triangle_it->points[0].sup_a)+
												(bary_v*entry_cur_triangle_it->points[1].sup_a)+
												(bary_w*entry_cur_triangle_it->points[2].sup_a));

						// collision normal
						const Vector3 wcolnormal = -entry_cur_triangle_it->n;

						// penetration depth
						const float wpendepth = entry_cur_dst;

						assert(wcolpoint.x > -1000);

						outContact->colliders[0] = colliderA;
						outContact->colliders[1] = colliderB;
						outContact->point = wcolpoint;
						outContact->normal = wcolnormal;
						outContact->penetration = entry_cur_dst;

						break;
					}

					for(auto it = lst_triangles.begin(); it != lst_triangles.end();) {
						// can this face be 'seen' by entry_cur_support?
						if((it->n | (entry_cur_support.v - it->points[0].v)) > 0) {
							lam_addEdge(it->points[0],it->points[1]);
							lam_addEdge(it->points[1],it->points[2]);
							lam_addEdge(it->points[2],it->points[0]);
							it = lst_triangles.erase(it);
							continue;
						}
						it++;
					}

					// create new triangles from the edges in the edge list
					for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
						lst_triangles.emplace_back(entry_cur_support,it->points[0],it->points[1]);
					}

					lst_edges.clear();
				}
			}

			return true;
		}
	} catch(...) {
		return false;
	}
}
#undef gjk_simtest