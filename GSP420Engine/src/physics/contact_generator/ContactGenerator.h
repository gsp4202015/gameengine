#pragma once

#include "core\Engine.h"
#include "core\Object.h"

class World;

class ContactGenerator : public Object {
protected:
	World* myWorld;

public:
	ContactGenerator();
	virtual ~ContactGenerator();

	virtual void threadProc(ThreadTaskReference thisTask,World* world);
};