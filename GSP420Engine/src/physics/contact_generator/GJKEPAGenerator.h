#pragma once

#include "ContactGenerator.h"

#include <vector>
#include <array>

class World;
class ColliderComponent;
class Contact;

class GJKEPAGenerator : public ContactGenerator {
private:
	typedef std::vector<std::array<ColliderComponent*,2>> ProcItemsCont;
	ProcItemsCont procItems;

	std::array<uint32,10> procItemsSizeOverTime;
	uint32 procItemsSizeOverTime_insertInd;

	void processCollisionTests(ThreadTaskReference thisTask,ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd);

public:
	GJKEPAGenerator();
	virtual ~GJKEPAGenerator();

	virtual void threadProc(ThreadTaskReference thisTask,World* world);

	static bool performTest(ColliderComponent* colliderA,ColliderComponent* colliderB,Contact* outContact);
};