#pragma once

#include "core\Object.h"
#include "core\JSON.h"
#include "core\EngineTypes.h"

class PhysicalMaterial : public Object {
public:
	float restitution;
	float friction;

	PhysicalMaterial(float restitution,float friction);
	PhysicalMaterial(const json &jsonKey);
	virtual ~PhysicalMaterial();

	bool initializeFromJSON(const json &jsonKey);
};