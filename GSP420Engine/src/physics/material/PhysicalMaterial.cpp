#include "PhysicalMaterial.h"


PhysicalMaterial::PhysicalMaterial(float restitution,float friction) {
	this->restitution = restitution;
	this->friction = friction;
}

PhysicalMaterial::PhysicalMaterial(const json &jsonKey) {
	initializeFromJSON(jsonKey);
}

PhysicalMaterial::~PhysicalMaterial() {

}

bool PhysicalMaterial::initializeFromJSON(const json &jsonKey) {
	try {
		restitution = (float)jsonKey["restitution"].as<double>();
		friction = (float)jsonKey["friction"].as<double>();
	} catch(const std::exception &e) {
		std::cerr << e.what() << '\n';
		return false;
	}
	return true;
}
