#include "RenderableComponent.h"

#include "core\EngineSettings.h"

#include "DX11\Mesh\Mesh.h"
#include "DX11\Tools\Texture2D.h"
#include "DX11\Entity\GraphicsEntity.h"

#include "DX11\Manager\MeshManager.h"
#include "DX11\Manager\TextureManager.h"

RenderableComponent::RenderableComponent(GraphicsEntity *graphicsEnt) {
	graphicsEntity = graphicsEnt;

	tickGroup = TickGroup::PRE_GRAPHICS;
}

RenderableComponent::~RenderableComponent() {
	delete graphicsEntity;
}

void RenderableComponent::syncInternals() {
	graphicsEntity->mWorldMatrix = getComponentToWorld();
}

Mesh* RenderableComponent::getMesh() const {
	return graphicsEntity->GetMesh();
}

void RenderableComponent::setMesh(const engine_string &name) {
	graphicsEntity->SetMesh(EngineSettings::Instance().graphics.meshManager->GetMesh(name.c_str()));
}

DXTexture2D* RenderableComponent::getTexture() const {
	return graphicsEntity->GetTexture();
}

void RenderableComponent::setTexture(const engine_string &name) {
	auto texture = EngineSettings::Instance().graphics.textureManager->GetOrLoadTexture(
		EngineSettings::Instance().graphics.device,
		name.c_str(),
		name.c_str());

	graphicsEntity->SetTexture(texture);
}

void RenderableComponent::onTick(uint32 groups,engine_duration deltaTime) {
	syncInternals();
}
