#include "DXApp.h"

#include "core\Engine.h"

LRESULT CALLBACK MainWndProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam) {
	switch(msg) {
		case WM_DESTROY:{
			PostQuitMessage(0);
			return 0;
		}
		case WM_CLOSE:{
			PostQuitMessage(0);
			return 0;
		}
		default:{
			return DefWindowProc(hWnd,msg,wParam,lParam);
		}
	}
}

DXApp::DXApp(bool windowed,bool vsync,HINSTANCE hInstance,string_normal title) {
	mWindowed = windowed;
	mVsyncEnabled = vsync;
	mAppInstance = hInstance;
	mApphWnd = {};
	mAppTitle = title;
	//g_dxApp = this;

	mDevice = nullptr;
	mDeviceContext = nullptr;
	mRenderTargetView = nullptr;
	mSwapChain = nullptr;
	mDepthStencil = nullptr;
	mDepthStencilState = nullptr;
	mDepthDisabledStencilState = nullptr;
	mDepthStencilView = nullptr;
	mRasterizerState = nullptr;

	if(!windowed)
		mWndStyle = /*WS_CLIPSIBLINGS | WS_CLIPCHILDREN | */WS_POPUP;
	else
		mWndStyle = WS_OVERLAPPED;
}

DXApp::~DXApp() {
	Release();
}

uint32 DXApp::Run() {
	BeginGame();
	while (IsGameStillRunning()) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	return 0;
}

bool DXApp::Initialize(cstring_wide fontFile) {
	if(!InitializeWindow())
		return false;

	if(!InitializeD3D(fontFile))
		return false;

	return true;
}

bool DXApp::InitializeD3D(cstring_wide fontFile) {
	uint32 numModes,numerator,denominator,stringLength;

	IDXGIFactory *factory;
	HR(CreateDXGIFactory(__uuidof(IDXGIFactory),(void**)&factory));

	IDXGIAdapter *adapter;
	HR(factory->EnumAdapters(0,&adapter));

	IDXGIOutput *adapterOutput;
	HR(adapter->EnumOutputs(0,&adapterOutput));

	HR(adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM,DXGI_ENUM_MODES_INTERLACED,&numModes,NULL));
	DXGI_MODE_DESC *displayModeList = new DXGI_MODE_DESC[numModes];
	if(!displayModeList)
		return false;

	HR(adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM,DXGI_ENUM_MODES_INTERLACED,&numModes,displayModeList));

	for(uint32 i = 0; i < numModes; i++) {
		if(displayModeList[i].Width == (uint32)mClientWidth) {
			if(displayModeList[i].Height == (uint32)mClientHeight) {
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}
	}

	DXGI_ADAPTER_DESC adapterDesc;
	HR(adapter->GetDesc(&adapterDesc));

	mVideoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

	int32 error = wcstombs_s(&stringLength,mVideoCardDescription,128,adapterDesc.Description,128);
	if(error != 0)
		return false;

	Memory::SafeDeleteArr(displayModeList);
	Memory::SafeRelease(adapterOutput);
	Memory::SafeRelease(adapter);
	Memory::SafeRelease(factory);

	DXGI_SWAP_CHAIN_DESC swapDesc;
	ZeroMemory(&swapDesc,sizeof(DXGI_SWAP_CHAIN_DESC));
	swapDesc.BufferCount = 1; //single buffered
	swapDesc.BufferDesc.Width = mClientWidth;
	swapDesc.BufferDesc.Height = mClientHeight;
	swapDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	if(mVsyncEnabled) {
		swapDesc.BufferDesc.RefreshRate.Numerator = numerator;
		swapDesc.BufferDesc.RefreshRate.Denominator = denominator;
	} else {
		swapDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapDesc.BufferDesc.RefreshRate.Denominator = 1;
	}

	//Set the usage of the back buffer
	swapDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	//Set handle to window
	swapDesc.OutputWindow = mApphWnd;
	//Discard back buffer contents after presenting
	swapDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapDesc.Windowed = mWindowed;
	//No flags
	swapDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	//Turn off multi-sampling
	swapDesc.SampleDesc.Count = 1;
	swapDesc.SampleDesc.Quality = 0;
	//Set scan line ordering and scaling
	swapDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	//Set feature level to DirectX 11
	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;

	HR(D3D11CreateDeviceAndSwapChain(NULL,D3D_DRIVER_TYPE_HARDWARE,NULL,0,&featureLevel,1,D3D11_SDK_VERSION,&swapDesc,&mSwapChain,&mDevice,NULL,&mDeviceContext));

	HR(mSwapChain->GetBuffer(NULL,__uuidof(ID3D11Texture2D),reinterpret_cast<void**>(&mBackBuffer)));
	HR(mDevice->CreateRenderTargetView(mBackBuffer,nullptr,&mRenderTargetView));

	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc,sizeof(depthBufferDesc));

	depthBufferDesc.Width = mClientWidth;
	depthBufferDesc.Height = mClientHeight;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	HR(mDevice->CreateTexture2D(&depthBufferDesc,NULL,&mDepthStencil));

	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc,sizeof(D3D11_DEPTH_STENCIL_DESC));

	//Set up depth stencil desc
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	//Stencil operations for when pixel is facing back
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	//Stencil operations for pixel facing front
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//Create depth stencil state
	HR(mDevice->CreateDepthStencilState(&depthStencilDesc,&mDepthStencilState));

	D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;
	ZeroMemory(&depthDisabledStencilDesc,sizeof(D3D11_DEPTH_STENCIL_DESC));

	//Set up depth stencil desc
	depthDisabledStencilDesc.DepthEnable = true;
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	//Stencil operations for when pixel is facing back
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	//Stencil operations for pixel facing front
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//Create depth disabled stencil state
	HR(mDevice->CreateDepthStencilState(&depthDisabledStencilDesc,&mDepthDisabledStencilState));

	//Set depth stencil state
	mDeviceContext->OMSetDepthStencilState(mDepthStencilState,1);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc,sizeof(depthStencilViewDesc));
	//Set depth stencil view desc
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	//Create depth stencil view
	HR(mDevice->CreateDepthStencilView(mDepthStencil,&depthStencilViewDesc,&mDepthStencilView));

	//Bind render target view and depth stencil buffer to pipeline
	mDeviceContext->OMSetRenderTargets(1,&mRenderTargetView,mDepthStencilView);

	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.AntialiasedLineEnable = false;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	//Create rasterizer state
	HR(mDevice->CreateRasterizerState(&rasterizerDesc,&mRasterizerState));

	//Viewport Creation
	mViewport.Width = static_cast<float>(mClientWidth);
	mViewport.Height = static_cast<float>(mClientHeight);
	mViewport.TopLeftX = 0.0f;
	mViewport.TopLeftY = 0.0f;
	mViewport.MinDepth = 0.0f;
	mViewport.MaxDepth = 1.0f;

	//Bind viewport
	mDeviceContext->RSSetViewports(1,&mViewport);

	mSpriteBatch.reset(new DirectX::SpriteBatch(mDeviceContext));
	mSpriteFont.reset(new DirectX::SpriteFont(mDevice,fontFile));

	// PUBLICIZE DATA
	EngineSettings::Instance().graphics.deviceContext = mDeviceContext;
	EngineSettings::Instance().graphics.device = mDevice;
	EngineSettings::Instance().graphics.spriteBatch = mSpriteBatch.get();
	EngineSettings::Instance().graphics.spriteFont = mSpriteFont.get();

	return true;
}

bool DXApp::InitializeWindow() {
	DEVMODE screenSettings;
	uint32 x = 0;
	uint32 y = 0;

	//Window Desc
	WNDCLASSEX wcex;
	ZeroMemory(&wcex,sizeof(WNDCLASSEX));
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wcex.hInstance = mAppInstance;
	wcex.lpfnWndProc = MainWndProc;
	wcex.hIcon = LoadIcon(NULL,IDI_WINLOGO);
	wcex.hCursor = LoadCursor(NULL,IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = mAppTitle.c_str();
	wcex.hIconSm = wcex.hIcon;

	if(!RegisterClassEx(&wcex)) {
		OutputDebugString("/nFAILED TO CREATE WINDOW CLASS/n");
		return false;
	}

	mClientWidth = GetSystemMetrics(SM_CXSCREEN);
	mClientHeight = GetSystemMetrics(SM_CYSCREEN);

	if(!mWindowed) {
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&screenSettings,0,sizeof(screenSettings));
		screenSettings.dmSize = sizeof(screenSettings);
		screenSettings.dmPelsWidth = (unsigned long)mClientWidth;
		screenSettings.dmPelsHeight = (unsigned long)mClientHeight;
		screenSettings.dmBitsPerPel = 32;
		screenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&screenSettings,CDS_FULLSCREEN);
	}
	else {
		mClientWidth = 990;
		mClientHeight = 760;

		x = (GetSystemMetrics(SM_CXSCREEN) - mClientWidth) / 2;
		y = (GetSystemMetrics(SM_CYSCREEN) - mClientHeight) / 2;
	}

	mApphWnd = CreateWindowEx(WS_EX_APPWINDOW,mAppTitle.c_str(), mAppTitle.c_str(),mWndStyle,x,y,mClientWidth,mClientHeight,NULL,NULL,mAppInstance,NULL);

	if(!mApphWnd) {
		OutputDebugString("/nFAILED TO CREATE WINDOW/n");
		return false;
	}

	ShowWindow(mApphWnd,SW_SHOW);
	SetForegroundWindow(mApphWnd);
	SetFocus(mApphWnd);

	return true;
}

void DXApp::Release() {
	if(mSwapChain)
		mSwapChain->SetFullscreenState(false,NULL);

	if(mRasterizerState)
		Memory::SafeRelease(mRasterizerState);

	if(mDepthStencilView)
		Memory::SafeRelease(mDepthStencilView);

	if(mDepthStencilState)
		Memory::SafeRelease(mDepthStencilState);

	if(mDepthStencil)
		Memory::SafeRelease(mDepthStencil);

	if(mRenderTargetView)
		Memory::SafeRelease(mRenderTargetView);

	if(mDeviceContext) {
		mDeviceContext->ClearState();
		Memory::SafeRelease(mDeviceContext);
	}

	if(mDevice)
		Memory::SafeRelease(mDevice);

	if(mSwapChain)
		Memory::SafeRelease(mSwapChain);
}

void DXApp::SetBackBufferRenderTarget() {
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	mDeviceContext->OMSetRenderTargets(1,&mRenderTargetView,mDepthStencilView);
}

void DXApp::ResetViewport() {
	// Set the viewport.
	mDeviceContext->RSSetViewports(1,&mViewport);
}

void DXApp::TurnZBufferOn() {
	mDeviceContext->OMSetDepthStencilState(mDepthStencilState,1);
}

void DXApp::TurnZBufferOff() {
	mDeviceContext->OMSetDepthStencilState(mDepthDisabledStencilState,1);
}