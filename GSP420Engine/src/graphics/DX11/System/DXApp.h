#pragma once
#define WIN32_LEAN_AND_MEAN

#include <atomic>
#include <memory>

#include "core\EngineTypes.h"
#include "graphics\DX11\DirectX11.h"

class DXApp {
protected:
	//Win32 Attributes
	bool						mWindowed;
	bool						mVsyncEnabled;
	HWND						mApphWnd;
	HINSTANCE					mAppInstance;
	uint32						mClientWidth,mClientHeight;
	string_normal				mAppTitle;
	DWORD						mWndStyle;

	//Directx Attributes
	ID3D11Texture2D							*mBackBuffer;
	int										mVideoCardMemory;
	char									mVideoCardDescription[128];
	ID3D11Device							*mDevice;
	ID3D11DeviceContext						*mDeviceContext;
	IDXGISwapChain							*mSwapChain;
	ID3D11RenderTargetView					*mRenderTargetView;
	ID3D11Texture2D							*mDepthStencil;
	ID3D11DepthStencilState					*mDepthStencilState;
	ID3D11DepthStencilState					*mDepthDisabledStencilState;
	ID3D11DepthStencilView					*mDepthStencilView;
	ID3D11RasterizerState					*mRasterizerState;
	D3D_DRIVER_TYPE							mDriverType;
	D3D_FEATURE_LEVEL						mFeatureLevel;
	D3D11_VIEWPORT							mViewport;

	std::unique_ptr<DirectX::SpriteFont>	mSpriteFont;
	std::unique_ptr<DirectX::SpriteBatch>	mSpriteBatch;

	std::atomic<bool> shouldQuit;

public:
	DXApp(bool windowed,bool vsync,HINSTANCE hInstance,string_normal title);
	virtual ~DXApp();

	//MAIN
	uint32 Run();

	//Framework
	virtual bool Initialize(cstring_wide fontFile);
	virtual void Release();
	void SetBackBufferRenderTarget();
	void ResetViewport();

	inline POINT GetWindowSize() const { POINT ret; ret.x = mClientWidth; ret.y = mClientHeight; return ret; }
	inline HWND GetWindowHWND() const { return mApphWnd; }
	inline string_normal GetWindowTitle() const { return mAppTitle; }

	virtual void BeginGame() = 0;

	virtual void CheckInput() = 0;
	virtual void RenderFullFrame() = 0;

	virtual bool IsGameStillRunning() = 0;

protected:
	//Initialize Win32 Window
	bool InitializeWindow();

	//Initialize Directx
	bool InitializeD3D(cstring_wide fontFile);

	void TurnZBufferOn();
	void TurnZBufferOff();
};
