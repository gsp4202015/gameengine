#pragma once
#include "GraphicsEntity.h"

class Cube : public GraphicsEntity {
public:
	Cube(DXTexture2D* texture,Mesh *mesh);
	virtual ~Cube();

	void RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Release();
};