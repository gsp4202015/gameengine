#include "HUD.h"

#include "core\JSON.h"
#include "core\Engine.h"

HUD::HUD(uint32 screenWidth, uint32 screenHeight, string_normal jsonFile, const TextureManager *mngrTexture) {
	mScreenWidth = screenWidth;
	mScreenHeight = screenHeight;

	InitializeHUDFromFile(jsonFile, mngrTexture);
}


HUD::~HUD() {
}

void HUD::InitializeHUDFromFile(string_normal jsonFile, const TextureManager *mngrTexture) {
	json hud = json::parse_file(jsonFile);

	json sprites = hud["sprites"];

	for(uint32 i = 0; i < sprites.size(); i++) {
		try {
			Sprite *temp = new Sprite();

			temp->mTexture.reset(mngrTexture->GetTexture(sprites[i]["texture_key"].as<string_normal>()));
			temp->mPlacement = SetPlacementFromInt(sprites[i]["placement"].as<uint32>());
			double offsetX = sprites[i]["offset_x"].as<double>();
			double offsetY  = sprites[i]["offset_y"].as<double>();

			CalculateScreenPosition(temp, Vector2(offsetX, offsetY));

			temp->mScale.x = sprites[i]["scale_x"].as<double>();
			temp->mScale.y = sprites[i]["scale_y"].as<double>();
			temp->mText.mOffset.x = temp->mPosition.x += sprites[i]["text_offset_x"].as<double>();
			temp->mText.mOffset.y = temp->mPosition.y += sprites[i]["text_offset_y"].as<double>();
			temp->mText.mCaption = sprites[i]["text_caption"].as<string_normal>();

			mSprites.push_back(temp);
		} catch(const std::exception &e) {
			std::cerr << e.what() << '\n';
		}
	}
}

Placement HUD::SetPlacementFromInt(uint32 num) {
	switch(num) {
		case 0:{
			return TOP_LEFT;
		}break;
		case 1:{
			return TOP_RIGHT;
		}break;
		case 2:{
			return BOTTOM_RIGHT;
		}break;
		case 3:{
			return BOTTOM_LEFT;
		}break;
		case 4:{
			return TOP;
		}break;
		case 5:{
			return BOTTOM;
		}break;
		default:{
			return BOTTOM;
		}
	}
}

void HUD::CalculateScreenPosition(Sprite *sprite, Vector2 offset) {
	Vector2 position(0.0f, 0.0f);

	switch(sprite->mPlacement) {
		case TOP_LEFT:{
			position += offset;
		}break;
		case TOP_RIGHT:{
			position = Vector2(mScreenWidth, 0.0f);
			position.x -= sprite->mTexture->GetWidth();
			position += offset;
		}break;
		case BOTTOM_RIGHT:{
			position = Vector2(mScreenWidth,mScreenHeight);
			position.x -= sprite->mTexture->GetWidth();
			position.y -= sprite->mTexture->GetHeight();
			position += offset;
		}break;
		case BOTTOM_LEFT:{
			position = Vector2(0.0f,mScreenHeight);
			position.y -= sprite->mTexture->GetHeight();
			position += offset;
		}break;
		case TOP:{
			position = Vector2(mScreenWidth / 2.0f, 0.0f);
			position += offset;
		}break;
		case BOTTOM:{
			position = Vector2(mScreenWidth / 2.0f, mScreenHeight);
			position.y -= sprite->mTexture->GetHeight();
			position += offset;
		}break;
	}

	sprite->mPosition = position;
}

void HUD::Render(DirectX::SpriteBatch *spriteBatch,DirectX::SpriteFont *spriteFont) {
	spriteBatch->Begin();

	for(uint32 i = 0; i < mSprites.size(); i++) {
		spriteBatch->Draw(mSprites[i]->mTexture->GetTexture(),mSprites[i]->mPosition,NULL,DirectX::Colors::White,0.0f,Vector2(0.0),mSprites[i]->mScale);
		spriteFont->DrawString(spriteBatch,string_wide_from_normal(mSprites[i]->mText.mCaption).c_str(),mSprites[i]->mText.mOffset, DirectX::Colors::Black);
	}

	spriteBatch->End();
}