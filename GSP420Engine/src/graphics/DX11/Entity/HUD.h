#pragma once
#include "graphics\DX11\DX_Misc\DXUtility.h"
#include "graphics\DX11\Manager\TextureManager.h"

enum Placement{
	TOP_LEFT,
	TOP_RIGHT,
	BOTTOM_RIGHT,
	BOTTOM_LEFT,
	TOP,
	BOTTOM
};

struct Sprite {
	std::unique_ptr<DXTexture2D> mTexture;
	Vector2 mPosition;
	Vector2 mScale;
	Placement mPlacement;

	struct {
		Vector2 mOffset;
		string_normal mCaption;
	}mText;
};

struct Text {
	string_normal mText;
	Vector2 mPosition;
	Placement mPlacement;
};

class HUD {
private:
	uint32 mScreenWidth;
	uint32 mScreenHeight;

	std::vector<Sprite*> mSprites;

public:
	HUD(uint32 screenWidth, uint32 screenHeight, string_normal jsonFile, const TextureManager *mngrTexture);
	~HUD();

	virtual void InitializeHUDFromFile(string_normal jsonFile, const TextureManager *mngrTexture);
	Placement SetPlacementFromInt(uint32 num);
	void CalculateScreenPosition(Sprite *sprite, Vector2 offset);
	virtual void Render(DirectX::SpriteBatch *spriteBatch, DirectX::SpriteFont *spriteFont);
};

