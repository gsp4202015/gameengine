#include "LightSource.h"

LightSource::LightSource(ID3D11Device *device,Vector4 ambient,Vector4 diffuse,Vector3 position,Vector3 target,uint32 screenWidth, uint32 screenHeight,float screenDepth,float screenNear) {
	mAmbientColor = ambient;
	mDiffuseColor = diffuse;
	mPosition = position;
	mTarget = target;

	mLightDirection = mTarget - mPosition;
	mLightDirection = DirectX::XMVector3Normalize(mLightDirection);

	mLightView = DirectX::XMMatrixIdentity();
	mLightProjection = DirectX::XMMatrixIdentity();
	mOrthographic = DirectX::XMMatrixIdentity();

	mDephtMap = new RenderTexture(device,8192,8192,1500.0f,1.0f);

	BuildLightViewMatrix();
	BuildLightProjOrthoMatrix(screenWidth,screenHeight,screenDepth,screenNear);
}

LightSource::~LightSource() {
	if(mDephtMap)
		Memory::SafeRelease(mDephtMap);
}

void LightSource::Update(float dt,cstring list,...) {
	va_list vl;
	va_start(vl,list);

	for(uint32 i = 0; list[i] != '\0'; i++) {
		switch(list[i]) {
			case 'a': //new position
				mPosition = va_arg(vl,Vector3);
				break;

			default:
				break;
		}
	}

	BuildLightViewMatrix();
}

void LightSource::SetLightParameters(Vector4 ambient,Vector4 diffuse) {
	mAmbientColor = ambient;
	mDiffuseColor = diffuse;
}

void LightSource::BuildLightViewMatrix() {
	Vector3 up(0.0f,1.0f,0.0f);

	mLightView = DirectX::XMMatrixLookAtLH(mPosition,mTarget,up);
}

void LightSource::BuildLightProjOrthoMatrix(uint32 screenWidth,uint32 screenHeight,float screenDepth,float screenNear) {
	float fieldOfView,screenAspect;

	// Setup field of view and screen aspect for a square light source.
	fieldOfView = (float)DirectX::XM_PI / 2.0f;
	screenAspect = 1.0f;

	// Create the projection matrix for the light.
	mLightProjection = DirectX::XMMatrixPerspectiveFovLH(fieldOfView,screenAspect,screenNear,screenDepth);
	mOrthographic = DirectX::XMMatrixOrthographicLH(screenWidth,screenHeight,screenNear,screenDepth);
}