#pragma once
#include "HUD.h"

class GameHUD : public HUD {
private:
	Sprite *mHealthBar;
	Sprite *mBurnBar;
	Sprite *mHealthPotion;
	Sprite *mBurnPotion;
	Sprite *mPowerPotion;

public:
	GameHUD(uint32 screenWidth,uint32 screenHeight,string_normal jsonFile,const TextureManager *mngrTexture);
	~GameHUD();

	void InitializeHUDFromFile(string_normal jsonFile,const TextureManager *mngrTexture);
	void Render(DirectX::SpriteBatch *spriteBatch,DirectX::SpriteFont *spriteFont);
};