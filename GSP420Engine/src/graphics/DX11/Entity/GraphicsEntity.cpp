#include "GraphicsEntity.h"

#include "core\Engine.h"

GraphicsEntity::GraphicsEntity(DXTexture2D *texture,Mesh *mesh) {
	mTexture = texture;
	mMesh = mesh;
}

GraphicsEntity::~GraphicsEntity() {
	Release();
}

void GraphicsEntity::Release() {
	if(mTexture)
		Memory::SafeRelease(mTexture);
}

void GraphicsEntity::LoadTexture(const cstring fileName) {
}

void GraphicsEntity::LoadMesh(const cstring fileName) {
}