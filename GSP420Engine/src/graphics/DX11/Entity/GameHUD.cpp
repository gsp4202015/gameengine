#include "GameHUD.h"

#include "core\JSON.h"

#include "core\EngineSettings.h"

#include "core\World.h"

#include "content\actors\APlayer.h"

GameHUD::GameHUD(uint32 screenWidth,uint32 screenHeight,string_normal jsonFile,const TextureManager *mngrTexture)
	: HUD(screenWidth,screenHeight,jsonFile,mngrTexture) {

	InitializeHUDFromFile(jsonFile, mngrTexture);
}


GameHUD::~GameHUD() {
}

void GameHUD::InitializeHUDFromFile(string_normal jsonFile,const TextureManager *mngrTexture) {
	json hud = json::parse_file(jsonFile);

	json healthBar = hud["health_bar"];
	json burnBar = hud["burn_bar"];
	json healthPotion = hud["health_potion"];
	json burnPotion = hud["burn_potion"];
	json powerPotion = hud["power_potion"];

	try {
		mHealthBar = new Sprite();
		mBurnBar = new Sprite();
		mHealthPotion = new Sprite();
		mBurnPotion = new Sprite();
		mPowerPotion = new Sprite();

		mHealthBar->mTexture.reset(mngrTexture->GetTexture(healthBar[0]["texture_key"].as<string_normal>()));
		mHealthBar->mPlacement = SetPlacementFromInt(healthBar[0]["placement"].as<uint32>());
		double offsetX = healthBar[0]["offset_x"].as<double>();
		double offsetY = healthBar[0]["offset_y"].as<double>();

		CalculateScreenPosition(mHealthBar,Vector2(offsetX,offsetY));

		Vector2 pos = mHealthBar->mPosition;
		mHealthBar->mScale.x = healthBar[0]["scale_x"].as<double>();
		mHealthBar->mScale.y = healthBar[0]["scale_y"].as<double>();
		mHealthBar->mText.mOffset.x = pos.x += healthBar[0]["text_offset_x"].as<double>();
		mHealthBar->mText.mOffset.y = pos.y += healthBar[0]["text_offset_y"].as<double>();
		mHealthBar->mText.mCaption = healthBar[0]["text_caption"].as<string_normal>();

		mBurnBar->mTexture.reset(mngrTexture->GetTexture(burnBar[0]["texture_key"].as<string_normal>()));
		mBurnBar->mPlacement = SetPlacementFromInt(burnBar[0]["placement"].as<uint32>());
		offsetX = burnBar[0]["offset_x"].as<double>();
		offsetY = burnBar[0]["offset_y"].as<double>();

		CalculateScreenPosition(mBurnBar,Vector2(offsetX,offsetY));

		pos = mBurnBar->mPosition;
		mBurnBar->mScale.x = burnBar[0]["scale_x"].as<double>();
		mBurnBar->mScale.y = burnBar[0]["scale_y"].as<double>();
		mBurnBar->mText.mOffset.x = pos.x += burnBar[0]["text_offset_x"].as<double>();
		mBurnBar->mText.mOffset.y = pos.y += burnBar[0]["text_offset_y"].as<double>();
		mBurnBar->mText.mCaption = burnBar[0]["text_caption"].as<string_normal>();

		mHealthPotion->mTexture.reset(mngrTexture->GetTexture(healthPotion[0]["texture_key"].as<string_normal>()));
		mHealthPotion->mPlacement = SetPlacementFromInt(healthPotion[0]["placement"].as<uint32>());
		offsetX = healthPotion[0]["offset_x"].as<double>();
		offsetY = healthPotion[0]["offset_y"].as<double>();

		CalculateScreenPosition(mHealthPotion,Vector2(offsetX,offsetY));

		pos = mHealthPotion->mPosition;
		mHealthPotion->mScale.x = healthPotion[0]["scale_x"].as<double>();
		mHealthPotion->mScale.y = healthPotion[0]["scale_y"].as<double>();
		mHealthPotion->mText.mOffset.x = pos.x += healthPotion[0]["text_offset_x"].as<double>();
		mHealthPotion->mText.mOffset.y = pos.y += healthPotion[0]["text_offset_y"].as<double>();
		mHealthPotion->mText.mCaption = healthPotion[0]["text_caption"].as<string_normal>();

		mBurnPotion->mTexture.reset(mngrTexture->GetTexture(burnPotion[0]["texture_key"].as<string_normal>()));
		mBurnPotion->mPlacement = SetPlacementFromInt(burnPotion[0]["placement"].as<uint32>());
		offsetX = burnPotion[0]["offset_x"].as<double>();
		offsetY = burnPotion[0]["offset_y"].as<double>();

		CalculateScreenPosition(mBurnPotion,Vector2(offsetX,offsetY));

		pos = mBurnPotion->mPosition;
		mBurnPotion->mScale.x = burnPotion[0]["scale_x"].as<double>();
		mBurnPotion->mScale.y = burnPotion[0]["scale_y"].as<double>();
		mBurnPotion->mText.mOffset.x = pos.x += burnPotion[0]["text_offset_x"].as<double>();
		mBurnPotion->mText.mOffset.y = pos.y += burnPotion[0]["text_offset_y"].as<double>();
		mBurnPotion->mText.mCaption = burnPotion[0]["text_caption"].as<string_normal>();

		mPowerPotion->mTexture.reset(mngrTexture->GetTexture(powerPotion[0]["texture_key"].as<string_normal>()));
		mPowerPotion->mPlacement = SetPlacementFromInt(powerPotion[0]["placement"].as<uint32>());
		offsetX = powerPotion[0]["offset_x"].as<double>();
		offsetY = powerPotion[0]["offset_y"].as<double>();

		CalculateScreenPosition(mPowerPotion,Vector2(offsetX,offsetY));
		
		pos = mPowerPotion->mPosition;
		mPowerPotion->mScale.x = powerPotion[0]["scale_x"].as<double>();
		mPowerPotion->mScale.y = powerPotion[0]["scale_y"].as<double>();
		mPowerPotion->mText.mOffset.x =  pos.x+= powerPotion[0]["text_offset_x"].as<double>();
		mPowerPotion->mText.mOffset.y = pos.y += powerPotion[0]["text_offset_y"].as<double>();
		mPowerPotion->mText.mCaption = powerPotion[0]["text_caption"].as<string_normal>();

	} catch(const std::exception &e) {
		std::cerr << e.what() << '\n';
	}
}

void GameHUD::Render(DirectX::SpriteBatch *spriteBatch,DirectX::SpriteFont *spriteFont) {
	HUD::Render(spriteBatch,spriteFont);

	mHealthBar->mScale.x = EngineSettings::Instance().system.activeWorld->getActivePlayer()->playerData.healthPercent;
	mBurnBar->mScale.x = EngineSettings::Instance().system.activeWorld->getActivePlayer()->playerData.burnBarPercent;

	spriteBatch->Begin();

	spriteBatch->Draw(mHealthBar->mTexture->GetTexture(),mHealthBar->mPosition,NULL,DirectX::Colors::White,0.0f,Vector2(0.0),mHealthBar->mScale);
	spriteFont->DrawString(spriteBatch,string_wide_from_normal(mHealthBar->mText.mCaption).c_str(),mHealthBar->mText.mOffset,DirectX::Colors::Black);

	spriteBatch->Draw(mBurnBar->mTexture->GetTexture(),mBurnBar->mPosition,NULL,DirectX::Colors::White,0.0f,Vector2(0.0),mBurnBar->mScale);
	spriteFont->DrawString(spriteBatch,string_wide_from_normal(mBurnBar->mText.mCaption).c_str(),mBurnBar->mText.mOffset,DirectX::Colors::Black);

	spriteBatch->Draw(mHealthPotion->mTexture->GetTexture(),mHealthPotion->mPosition,NULL,DirectX::Colors::White,0.0f,Vector2(0.0),mHealthPotion->mScale);
	spriteFont->DrawString(spriteBatch,string_wide_from_normal(mHealthPotion->mText.mCaption).c_str(),mHealthPotion->mText.mOffset,DirectX::Colors::Black);

	spriteBatch->Draw(mBurnPotion->mTexture->GetTexture(),mBurnPotion->mPosition,NULL,DirectX::Colors::White,0.0f,Vector2(0.0),mBurnPotion->mScale);
	spriteFont->DrawString(spriteBatch,string_wide_from_normal(mBurnPotion->mText.mCaption).c_str(),mBurnPotion->mText.mOffset,DirectX::Colors::Black);

	spriteBatch->Draw(mPowerPotion->mTexture->GetTexture(),mPowerPotion->mPosition,NULL,DirectX::Colors::White,0.0f,Vector2(0.0),mPowerPotion->mScale);
	spriteFont->DrawString(spriteBatch,string_wide_from_normal(mPowerPotion->mText.mCaption).c_str(),mPowerPotion->mText.mOffset,DirectX::Colors::Black);

	spriteBatch->End();
}