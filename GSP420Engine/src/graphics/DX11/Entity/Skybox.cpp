#include "Skybox.h"

Skybox::Skybox(DXTexture2D* texture,Mesh* mesh)
	: GraphicsEntity(texture,mesh) {
}

Skybox::~Skybox() {
	Release();
}

void Skybox::RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) {
}

void Skybox::Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) {
	const Matrix matWorld = Matrix::CreateScale(2,2,2) * mWorldMatrix;

	ShaderBase *skyboxShader = shManager->GetShader(STR("skybox_shader"));

	skyboxShader->SetTexture(deviceContext,STR("e"),mTexture->GetTexture());
	skyboxShader->SetBuffers(deviceContext,STR("wvp"),matWorld,viewMatrix,projMatrix);
	mMesh->Render(deviceContext,skyboxShader);
}

void Skybox::Release() {
	GraphicsEntity::Release();
}