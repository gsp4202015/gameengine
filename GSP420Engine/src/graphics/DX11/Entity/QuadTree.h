#pragma once
#include "graphics\DX11\Tools\Frustrum.h"
#include "graphics\DX11\Mesh\Mesh.h"

struct QuadNode {
	Vector2 Position;
	float Width;
	uint32 TriangleCount;
	ID3D11Buffer *VertexBuffer;
	ID3D11Buffer *IndexBuffer;
	QuadNode *Nodes[4];
};

class QuadTree {
public:
	struct VertexPTN {
		Vector3 position;
		Vector2 texture;
		Vector3 normal;
	};

private:
	uint32 mNodeCount;

	uint32 mMaxTriangle;
	uint32 mVertCount;
	uint32 mTriangleCount;
	VertexPTN *mVertices;
	QuadNode *mParentNode;

public:
	QuadTree(ID3D11Device *device,Mesh *terrain,uint32 maxTriangles);
	~QuadTree();

	void Release();
	void Render(Frustrum *frustrum,ID3D11DeviceContext *deviceContext,ShaderBase *shader);
	void CalculateMeshDimensions(uint32 vertCount,Vector2 &position,float &width);
	void CreateNode(QuadNode *node,Vector2 position,float width,ID3D11Device *device);
	uint32 CountTriangles(Vector2 position,float width);
	bool CheckTriangleContained(uint32 index,Vector2 position,float width);
	void ReleaseNode(QuadNode *node);
	void RenderNode(QuadNode *node,Frustrum *frustrum,ID3D11DeviceContext *deviceContext,ShaderBase *shader);
};