#pragma once
#include "GraphicsEntity.h"

class Skybox : public GraphicsEntity {
public:
	Skybox(DXTexture2D* texture,Mesh* mesh);
	virtual ~Skybox();

	void RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Release();
};