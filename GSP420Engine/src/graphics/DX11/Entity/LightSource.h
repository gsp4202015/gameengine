#pragma once
#include "core\EngineTypes.h"

#include "graphics\DX11\DX_Misc\DXUtility.h"
#include "graphics\DX11\Tools\RenderTexture.h"

class LightSource {
private:
	Vector3 mPosition;
	Vector3 mTarget;
	Vector3 mLightDirection;
	Vector4 mAmbientColor;
	Vector4 mDiffuseColor;
	Matrix mLightView;
	Matrix mLightProjection;
	Matrix mOrthographic;
	RenderTexture *mDephtMap;

public:
	LightSource(ID3D11Device *device,Vector4 ambient,Vector4 diffuse,Vector3 position,Vector3 target,uint32 screenWidth,uint32 screenHeight,float screenDepth,float screenNear);
	~LightSource();

	void Update(float dt,cstring list,...);
	void BuildLightViewMatrix();
	void BuildLightProjOrthoMatrix(uint32 screenWidth,uint32 screenHeight,float screenDepth,float screenNear);
	void SetLightParameters(Vector4 ambient,Vector4 diffuse);

	void SetAmbientColor(Vector4 ambient) { mAmbientColor = ambient; };
	void SetDiffuseColor(Vector4 diffuse) { mDiffuseColor = diffuse; };

	Vector3 GetLightDirection() { return mLightDirection; };
	Vector3 GetPosition() { return mPosition; };
	Vector4 GetAmbientColor() { return mAmbientColor; };
	Vector4 GetDiffuseColor() { return mDiffuseColor; };
	Matrix GetLightOrthographicMatrix() { return mOrthographic; };
	Matrix GetLightViewMatrix() { return mLightView; };
	Matrix GetLightProjectionMatrix() { return mLightProjection; };
	RenderTexture* GetRenderTexture() { return mDephtMap; };
};