#pragma once
#include "graphics\DX11\Mesh\Mesh.h"
#include "graphics\DX11\Tools\Frustrum.h"
#include "graphics\DX11\Tools\Texture2D.h"
#include "core\Transform.h"
#include "LightSource.h"

class GraphicsEntity {
protected:
	DXTexture2D *mTexture;
	Mesh *mMesh;

public:
	Matrix mWorldMatrix;

	GraphicsEntity(DXTexture2D *texture,Mesh *mesh);
	virtual ~GraphicsEntity();

	virtual void RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) = 0;
	virtual void Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) = 0;
	virtual void Release();

	//Accessor
	Matrix GetWorldMatrix() const { return mWorldMatrix; };
	Mesh* GetMesh() const { return mMesh; }
	DXTexture2D*  GetTexture() const { return mTexture; };

	//Mutator
	void LoadTexture(const cstring fileName);
	void LoadMesh(const cstring fileName);
	void SetMesh(Mesh *mesh) { mMesh = mesh; }
	void SetTexture(DXTexture2D *texture) { mTexture = texture; }
};