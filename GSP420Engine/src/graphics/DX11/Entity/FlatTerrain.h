#pragma once
#include "GraphicsEntity.h"
#include "QuadTree.h"

class FlatTerrain : public GraphicsEntity {
private:
	QuadTree *mQuadrants;

public:
	FlatTerrain(DXTexture2D* texture,Mesh *mesh,ID3D11Device *device,uint32 maxTriangles);
	virtual ~FlatTerrain();

	void RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Release();

	QuadTree* GetQuadTree() { return mQuadrants; };
};
