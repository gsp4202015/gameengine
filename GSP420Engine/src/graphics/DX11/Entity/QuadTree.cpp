#include "QuadTree.h"
#include "FlatTerrain.h"

QuadTree::QuadTree(ID3D11Device *device,Mesh *terrain,uint32 maxTriangles) {
	mMaxTriangle = maxTriangles;

	uint32 vertCount = terrain->GetVertexCount();

	mParentNode = 0;
	mVertices = 0;

	mTriangleCount = vertCount / 3;

	mVertices = new VertexPTN[vertCount];

	terrain->CopyVertexArray((void*)mVertices);

	Vector2 position;
	float width;

	CalculateMeshDimensions(vertCount,position,width);

	mParentNode = new QuadNode;

	mNodeCount = 1;
	CreateNode(mParentNode,position,width,device);
	
	if(mVertices)
		Memory::SafeDeleteArr(mVertices);
}

QuadTree::~QuadTree() {
	Release();
}

void QuadTree::Release() {
	if(mParentNode) {
		ReleaseNode(mParentNode);
		Memory::SafeDelete(mParentNode);
	}
}

void QuadTree::Render(Frustrum *frustrum,ID3D11DeviceContext *deviceContext,ShaderBase *shader) {
	RenderNode(mParentNode,frustrum,deviceContext,shader);
}

void QuadTree::CalculateMeshDimensions(uint32 vertCount,Vector2 &position,float &meshWidth) {
	position.x = 0.0f;
	position.y = 0.0f;

	for(uint32 i = 0; i < vertCount; i++) {
		position.x += mVertices[i].position.x;
		position.y += mVertices[i].position.z;
	}

	position.x = position.x / (float)vertCount;
	position.y = position.y / (float)vertCount;

	float maxWidth = 0.0f;
	float maxDepth = 0.0f;

	float minWidth = fabsf(mVertices[0].position.x - position.x);
	float minDepth = fabsf(mVertices[0].position.z - position.x);

	float width = 0.0f;
	float depth = 0.0f;

	for(uint32 i = 0; i < vertCount; i++) {
		width = fabsf(mVertices[i].position.x - position.x);
		depth = fabsf(mVertices[i].position.z - position.y);

		if(width > maxWidth)
			maxWidth = width;

		if(width < minWidth)
			minWidth = width;

		if(depth > maxDepth)
			maxDepth = depth;

		if(depth < minDepth)
			minDepth = depth;
	}

	Vector2 maxPosition;
	maxPosition.x = (float)max(fabsf(minWidth),fabsf(maxWidth));
	maxPosition.y = (float)max(fabsf(minDepth),fabsf(maxDepth));

	meshWidth = max(maxPosition.x,maxPosition.y) * 2.0f;
}

void QuadTree::CreateNode(QuadNode *node,Vector2 position,float width,ID3D11Device *device) {
	node->Position.x = position.x;
	node->Position.y = position.y;
	node->Width = width;
	node->TriangleCount = 0;
	node->VertexBuffer = 0;
	node->IndexBuffer = 0;
	node->Nodes[0] = 0;
	node->Nodes[1] = 0;
	node->Nodes[2] = 0;
	node->Nodes[3] = 0;

	uint32 numTriangles = CountTriangles(position,width);

	if(numTriangles == 0)
		return;

	float offsetX = 0.0f;
	float offsetZ = 0.0f;
	uint32 count = 0;

	if(numTriangles > mMaxTriangle) {
		for(uint32 i = 0; i < 4; i++) {
			offsetX = (((i % 2) < 1) ? -1.0f : 1.0f) * (width / 4.0f);
			offsetZ = (((i % 4) < 2) ? -1.0f : 1.0f) * (width / 4.0f);

			count = CountTriangles(Vector2((position.x + offsetX),(position.y + offsetZ)),(width / 2.0f));
			if(count > 0) {
				node->Nodes[i] = new QuadNode;
				CreateNode(node->Nodes[i],Vector2((position.x + offsetX),(position.y + offsetZ)),(width / 2.0f),device);
			}
		}

		return;
	}

	node->TriangleCount = numTriangles;

	uint32 vertexCount = numTriangles * 3;

	VertexPTN *vertices = new VertexPTN[vertexCount];

	unsigned long *indices = new unsigned long[vertexCount];

	bool result;
	uint32 vertexIndex = 0;

	uint32 index = 0;
	for(uint32 i = 0; i < mTriangleCount; i++) {
		result = CheckTriangleContained(i,position,width);

		if(result) {
			vertexIndex = i * 3;

			vertices[index].position = mVertices[vertexIndex].position;
			vertices[index].texture = mVertices[vertexIndex].texture;
			vertices[index].normal = mVertices[vertexIndex].normal;
			indices[index] = index;
			index++;
			vertexIndex++;

			vertices[index].position = mVertices[vertexIndex].position;
			vertices[index].texture = mVertices[vertexIndex].texture;
			vertices[index].normal = mVertices[vertexIndex].normal;
			indices[index] = index;
			index++;
			vertexIndex++;

			vertices[index].position = mVertices[vertexIndex].position;
			vertices[index].texture = mVertices[vertexIndex].texture;
			vertices[index].normal = mVertices[vertexIndex].normal;
			indices[index] = index;
			index++;
		}
	}

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexPTN) * vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexData;
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	device->CreateBuffer(&vertexBufferDesc,&vertexData,&node->VertexBuffer);

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * vertexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexData;
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	device->CreateBuffer(&indexBufferDesc,&indexData,&node->IndexBuffer);

	Memory::SafeDeleteArr(vertices);
	Memory::SafeDeleteArr(indices);

	mNodeCount++;
}

uint32 QuadTree::CountTriangles(Vector2 position,float width) {
	uint32 count = 0;
	bool result;

	for(uint32 i = 0; i < mTriangleCount; i++) {
		result = CheckTriangleContained(i,position,width);

		if(result)
			count++;
	}

	return count;
}

bool QuadTree::CheckTriangleContained(uint32 index,Vector2 position,float width) {
	float radius = width / 2.0f;

	uint32 vertexIndex = index * 3;

	Vector2 position1;
	position1.x = mVertices[vertexIndex].position.x;
	position1.y = mVertices[vertexIndex].position.z;
	vertexIndex++;

	Vector2 position2;
	position2.x = mVertices[vertexIndex].position.x;
	position2.y = mVertices[vertexIndex].position.z;
	vertexIndex++;

	Vector2 position3;
	position3.x = mVertices[vertexIndex].position.x;
	position3.y = mVertices[vertexIndex].position.z;

	float minX = min(position1.x,min(position2.x,position3.x));
	if(minX > (position.x + radius))
		return false;

	float maxX = max(position1.x,max(position2.x,position3.x));
	if(maxX < (position.x - radius))
		return false;

	float minZ = min(position1.y,min(position2.y,position3.y));
	if(minZ >(position.y + radius))
		return false;

	float maxZ = max(position1.y,max(position2.y,position3.y));
	if(maxZ < (position.y - radius))
		return false;

	return true;
}

void QuadTree::ReleaseNode(QuadNode *node) {
	for(uint32 i = 0; i < 4; i++) {
		if(node->Nodes[i] != 0)
			ReleaseNode(node->Nodes[i]);
	}

	if(node->VertexBuffer)
		Memory::SafeRelease(node->VertexBuffer);

	if(node->IndexBuffer)
		Memory::SafeRelease(node->IndexBuffer);

	for(uint32 i = 0; i < 4; i++) {
		if(node->Nodes[i])
			Memory::SafeDelete(node->Nodes[i]);
	}
}

void QuadTree::RenderNode(QuadNode* node,Frustrum *frustrum,ID3D11DeviceContext *deviceContext,ShaderBase *shader) {
	bool result = frustrum->CheckCube(Vector3(node->Position.x,0.0f,node->Position.y),node->Width);

	if(!result)
		return;

	uint32 count = 0;
	for(uint32 i = 0; i < 4; i++) {
		if(node->Nodes[i] != 0) {
			RenderNode(node->Nodes[i],frustrum,deviceContext,shader);
			count++;
		}
	}

	if(count != 0)
		return;

	uint32 stride = sizeof(VertexPTN);
	uint32 offset = 0;

	deviceContext->IASetVertexBuffers(0,1,&node->VertexBuffer,&stride,&offset);

	deviceContext->IASetIndexBuffer(node->IndexBuffer,DXGI_FORMAT_R32_UINT,0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	uint32 indexCount = node->TriangleCount * 3;

	shader->RenderShader(deviceContext,indexCount);
}