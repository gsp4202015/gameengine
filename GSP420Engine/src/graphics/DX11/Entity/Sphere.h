#pragma once
#include "GraphicsEntity.h"

class Sphere : public GraphicsEntity {
public:
	Sphere(DXTexture2D* texture,Mesh* mesh);
	virtual ~Sphere();

	void RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Release();
};
