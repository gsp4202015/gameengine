#include "FlatTerrain.h"

FlatTerrain::FlatTerrain(DXTexture2D* texture,Mesh* mesh,ID3D11Device *device,uint32 maxTriangles)
	:GraphicsEntity(texture,mesh) {
	mQuadrants = new QuadTree(device,mesh,maxTriangles);
}

FlatTerrain::~FlatTerrain() {
}

void FlatTerrain::RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) {
	ShaderBase *depthShader = shManager->GetShader(STR("depth_shader"));
	depthShader->SetBuffers(deviceContext,STR("wvpab"),mWorldMatrix,viewMatrix,projMatrix,light->GetLightViewMatrix(),light->GetLightOrthographicMatrix());
	mQuadrants->Render(frustrum,deviceContext,depthShader);
}

void FlatTerrain::Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) {
	ShaderBase *shadowShader = shManager->GetShader(STR("shadow_shader"));
	shadowShader->SetTexture(deviceContext,STR("de"),light->GetRenderTexture()->GetShaderResourceView(),mTexture->GetTexture());
	shadowShader->SetBuffers(deviceContext,STR("wvpabcdef"),mWorldMatrix,viewMatrix,projMatrix,light->GetLightViewMatrix(),light->GetLightOrthographicMatrix(),light->GetAmbientColor(),light->GetDiffuseColor(),light->GetPosition(),light->GetLightDirection());
	mQuadrants->Render(frustrum,deviceContext,shadowShader);
}

void FlatTerrain::Release() {
	GraphicsEntity::Release();
}