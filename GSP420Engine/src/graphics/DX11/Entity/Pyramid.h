#pragma once
#include "GraphicsEntity.h"

class Pyramid : public GraphicsEntity {
public:
	Pyramid(DXTexture2D* texture,Mesh* mesh);
	virtual ~Pyramid();

	void RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light);
	void Release();
};