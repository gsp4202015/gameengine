#include "Sphere.h"

Sphere::Sphere(DXTexture2D* texture,Mesh* mesh)
	: GraphicsEntity(texture,mesh) {

}

Sphere::~Sphere() {
	Release();
}

void Sphere::RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) {
	ShaderBase *depthShader = shManager->GetShader(STR("depth_shader"));
	depthShader->SetBuffers(deviceContext,STR("wvpab"),mWorldMatrix,viewMatrix,projMatrix,light->GetLightViewMatrix(),light->GetLightOrthographicMatrix());
	mMesh->Render(deviceContext,depthShader);
}

void Sphere::Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) {
	const Transform myTrans = mWorldMatrix;

	if(frustrum->CheckSphere(myTrans.position,myTrans.scale.x)) {
		ShaderBase *shadowShader = shManager->GetShader(STR("shadow_shader"));
		shadowShader->SetTexture(deviceContext,STR("de"),light->GetRenderTexture()->GetShaderResourceView(),mTexture->GetTexture());
		shadowShader->SetBuffers(deviceContext,STR("wvpabcdef"),myTrans.getMatrix(),viewMatrix,projMatrix,light->GetLightViewMatrix(),light->GetLightOrthographicMatrix(),light->GetAmbientColor(),light->GetDiffuseColor(),light->GetPosition(),light->GetLightDirection());
		mMesh->Render(deviceContext,shadowShader);
	}
}

void Sphere::Release() {
	GraphicsEntity::Release();
}