#include "Pyramid.h"

Pyramid::Pyramid(DXTexture2D* texture,Mesh* mesh)
	: GraphicsEntity(texture,mesh) {
}

Pyramid::~Pyramid() {
	Release();
}

void Pyramid::RenderToTexture(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) {
	ShaderBase *depthShader = shManager->GetShader(STR("depth_shader"));

	depthShader->SetBuffers(deviceContext,STR("wvpab"),mWorldMatrix,viewMatrix,projMatrix,light->GetLightViewMatrix(),light->GetLightOrthographicMatrix());
	mMesh->Render(deviceContext,depthShader);
}

void Pyramid::Render(ID3D11DeviceContext *deviceContext,Frustrum *frustrum,ShaderManager *shManager,Matrix viewMatrix,Matrix projMatrix,LightSource *light) {
	ShaderBase *shadowShader = shManager->GetShader(STR("shadow_shader"));

	shadowShader->SetTexture(deviceContext,STR("de"),light->GetRenderTexture()->GetShaderResourceView(),mTexture->GetTexture());
	shadowShader->SetBuffers(deviceContext,STR("wvpabcdef"),mWorldMatrix,viewMatrix,projMatrix,light->GetLightViewMatrix(),light->GetLightOrthographicMatrix(),light->GetAmbientColor(),light->GetDiffuseColor(),light->GetPosition(),light->GetLightDirection());
	mMesh->Render(deviceContext,shadowShader);
}

void Pyramid::Release() {
	GraphicsEntity::Release();
}