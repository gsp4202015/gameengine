#include "SkyboxMesh.h"

SkyboxMesh::SkyboxMesh()
	: Mesh() {
}

SkyboxMesh::~SkyboxMesh() {
}

bool SkyboxMesh::InitializeBuffers(ID3D11Device *device) {
	VertexPT *vertices;
	unsigned long *indices;
	D3D11_BUFFER_DESC vertexBufferDesc,indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData,indexData;

	mVertexCount = 24;
	mIndexCount = 36;

	vertices = new VertexPT[mVertexCount];
	if(!vertices)
		return false;

	indices = new unsigned long[mIndexCount];
	if(!indices)
		return false;

	//BACK
	vertices[0].Position = Vector3(-1.0f,-1.0f,-1.0f);
	vertices[0].Texture = Vector2(1.0f,0.6666f);

	vertices[1].Position = Vector3(-1.0f,1.0f,-1.0f);
	vertices[1].Texture = Vector2(1.0f,0.3343f);

	vertices[2].Position = Vector3(1.0f,1.0f,-1.0f);
	vertices[2].Texture = Vector2(0.75f,0.3343f);

	vertices[3].Position = Vector3(1.0f,-1.0f,-1.0f);
	vertices[3].Texture = Vector2(0.75f,0.6666f);

	//FRONT
	vertices[4].Position = Vector3(-1.0f,-1.0f,1.0f);
	vertices[4].Texture = Vector2(0.25f,0.6666f);

	vertices[5].Position = Vector3(1.0f,-1.0f,1.0f);
	vertices[5].Texture = Vector2(0.50f,0.6666f);

	vertices[6].Position = Vector3(1.0f,1.0f,1.0f);
	vertices[6].Texture = Vector2(0.50f,0.3333f);

	vertices[7].Position = Vector3(-1.0f,1.0f,1.0f);
	vertices[7].Texture = Vector2(0.25f,0.3333f);

	//TOP
	vertices[8].Position = Vector3(-1.0f,1.0f,-1.0f);
	vertices[8].Texture = Vector2(0.2509f,0.0f);

	vertices[9].Position = Vector3(-1.0f,1.0f,1.0f);
	vertices[9].Texture = Vector2(0.2509f,0.3333f);

	vertices[10].Position = Vector3(1.0f,1.0f,1.0f);
	vertices[10].Texture = Vector2(0.4999f,0.3333f);

	vertices[11].Position = Vector3(1.0f,1.0f,-1.0f);
	vertices[11].Texture = Vector2(0.4999f,0.0f);

	//BOTTOM
	vertices[12].Position = Vector3(-1.0f,-1.0f,-1.0f);
	vertices[12].Texture = Vector2(0.50f,1.0f);

	vertices[13].Position = Vector3(1.0f,-1.0f,-1.0f);
	vertices[13].Texture = Vector2(0.25f,1.0f);

	vertices[14].Position = Vector3(1.0f,-1.0f,1.0f);
	vertices[14].Texture = Vector2(0.50f,0.6666f);

	vertices[15].Position = Vector3(-1.0f,-1.0f,1.0f);
	vertices[15].Texture = Vector2(0.25f,0.6666f);

	//LEFT
	vertices[16].Position = Vector3(-1.0f,-1.0f,1.0f);
	vertices[16].Texture = Vector2(0.25f,0.6666f);

	vertices[17].Position = Vector3(-1.0f,1.0f,1.0f);
	vertices[17].Texture = Vector2(0.25f,0.3343f);

	vertices[18].Position = Vector3(-1.0f,1.0f,-1.0f);
	vertices[18].Texture = Vector2(0.0f,0.3343f);

	vertices[19].Position = Vector3(-1.0f,-1.0f,-1.0f);
	vertices[19].Texture = Vector2(0.0f,0.6666f);

	//RIGHT
	vertices[20].Position = Vector3(1.0f,-1.0f,-1.0f);
	vertices[20].Texture = Vector2(0.75f,0.6666f);

	vertices[21].Position = Vector3(1.0f,-1.0f,1.0f);
	vertices[21].Texture = Vector2(0.50f,0.6666f);

	vertices[22].Position = Vector3(1.0f,1.0f,1.0f);
	vertices[22].Texture = Vector2(0.50f,0.3343f);

	vertices[23].Position = Vector3(1.0f,1.0f,-1.0f);
	vertices[23].Texture = Vector2(0.75f,0.3343f);

	//BACK
	indices[0] = 2;
	indices[1] = 1;
	indices[2] = 0;

	indices[3] = 0;
	indices[4] = 3;
	indices[5] = 2;

	//FRONT
	indices[6] = 6;
	indices[7] = 5;
	indices[8] = 4;

	indices[9] = 7;
	indices[10] = 6;
	indices[11] = 4;

	//TOP
	indices[12] = 10;
	indices[13] = 9;
	indices[14] = 8;

	indices[15] = 11;
	indices[16] = 10;
	indices[17] = 8;

	//BOTTOM
	indices[18] = 15;
	indices[19] = 14;
	indices[20] = 13;

	indices[21] = 13;
	indices[22] = 12;
	indices[23] = 15;

	//LEFT
	indices[24] = 18;
	indices[25] = 17;
	indices[26] = 16;

	indices[27] = 19;
	indices[28] = 18;
	indices[29] = 16;

	//RIGHT
	indices[30] = 22;
	indices[31] = 23;
	indices[32] = 21;

	indices[33] = 20;
	indices[34] = 21;
	indices[35] = 23;

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexPT) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	HR(device->CreateBuffer(&vertexBufferDesc,&vertexData,&mVertexBuffer));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * mIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = indices;

	HR(device->CreateBuffer(&indexBufferDesc,&indexData,&mIndexBuffer));

	Memory::SafeDeleteArr(vertices);

	Memory::SafeDeleteArr(indices);

	return true;
}

void SkyboxMesh::RenderBuffers(ID3D11DeviceContext *deviceContext) {
	uint32 stride = sizeof(VertexPT);
	uint32 offset = 0;

	deviceContext->IASetVertexBuffers(0,1,&mVertexBuffer,&stride,&offset);

	deviceContext->IASetIndexBuffer(mIndexBuffer,DXGI_FORMAT_R32_UINT,0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}