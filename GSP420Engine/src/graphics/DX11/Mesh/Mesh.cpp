#include "Mesh.h"

#include "core\JSON.h"

Mesh::Mesh() {
	mIndexCount = 0;
	mVertexCount = 0;

	mIndexBuffer = nullptr;
	mVertexBuffer = nullptr;
}

Mesh::~Mesh() {
	Release();
}

bool Mesh::InitializeBuffers(ID3D11Device *device,const string_normal meshFile) {
	unsigned long *indices = nullptr;
	D3D11_BUFFER_DESC vertexBufferDesc,indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData,indexData;

	std::ifstream fin;
	char input;
	int i;


	// Open the model file.
	fin.open(meshFile);

	// If it could not open the file then exit.
	if(fin.fail()) {
		return false;
	}

	// Read up to the value of vertex count.
	fin.get(input);
	while(input != ':') {
		fin.get(input);
	}

	// Read in the vertex count.
	fin >> mVertexCount;

	mIndexCount = mVertexCount;

	// Create the model using the vertex count that was read in.
	mVertices = new VertexPTN[mVertexCount];
	if(!mVertices) {
		return false;
	}

	indices = new unsigned long[mIndexCount];
	if(!indices) {
		return false;
	}

	// Read up to the beginning of the data.
	fin.get(input);
	while(input != ':') {
		fin.get(input);
	}
	fin.get(input);
	fin.get(input);

	// Read in the vertex data.
	for(i = 0; i < mVertexCount; i++) {
		fin >> mVertices[i].Position.x >> mVertices[i].Position.y >> mVertices[i].Position.z;
		fin >> mVertices[i].Texture.x >> mVertices[i].Texture.y;
		fin >> mVertices[i].Normal.x >> mVertices[i].Normal.y >> mVertices[i].Normal.z;

		indices[i] = i;
	}

	// Close the model file.
	fin.close();

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexPTN) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = mVertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	HR(device->CreateBuffer(&vertexBufferDesc,&vertexData,&mVertexBuffer));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * mIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	HR(device->CreateBuffer(&indexBufferDesc,&indexData,&mIndexBuffer));

	Memory::SafeDeleteArr(indices);

	return true;
}

void Mesh::RenderBuffers(ID3D11DeviceContext *deviceContext) {
	uint32 stride = sizeof(VertexPTN);
	uint32 offset = 0;

	deviceContext->IASetVertexBuffers(0,1,&mVertexBuffer,&stride,&offset);

	deviceContext->IASetIndexBuffer(mIndexBuffer,DXGI_FORMAT_R32_UINT,0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Mesh::RenderShader(ID3D11DeviceContext *deviceContext,ShaderBase *shader) {
	shader->RenderShader(deviceContext,mIndexCount);
}

void Mesh::Render(ID3D11DeviceContext *deviceContext,ShaderBase *shader) {
	RenderBuffers(deviceContext);

	RenderShader(deviceContext,shader);
}

void Mesh::Release() {
	if(mIndexBuffer)
		Memory::SafeRelease(mIndexBuffer);

	if(mVertexBuffer)
		Memory::SafeRelease(mVertexBuffer);
	if(mVertices)
		Memory::SafeDeleteArr(mVertices);
}

void Mesh::CopyVertexArray(void *vertArr) {
	memcpy(vertArr,mVertices,sizeof(VertexPTN) * mVertexCount);
}

void Mesh::GetVertexCountAndArray(uint32 &count,void *vertices) {
	CopyVertexArray(vertices);
	count = mVertexCount;
 }