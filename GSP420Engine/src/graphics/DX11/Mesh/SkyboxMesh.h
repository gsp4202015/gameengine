#pragma once
#include "core\EngineTypes.h"

#include "Mesh.h"

class SkyboxMesh : public Mesh {
public:
	SkyboxMesh();
	~SkyboxMesh();

	bool InitializeBuffers(ID3D11Device *device);
	void RenderBuffers(ID3D11DeviceContext *deviceContext);
};