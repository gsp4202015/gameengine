#include "PyramidMesh.h"

PyramidMesh::PyramidMesh(cstring name)
	: Mesh() {
}

PyramidMesh::~PyramidMesh() {
}

bool PyramidMesh::InitializeBuffers(ID3D11Device *device) {
	VertexPTN *vertices;
	unsigned long *indices;
	D3D11_BUFFER_DESC vertexBufferDesc,indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData,indexData;

	mVertexCount = 15;
	mIndexCount = 18;

	vertices = new VertexPTN[mVertexCount];
	if(!vertices)
		return false;

	indices = new unsigned long[mIndexCount];
	if(!indices)
		return false;

	//TOP
	vertices[0].Position = Vector3(0.0f,1.0f,0.0f);
	vertices[0].Texture = Vector2(0.5f,0.0f);
	vertices[0].Normal = Vector3(1.0f,0.0f,0.0f);

	//FRONT
	vertices[1].Position = Vector3(-1.0f,-1.0f,-1.0f);
	vertices[1].Texture = Vector2(0.0f,1.0f);
	vertices[1].Normal = Vector3(1.0f,0.0f,0.0f);

	vertices[2].Position = Vector3(1.0f,-1.0f,-1.0f);
	vertices[2].Texture = Vector2(1.0f,1.0f);
	vertices[2].Normal = Vector3(1.0f,0.0f,0.0f);

	//LEFT
	vertices[3].Position = Vector3(-1.0f,-1.0f,1.0f);
	vertices[3].Texture = Vector2(0.0f,1.0f);
	vertices[3].Normal = Vector3(1.0f,0.0f,0.0f);

	vertices[4].Position = Vector3(-1.0f,-1.0f,-1.0f);
	vertices[4].Texture = Vector2(1.0f,1.0f);
	vertices[4].Normal = Vector3(1.0f,0.0f,0.0f);

	//RIGHT
	vertices[5].Position = Vector3(1.0f,-1.0f,-1.0f);
	vertices[5].Texture = Vector2(0.0f,1.0f);
	vertices[5].Normal = Vector3(1.0f,0.0f,0.0f);

	vertices[6].Position = Vector3(1.0f,-1.0f,1.0f);
	vertices[6].Texture = Vector2(1.0f,1.0f);
	vertices[6].Normal = Vector3(1.0f,0.0f,0.0f);

	//BACK
	vertices[7].Position = Vector3(1.0f,-1.0f,1.0f);
	vertices[7].Texture = Vector2(1.0f,1.0f);
	vertices[7].Normal = Vector3(1.0f,0.0f,0.0f);

	vertices[8].Position = Vector3(-1.0f,-1.0f,1.0f);
	vertices[8].Texture = Vector2(0.0f,1.0f);
	vertices[8].Normal = Vector3(1.0f,0.0f,0.0f);

	//BOTTOM
	vertices[9].Position = Vector3(-1.0f,-1.0f,-1.0f);
	vertices[9].Texture = Vector2(0.0f,1.0f);
	vertices[9].Normal = Vector3(0.0f,-1.0f,0.0f);

	vertices[10].Position = Vector3(1.0f,-1.0f,1.0f);
	vertices[10].Texture = Vector2(1.0f,0.0f);
	vertices[10].Normal = Vector3(0.0f,-1.0f,0.0f);

	vertices[11].Position = Vector3(-1.0f,-1.0f,1.0f);
	vertices[11].Texture = Vector2(0.0f,0.0f);
	vertices[11].Normal = Vector3(0.0f,-1.0f,0.0f);

	vertices[12].Position = Vector3(1.0f,-1.0f,-1.0f);
	vertices[12].Texture = Vector2(1.0f,1.0f);
	vertices[12].Normal = Vector3(0.0f,-1.0f,0.0f);

	vertices[13].Position = Vector3(1.0f,-1.0f,1.0f);
	vertices[13].Texture = Vector2(1.0f,0.0f);
	vertices[13].Normal = Vector3(0.0f,-1.0f,0.0f);

	vertices[14].Position = Vector3(-1.0f,-1.0f,-1.0f);
	vertices[14].Texture = Vector2(0.0f,1.0f);
	vertices[14].Normal = Vector3(0.0f,-1.0f,0.0f);

	//FRONT
	indices[0] = 0;
	indices[1] = 2;
	indices[2] = 1;

	indices[3] = 0;
	indices[4] = 4;
	indices[5] = 3;

	indices[6] = 0;
	indices[7] = 6;
	indices[8] = 5;

	indices[9] = 0;
	indices[10] = 8;
	indices[11] = 7;

	indices[12] = 9;
	indices[13] = 10;
	indices[14] = 11;

	indices[15] = 12;
	indices[16] = 13;
	indices[17] = 14;

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexPTN) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	HR(device->CreateBuffer(&vertexBufferDesc,&vertexData,&mVertexBuffer));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * mIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = indices;

	HR(device->CreateBuffer(&indexBufferDesc,&indexData,&mIndexBuffer));

	Memory::SafeDeleteArr(vertices);

	Memory::SafeDeleteArr(indices);

	return true;
}

void PyramidMesh::RenderBuffers(ID3D11DeviceContext *deviceContext) {
	uint32 stride = sizeof(VertexPTN);
	uint32 offset = 0;

	deviceContext->IASetVertexBuffers(0,1,&mVertexBuffer,&stride,&offset);

	deviceContext->IASetIndexBuffer(mIndexBuffer,DXGI_FORMAT_R32_UINT,0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}