#pragma once

#include "Mesh.h"

class PyramidMesh : public Mesh {
public:
	PyramidMesh(cstring name);
	~PyramidMesh();

	bool InitializeBuffers(ID3D11Device *device);
	void RenderBuffers(ID3D11DeviceContext *deviceContext);
};
