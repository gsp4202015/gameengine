#pragma once
#include "core\EngineTypes.h"

#include "graphics\DX11\Shader\ShaderBase.h"

#include <string>
#include <fstream>

struct VertexPT {
	Vector3 Position;
	Vector2 Texture;
};

struct VertexPT2 {
	Vector2 Position;
	Vector2 Texture;
};

struct VertexPTN {
	Vector3 Position;
	Vector2 Texture;
	Vector3 Normal;
};

struct VertexP {
	Vector3 Position;
};

struct VertexPCN {
	Vector3 Position;
	Vector4 Color;
	Vector3 Normal;
};

class ShaderBase;

class Mesh {
protected:
	VertexPTN *mVertices;
	ID3D11Buffer *mIndexBuffer,*mVertexBuffer;
	uint32	mIndexCount,mVertexCount;

public:
	Mesh();
	~Mesh();

	bool InitializeBuffers(ID3D11Device *device, const string_normal meshFile);
	virtual void RenderBuffers(ID3D11DeviceContext *deviceContext);

	void Release();

	void RenderShader(ID3D11DeviceContext *deviceContext,ShaderBase *shader);
	void Render(ID3D11DeviceContext *deviceContext,ShaderBase *shader);
	void CopyVertexArray(void *vertArr);

	uint32 GetVertexCount() const { return mVertexCount; };
	uint32 GetIndexCount() const { return mIndexCount; };

	ID3D11Buffer* GetIndexBuffer() { return mIndexBuffer; };
	ID3D11Buffer* GetVertexBuffer() { return mVertexBuffer; };

	void GetVertexCountAndArray(uint32 &count, void *vertices);
};
