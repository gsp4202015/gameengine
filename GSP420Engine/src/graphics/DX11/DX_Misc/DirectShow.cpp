#include "DirectShow.h"

DirectShow::DirectShow(void)
{
}

DirectShow::~DirectShow(void)
{
}

void DirectShow::PlayVideo(HWND hWnd,string_normal fileName,Input* input)
{
	bool mIsPlaying = true;
	IGraphBuilder *mGraphBuilder;
	IMediaControl *mMediaController;
	IVideoWindow * mWindow;
	IMediaEvent *mEvent;

	HR(CoInitialize(NULL));

	HR(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC, IID_IGraphBuilder, (void **)&mGraphBuilder));

	HR(mGraphBuilder->QueryInterface(IID_IMediaControl, (void**)&mMediaController));
	HR(mGraphBuilder->QueryInterface(IID_IMediaEventEx, (void**)&mEvent));
	HR(mGraphBuilder->QueryInterface(IID_IVideoWindow, (void**)&mWindow));
	
	HR(mGraphBuilder->RenderFile(string_normal_to_wide(fileName).c_str(), NULL));
	
	HR(mWindow->put_Owner((OAHWND)hWnd));
	HR(mWindow->put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS));

	RECT rect;
	GetClientRect(hWnd, &rect);

	HR(mWindow->SetWindowPosition(rect.left, rect.top, rect.right, rect.bottom));

	HR(mMediaController->Run());

	long evCode = 0;
	LONG_PTR param1;
	LONG_PTR param2;

	while (mIsPlaying)
	{
		mEvent->GetEvent(&evCode, &param1, &param2, 0);
		input->updateInput();

		if (evCode == EC_COMPLETE)
			mIsPlaying = false;
		else if (input->getKeyIsPressed(VK_ESCAPE))
			mIsPlaying = false;
			
		mEvent->FreeEventParams(evCode, param1, param2);
	}

	mMediaController->Stop();
	
	Memory::SafeRelease(mGraphBuilder);
	Memory::SafeRelease(mMediaController);
	Memory::SafeRelease(mWindow);
	Memory::SafeRelease(mEvent);
}