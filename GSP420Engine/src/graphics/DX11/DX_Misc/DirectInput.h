#pragma once
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include "DXUtility.h"

enum KeyState{
	NoChange,
	Pressed,
	Held,
	Released
};

class DirectInput
{
private:
	IDirectInput8* mDInput;
	IDirectInputDevice8* mKeyboard;
	IDirectInputDevice8* mMouse;
	DIMOUSESTATE2 mMouseState;
	char mKeyBuffer[256];
	char mCurrKeyBuffer[256];
	char mLastKeyBuffer[256];
	POINT mMousePosition;

public:
	DirectInput(HINSTANCE hInst, HWND hWnd);
	~DirectInput(void);

	void CheckDevice(HWND hWnd);
	bool KeyPressed(char key){return (mKeyBuffer[key] & 0x80) != 0;};
	bool MousePressed(int button){return (mMouseState.rgbButtons[button] & 0x80) != 0;};

	KeyState GetKeyState(int key);
	bool IsKeyPressed(int key);
	bool IsKeyHeld(int key);
	bool IsKeyReleased(int key);

	POINT GetMousePos(){return mMousePosition;};
	const float GetMouseDx() const {return (float)mMouseState.lX;};
	const float GetMouseDy() const {return (float)mMouseState.lY;};
	const float GetMouseDz() const {return (float)mMouseState.lZ;};
};

