#include "DirectInput.h"

DirectInput::DirectInput(HINSTANCE hInst, HWND hWnd)
{
	ZeroMemory(mKeyBuffer, sizeof(mKeyBuffer));
	ZeroMemory(&mMouseState, sizeof(mMouseState));
	ZeroMemory(mKeyBuffer, sizeof(mKeyBuffer));
	ZeroMemory(mLastKeyBuffer, sizeof(mLastKeyBuffer));
	ZeroMemory(mCurrKeyBuffer, sizeof(mCurrKeyBuffer));

	DirectInput8Create(hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&mDInput, 0);

	mDInput->CreateDevice(GUID_SysKeyboard, &mKeyboard, 0);
	mKeyboard->SetDataFormat(&c_dfDIKeyboard);
	mKeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	mKeyboard->Acquire();

	mDInput->CreateDevice(GUID_SysMouse, &mMouse, 0);
	mMouse->SetDataFormat(&c_dfDIMouse2);
	mMouse->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	mMouse->Acquire();
}

DirectInput::~DirectInput(void)
{
	mDInput->Release();
	mKeyboard->Unacquire();
	mMouse->Unacquire();
	mKeyboard->Release();
	mMouse->Release();
}

void DirectInput::CheckDevice(HWND hWnd)
{
	HRESULT hr = mKeyboard->GetDeviceState(sizeof(mKeyBuffer), (void**)&mKeyBuffer);
	if (FAILED(hr))
	{
		//Keyboard lost
		ZeroMemory(mKeyBuffer, sizeof(mKeyBuffer));

		hr = mKeyboard->Acquire();
	}

	hr = mMouse->GetDeviceState(sizeof(DIMOUSESTATE2), (void**)&mMouseState);
	if (FAILED(hr))
	{
		//Mouse lost
		ZeroMemory(&mMouseState, sizeof(mMouseState));

		hr = mMouse->Acquire();
	}

	POINT mouse;
	GetCursorPos(&mouse);
	ScreenToClient(hWnd, &mouse);

	mMousePosition.x = mouse.x;
	mMousePosition.y = mouse.y;
}

KeyState DirectInput::GetKeyState(int key)
{
	KeyState temp;

	mCurrKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	if ((mCurrKeyBuffer[key] != mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 0))
		temp = Pressed;
	else if ((mCurrKeyBuffer[key] == mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 1))
		temp = Held;
	else if ((mCurrKeyBuffer[key] != mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 1))
		temp = Released;
	else
		temp = NoChange;

	mLastKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	return temp;
}

bool DirectInput::IsKeyPressed(int key)
{
	bool temp;

	mCurrKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	temp = (mCurrKeyBuffer[key] != mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 0);

	mLastKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	return temp;
}

bool DirectInput::IsKeyHeld(int key)
{
	bool temp;

	mCurrKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	temp = (mCurrKeyBuffer[key] == mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 1);

	mLastKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	return temp;
}

bool DirectInput::IsKeyReleased(int key)
{
	bool temp = false;

	mCurrKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	if (mCurrKeyBuffer[key] != mLastKeyBuffer[key])
	{
		temp = (mCurrKeyBuffer[key] != mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 1);
	}
	mLastKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	return temp;
}