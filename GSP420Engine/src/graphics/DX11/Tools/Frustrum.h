#pragma once
#include "graphics\DX11\DX_Misc\DXUtility.h"
#include "graphics\DX11\Manager\ShaderManager.h"

class Frustrum {
private:
	Vector4 mPlanes[6];

public:
	Frustrum(ID3D11Device *device);
	~Frustrum();

	void ConstructFrustum(float screenDepth,Matrix projection,Matrix view);
	bool CheckPoint(Vector3 position);
	bool CheckCube(Vector3 center,float radius);
	bool CheckSphere(Vector3 center,float radius);
	bool CheckRectangle(Vector3 center,Vector3 size);
};
