#pragma once

#include "core\EngineTypes.h"
#include "graphics\DX11\DX_Misc\DXUtility.h"

class RenderTexture {
private:
	ID3D11Texture2D* mRenderTargetTexture;
	ID3D11RenderTargetView* mRenderTargetView;
	ID3D11ShaderResourceView* mShaderResourceView;
	ID3D11Texture2D* mDepthStencilBuffer;
	ID3D11DepthStencilView* mDepthStencilView;
	D3D11_VIEWPORT mViewport;
	Matrix mProjectionMatrix;
	Matrix mOrthoMatrix;

public:
	RenderTexture(ID3D11Device* device,uint32 textureWidth,uint32 textureLength,float screenDepth,float screenNear);
	~RenderTexture();

	void Release();
	void SetRenderTarget(ID3D11DeviceContext* deviceContext);
	void ClearRenderTarget(ID3D11DeviceContext* deviceContext,float red,float green,float blue,float alpha);

	ID3D11ShaderResourceView* GetShaderResourceView() { return mShaderResourceView; };
	Matrix GetProjectionMatrix() { return mProjectionMatrix; };
	Matrix GetOrthoMatrix() { return mOrthoMatrix; };
};