#include "GameCamera.h"

GameCamera::GameCamera(Vector3 position,Vector3 lookAt,float speed,float sensitivity,HWND hWnd,uint32 screenWidth,uint32 screenHeight) {
	mProjectionMatrix = DirectX::XMMatrixIdentity();
	mViewMatrix = DirectX::XMMatrixIdentity();
	mInvertedPitchMatrix = DirectX::XMMatrixIdentity();
	mOrthographicMatrix = DirectX::XMMatrixIdentity();

	mUp = Vector3(0.0f,1.0f,0.0f);
	mRight = Vector3(1.0f,0.0f,0.0f);
	mForward = Vector3(0.0f,0.0f,1.0f);

	mPosition = position;
	mPrevPosition = position;
	mRotation = Vector3(0.0f,0.0f,0.0f);

	mLookAt = DirectX::XMVector3Normalize((lookAt - position));

	mLookAt *= 10.0f;
	mSpeed = speed;
	mSensitivity = sensitivity;

	mInvertedPitchMatrix = DirectX::XMMatrixRotationAxis(mRight,0.0f);

	BuildProjectionOrthographicMatrix(hWnd,screenWidth,screenHeight);
	BuildViewMatrix();
}

GameCamera::~GameCamera() {
}

void GameCamera::Update(float dt,DirectInput *dInput) {
	mForward = DirectX::XMVector3Normalize(mForward);
	mRight = DirectX::XMVector3Normalize(mRight);

	mUp = DirectX::XMVector3Cross(mForward,mRight);
	mUp = DirectX::XMVector3Normalize(mUp);

	BuildViewMatrix();

	Rotate(dt,dInput);
	Transform(dt,dInput);

	mLookAt = mPosition + mForward;
}

void GameCamera::Rotate(float dt,DirectInput* dInput) {
	if(dInput->MousePressed(1)) {
		float pitch = dInput->GetMouseDy() * dt;
		float yAngle = dInput->GetMouseDx() * dt;

		pitch *= mSensitivity;
		yAngle *= mSensitivity;

		Matrix rotAxis;
		rotAxis = DirectX::XMMatrixRotationAxis(mRight,pitch);
		mInvertedPitchMatrix = DirectX::XMMatrixRotationAxis(mRight,-pitch);
		mForward = DirectX::XMVector3TransformCoord(mForward,rotAxis);
		mUp = DirectX::XMVector3TransformCoord(mUp,rotAxis);

		Matrix rotYAxis;
		rotYAxis = DirectX::XMMatrixRotationY(yAngle);
		mRight = DirectX::XMVector3TransformCoord(mRight,rotYAxis);
		mUp = DirectX::XMVector3TransformCoord(mUp,rotYAxis);
		mForward = DirectX::XMVector3TransformCoord(mForward,rotYAxis);

		mInvertedPitchMatrix *= rotYAxis;
	}
}

void GameCamera::Transform(float dt,DirectInput* dInput) {
	float speedSet = mSpeed;
	if(dInput->IsKeyHeld(DIK_LSHIFT)) {
		speedSet *= 10;
	}

	Vector3 direction(0.0f,0.0f,0.0f);

	if(dInput->IsKeyHeld(DIK_W)) {
		direction += mForward;
	}

	if(dInput->IsKeyHeld(DIK_S)) {
		direction -= mForward;
	}

	if(dInput->IsKeyHeld(DIK_D)) {
		direction += mRight;
	}

	if(dInput->IsKeyHeld(DIK_A)) {
		direction -= mRight;
	}

	if(dInput->IsKeyHeld(DIK_Q)) {
		direction.y += 1;
	}

	if(dInput->IsKeyHeld(DIK_E)) {
		direction.y -= 1;
	}

	direction = DirectX::XMVector3Normalize(direction);
	mPrevPosition = mPosition;
	mPosition += direction * speedSet * dt;
	mLookAt += direction * speedSet * dt;
}

void GameCamera::BuildViewMatrix() {
	mViewMatrix = DirectX::XMMatrixLookAtLH(mPosition,mLookAt,mUp);
}

void GameCamera::BuildProjectionOrthographicMatrix(HWND hWnd,uint32 screenWidth,uint32 screenHeight) {
	RECT tempRect;
	GetClientRect(hWnd,&tempRect);

	float w = (float)tempRect.right;
	float h = (float)tempRect.bottom;

	mOrthographicMatrix = DirectX::XMMatrixOrthographicLH(static_cast<float>(screenWidth),static_cast<float>(screenHeight),1.0f,1000.0f);
	mProjectionMatrix = DirectX::XMMatrixPerspectiveFovLH(DirectX::XM_PI / 4.0f,w / h,1.0f,1500.0f);
}