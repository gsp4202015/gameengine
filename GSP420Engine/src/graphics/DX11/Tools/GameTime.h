#pragma once
#include "graphics\DX11\DX_Misc\DXUtility.h"

class GameTime {
private:
	LONGLONG mStart;
	float mFrequencySecs,mElapsedTime,mTotalTime;

public:
	GameTime(void);
	~GameTime(void);

	bool Initialize();
	void Update();

	const float GetTime() const { return mTotalTime; };
	const float GetDeltaTime() const { return mElapsedTime; };
};