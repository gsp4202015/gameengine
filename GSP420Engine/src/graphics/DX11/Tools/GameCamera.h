#pragma once

#include "core\EngineTypes.h"
#include "graphics\DX11\DX_Misc\DXUtility.h"
#include "graphics\DX11\DX_Misc\DirectInput.h"

class GameCamera {
private:
	Matrix mViewMatrix;
	Matrix mProjectionMatrix;
	Matrix mOrthographicMatrix;
	Matrix mInvertedPitchMatrix;

	Vector3 mPosition;
	Vector3 mPrevPosition;
	Vector3 mRotation;
	Vector3 mUp,mForward,mRight;
	Vector3 mLookAt;

	Vector2 mWindowCenter;

	float mSpeed;
	float mSensitivity;

public:
	GameCamera(Vector3 position,Vector3 lookAt, float speed,float sensitivity, HWND hWnd, uint32 screenWidth, uint32 screenHeight);
	~GameCamera();

	void Update(float dt,DirectInput *dInput);
	void BuildViewMatrix();
	void BuildProjectionOrthographicMatrix(HWND hWnd,uint32 screenWidth,uint32 screenHeight);
	void Rotate(float dt,DirectInput* dInput);
	void Transform(float dt,DirectInput* dInput);

	Matrix GetOrthographicMatrix() { return mOrthographicMatrix; };
	Matrix GetViewMatrix() { return mViewMatrix; };
	Matrix GetProjectionMatrix() { return mProjectionMatrix; };
	Vector3 GetPosition() { return Vector3(mPosition); };
	Vector3 GetPreviousPosition() { return mPrevPosition; };
	Vector3 GetRotation() { return Vector3(mRotation); };

	void SetPosition(float x,float y,float z) { mPosition.x = x; mPosition.y = y; mPosition.z = z; };
	void SetRotation(float x,float y,float z) { mRotation.x = x; mRotation.y = y; mRotation.z = z; };
};
