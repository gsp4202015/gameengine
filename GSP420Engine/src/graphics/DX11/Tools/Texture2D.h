#pragma once
#include "core\EngineTypes.h"

#include "graphics\DX11\DX_Misc\DXUtility.h"

#include <string>

class DXTexture2D {
private:
	ID3D11Resource*				mResource;
	ID3D11ShaderResourceView*	mTexture;
	UINT						mWidth;
	UINT						mHeight;
	engine_string				mFileName;
	engine_string				mRegistrationName;

public:
	DXTexture2D(const engine_string &fileName,const engine_string &registrationName,ID3D11Resource* rsrce,ID3D11ShaderResourceView* texture,UINT width,UINT height);
	~DXTexture2D();

	void Release();

	//Accessors
	ID3D11Resource* GetResource() const { return mResource; };
	ID3D11ShaderResourceView* GetTexture() const { return mTexture; };
	const UINT GetWidth() const { return mWidth; };
	const UINT GetHeight() const { return mHeight; };
	const engine_string& GetFileName() const { return mFileName; }
	const engine_string& GetRegistrationName() const { return mRegistrationName; }
};
