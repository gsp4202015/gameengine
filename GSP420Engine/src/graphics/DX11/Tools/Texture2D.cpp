#include "Texture2D.h"

DXTexture2D::DXTexture2D(const engine_string &fileName,const engine_string &registrationName,ID3D11Resource* rsrce,ID3D11ShaderResourceView* texture,UINT width,UINT height) {
	mFileName = fileName;
	mRegistrationName = registrationName;
	mResource = rsrce;
	mTexture = texture;
	mWidth = width;
	mHeight = height;
}

DXTexture2D::~DXTexture2D() {
	Release();
}

void DXTexture2D::Release() {
	Memory::SafeRelease(mResource);
	Memory::SafeRelease(mTexture);
}