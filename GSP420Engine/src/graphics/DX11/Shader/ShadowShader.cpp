#include "ShadowShader.h"

ShadowShader::ShadowShader(cstring vsFileName,cstring psFileName)
	: ShaderBase(vsFileName,psFileName) {
	mSampleStateWrap = 0;
	mSampleStateClamp = 0;
	mLightBuffer = 0;
	mLightBuffer2 = 0;
}

ShadowShader::~ShadowShader() {
}

void ShadowShader::Release() {
	ShaderBase::Release();

	if(mLightBuffer)
		Memory::SafeRelease(mLightBuffer);

	if(mLightBuffer2)
		Memory::SafeRelease(mLightBuffer2);

	if(mSampleStateWrap)
		Memory::SafeRelease(mSampleStateWrap);

	if(mSampleStateClamp)
		Memory::SafeRelease(mSampleStateClamp);
}

//Variable List Types: { w = world matrix, v = view matrix, p = projection matrix, a = light view matrix, b = light projection matrix,
//c = ambient color vector4, d = diffuse color vector4, e = light direction vector3, f = light position}
void ShadowShader::SetBuffers(ID3D11DeviceContext *deviceContext,cstring list,...) {
	Matrix world = DirectX::XMMatrixIdentity();
	Matrix view = DirectX::XMMatrixIdentity();
	Matrix projection = DirectX::XMMatrixIdentity();
	Matrix lightView = DirectX::XMMatrixIdentity();
	Matrix lightProjection = DirectX::XMMatrixIdentity();
	Vector4 ambient;
	Vector4 diffuse;
	Vector3 lightDir;
	Vector3 lightPos;

	va_list vl;
	va_start(vl,list);

	for(uint32 i = 0; list[i] != '\0'; i++) {
		switch(list[i]) {
			case 'w':
				world = va_arg(vl,Matrix);
				break;

			case 'v':
				view = va_arg(vl,Matrix);
				break;

			case 'p':
				projection = va_arg(vl,Matrix);
				break;

			case 'a':
				lightView = va_arg(vl,Matrix);
				break;

			case 'b':
				lightProjection = va_arg(vl,Matrix);
				break;

			case 'c':
				ambient = va_arg(vl,Vector4);
				break;

			case 'd':
				diffuse = va_arg(vl,Vector4);
				break;

			case 'e':
				lightDir = va_arg(vl,Vector3);
				break;

			case 'f':
				lightPos = va_arg(vl,Vector3);
				break;

			default:
				break;
		}
	}

	va_end(vl);

	//Setup shader parameters
	world = DirectX::XMMatrixTranspose(world);
	view = DirectX::XMMatrixTranspose(view);
	projection = DirectX::XMMatrixTranspose(projection);
	lightView = DirectX::XMMatrixTranspose(lightView);
	lightProjection = DirectX::XMMatrixTranspose(lightProjection);

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	deviceContext->Map(mMatrixBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);

	MatrixBuffer2 *dataPtr = (MatrixBuffer2*)mappedResource.pData;
	dataPtr->World = world;
	dataPtr->View = view;
	dataPtr->Projection = projection;
	dataPtr->LightView = lightView;
	dataPtr->LightProjection = lightProjection;

	deviceContext->Unmap(mMatrixBuffer,0);

	uint32 bufferNumber = 0;
	deviceContext->VSSetConstantBuffers(bufferNumber,1,&mMatrixBuffer);

	deviceContext->Map(mLightBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);

	LightBuffer* dataPtr2;
	dataPtr2 = (LightBuffer*)mappedResource.pData;

	dataPtr2->AmbientColor = ambient;
	dataPtr2->DiffuseColor = diffuse;
	dataPtr2->LightDirection = lightDir;
	dataPtr2->Padding = 0.0f;

	deviceContext->Unmap(mLightBuffer,0);

	bufferNumber = 0;
	deviceContext->PSSetConstantBuffers(bufferNumber,1,&mLightBuffer);

	deviceContext->Map(mLightBuffer2,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);

	LightBuffer2* dataPtr3;
	dataPtr3 = (LightBuffer2*)mappedResource.pData;

	dataPtr3->LightPosition = lightPos;
	dataPtr3->Padding = 0.0f;

	deviceContext->Unmap(mLightBuffer2,0);

	bufferNumber = 1;
	deviceContext->VSSetConstantBuffers(bufferNumber,1,&mLightBuffer2);
}

void ShadowShader::SetTexture(ID3D11DeviceContext *deviceContext,cstring list,...) {
	va_list vl;
	va_start(vl,list);

	for(uint32 i = 0; list[i] != '\0'; i++) {
		switch(list[i]) {
			case 'd':
				deviceContext->PSSetShaderResources(1,1,&va_arg(vl,ID3D11ShaderResourceView*));
				break;

			case 'e':
				deviceContext->PSSetShaderResources(0,1,&va_arg(vl,ID3D11ShaderResourceView*));
				break;

			default:
				break;
		}
	}

	va_end(vl);
}

bool ShadowShader::InitializeShader(ID3D11Device *device,HWND hWnd) {
	ID3DBlob *errorMessage;
	ID3DBlob *vertexShaderBuffer;
	ID3DBlob *pixelShaderBuffer;

	HR(D3DCompileFromFile(string_to_wide(mVsFileName).c_str(),NULL,D3D_COMPILE_STANDARD_FILE_INCLUDE,"VSMain","vs_5_0",D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG,0,&vertexShaderBuffer,&errorMessage));
	HR(D3DCompileFromFile(string_to_wide(mPsFileName).c_str(),NULL,D3D_COMPILE_STANDARD_FILE_INCLUDE,"PSMain","ps_5_0",D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG,0,&pixelShaderBuffer,&errorMessage));

	HR(device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(),vertexShaderBuffer->GetBufferSize(),NULL,&mVertexShader));
	HR(device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(),pixelShaderBuffer->GetBufferSize(),NULL,&mPixelShader));

	D3D11_INPUT_ELEMENT_DESC polygonLayout[3];
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "NORMAL";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;

	uint32 numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	HR(device->CreateInputLayout(polygonLayout,numElements,vertexShaderBuffer->GetBufferPointer(),vertexShaderBuffer->GetBufferSize(),&mInputLayout));

	Memory::SafeRelease(vertexShaderBuffer);

	Memory::SafeRelease(pixelShaderBuffer);

	D3D11_BUFFER_DESC matrixBufferDesc;
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer2);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	HR(device->CreateBuffer(&matrixBufferDesc,NULL,&mMatrixBuffer));

	D3D11_BUFFER_DESC lightBufferDesc;
	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(LightBuffer);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;

	HR(device->CreateBuffer(&lightBufferDesc,NULL,&mLightBuffer));

	D3D11_BUFFER_DESC lightBuffer2Desc;
	lightBuffer2Desc.Usage = D3D11_USAGE_DYNAMIC;
	lightBuffer2Desc.ByteWidth = sizeof(LightBuffer2);
	lightBuffer2Desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBuffer2Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBuffer2Desc.MiscFlags = 0;
	lightBuffer2Desc.StructureByteStride = 0;

	HR(device->CreateBuffer(&lightBuffer2Desc,NULL,&mLightBuffer2));

	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HR(device->CreateSamplerState(&samplerDesc,&mSampleStateWrap));

	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

	HR(device->CreateSamplerState(&samplerDesc,&mSampleStateClamp));

	return true;
}

void ShadowShader::RenderShader(ID3D11DeviceContext *deviceContext,uint32 indexCount) {
	//Render shader
	deviceContext->IASetInputLayout(mInputLayout);

	deviceContext->VSSetShader(mVertexShader,NULL,0);
	deviceContext->PSSetShader(mPixelShader,NULL,0);

	deviceContext->PSSetSamplers(0,1,&mSampleStateWrap);
	deviceContext->PSSetSamplers(0,1,&mSampleStateClamp);

	deviceContext->DrawIndexed(indexCount,0,0);
}