#pragma once
#include "core\EngineTypes.h"

#include "ShaderBase.h"

class SkyboxShader : public ShaderBase {
private:
	ID3D11SamplerState *mSamplerState;

public:
	SkyboxShader(cstring vsFileName,cstring psFileName);
	~SkyboxShader();

	void Release();

	void SetBuffers(ID3D11DeviceContext *deviceContext,cstring list,...);
	void SetTexture(ID3D11DeviceContext *deviceContext,cstring list,...);
	bool InitializeShader(ID3D11Device *device,HWND hWnd);
	void RenderShader(ID3D11DeviceContext *deviceContext,uint32 indexCount);
};