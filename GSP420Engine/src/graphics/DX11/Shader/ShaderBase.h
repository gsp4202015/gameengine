#pragma once

#include "core\EngineTypes.h"
#include "graphics\DX11\DirectX11.h"

#include "graphics\DX11\DX_Misc\DXUtility.h"

class ShaderBase {
protected:

	struct MatrixBuffer1 {
		Matrix World;
		Matrix View;
		Matrix Projection;
	};

	struct MatrixBuffer2 {
		Matrix World;
		Matrix View;
		Matrix Projection;
		Matrix LightView;
		Matrix LightProjection;
	};

	struct PixelBufferType {
		Vector4 PixelColor;
	};

	cstring mPsFileName;
	cstring mVsFileName;

	ID3D11VertexShader *mVertexShader;
	ID3D11PixelShader *mPixelShader;
	ID3D11InputLayout *mInputLayout;
	ID3D11Buffer *mMatrixBuffer;

public:
	ShaderBase(cstring vsFileName,cstring psFileName);
	~ShaderBase();

	virtual void Release();

	virtual void SetTexture(ID3D11DeviceContext *deviceContext,cstring list,...) = 0;
	virtual void SetBuffers(ID3D11DeviceContext *deviceContext,cstring list,...) = 0;
	virtual bool InitializeShader(ID3D11Device *device,HWND hWnd) = 0;
	virtual void RenderShader(ID3D11DeviceContext *deviceContext,uint32 indexCount) = 0;
};