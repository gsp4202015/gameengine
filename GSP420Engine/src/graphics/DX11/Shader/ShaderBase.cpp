#include "ShaderBase.h"

ShaderBase::ShaderBase(cstring vsFileName,cstring psFileName) {
	mVsFileName = vsFileName;
	mPsFileName = psFileName;

	mVertexShader = nullptr;
	mPixelShader = nullptr;
	mInputLayout = nullptr;
	mMatrixBuffer = nullptr;
}

ShaderBase::~ShaderBase() {
	Release();
}

void ShaderBase::Release() {
	if(mMatrixBuffer)
		Memory::SafeRelease(mMatrixBuffer);

	if(mInputLayout)
		Memory::SafeRelease(mInputLayout);

	if(mPixelShader)
		Memory::SafeRelease(mPixelShader);

	if(mVertexShader)
		Memory::SafeRelease(mVertexShader);
}