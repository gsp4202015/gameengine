#include "SkyboxShader.h"

SkyboxShader::SkyboxShader(cstring vsFileName,cstring psFileName)
	: ShaderBase(vsFileName,psFileName) {
	mSamplerState = nullptr;
}

SkyboxShader::~SkyboxShader() {
	if(mSamplerState)
		Memory::SafeRelease(mSamplerState);

	ShaderBase::Release();
}

void SkyboxShader::Release() {
	if(mSamplerState)
		Memory::SafeRelease(mSamplerState);

	ShaderBase::Release();
}

//Variable List Types: { w = world matrix, v = view matrix, p = projection matrix, a = light view matrix, b = light projection matrix }
void SkyboxShader::SetBuffers(ID3D11DeviceContext *deviceContext,cstring list,...) {
	Matrix world = DirectX::XMMatrixIdentity();
	Matrix view = DirectX::XMMatrixIdentity();
	Matrix projection = DirectX::XMMatrixIdentity();
	Matrix lightView = DirectX::XMMatrixIdentity();
	Matrix lightProjection = DirectX::XMMatrixIdentity();

	va_list vl;
	va_start(vl,list);

	for(uint32 i = 0; list[i] != '\0'; i++) {
		switch(list[i]) {
			case 'w':
				world = va_arg(vl,Matrix);
				break;

			case 'v':
				view = va_arg(vl,Matrix);
				break;

			case 'p':
				projection = va_arg(vl,Matrix);
				break;

			case 'a':
				lightView = va_arg(vl,Matrix);
				break;

			case 'b':
				lightProjection = va_arg(vl,Matrix);
				break;

			default:
				break;
		}
	}

	va_end(vl);

	//Setup shader parameters
	world = DirectX::XMMatrixTranspose(world);
	view = DirectX::XMMatrixTranspose(view);
	projection = DirectX::XMMatrixTranspose(projection);
	lightView = DirectX::XMMatrixTranspose(lightView);
	lightProjection = DirectX::XMMatrixTranspose(lightProjection);

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	deviceContext->Map(mMatrixBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);

	MatrixBuffer2 *dataPtr = (MatrixBuffer2*)mappedResource.pData;
	dataPtr->World = world;
	dataPtr->View = view;
	dataPtr->Projection = projection;
	dataPtr->LightView = lightView;
	dataPtr->LightProjection = lightProjection;

	deviceContext->Unmap(mMatrixBuffer,0);

	uint32 bufferNumber = 0;
	deviceContext->VSSetConstantBuffers(bufferNumber,1,&mMatrixBuffer);
}

bool SkyboxShader::InitializeShader(ID3D11Device *device,HWND hWnd) {
	ID3DBlob *errorMessage;
	ID3DBlob *vertexShaderBuffer;
	ID3DBlob *pixelShaderBuffer;

	HR(D3DCompileFromFile(string_to_wide(mVsFileName).c_str(),NULL,D3D_COMPILE_STANDARD_FILE_INCLUDE,"VSMain","vs_5_0",D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG,0,&vertexShaderBuffer,&errorMessage));
	HR(D3DCompileFromFile(string_to_wide(mPsFileName).c_str(),NULL,D3D_COMPILE_STANDARD_FILE_INCLUDE,"PSMain","ps_5_0",D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG,0,&pixelShaderBuffer,&errorMessage));

	HR(device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(),vertexShaderBuffer->GetBufferSize(),NULL,&mVertexShader));
	HR(device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(),pixelShaderBuffer->GetBufferSize(),NULL,&mPixelShader));

	D3D11_INPUT_ELEMENT_DESC polygonLayout[2];
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	uint32 numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	HR(device->CreateInputLayout(polygonLayout,numElements,vertexShaderBuffer->GetBufferPointer(),vertexShaderBuffer->GetBufferSize(),&mInputLayout));

	Memory::SafeRelease(vertexShaderBuffer);

	Memory::SafeRelease(pixelShaderBuffer);

	D3D11_BUFFER_DESC matrixBufferDesc;
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer2);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	HR(device->CreateBuffer(&matrixBufferDesc,NULL,&mMatrixBuffer));

	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HR(device->CreateSamplerState(&samplerDesc,&mSamplerState));

	return true;
}

//List Types: { d = depth map texture, e = entity texture }
void SkyboxShader::SetTexture(ID3D11DeviceContext *deviceContext,cstring list,...) {
	va_list vl;
	va_start(vl,list);

	for(uint32 i = 0; list[i] != '\0'; i++) {
		switch(list[i]) {
			case 'e':
				deviceContext->PSSetShaderResources(0,1,&va_arg(vl,ID3D11ShaderResourceView*));
				break;

			default:
				break;
		}
	}

	va_end(vl);
}

void SkyboxShader::RenderShader(ID3D11DeviceContext *deviceContext,uint32 indexCount) {
	//Render shader
	deviceContext->IASetInputLayout(mInputLayout);

	deviceContext->VSSetShader(mVertexShader,NULL,0);
	deviceContext->PSSetShader(mPixelShader,NULL,0);

	deviceContext->PSSetSamplers(0,1,&mSamplerState);

	deviceContext->DrawIndexed(indexCount,0,0);
}