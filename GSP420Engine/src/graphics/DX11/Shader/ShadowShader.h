#pragma once
#include "core\EngineTypes.h"

#include "ShaderBase.h"

class ShadowShader : public ShaderBase {
private:

	struct LightBuffer {
		Vector4 AmbientColor;
		Vector4 DiffuseColor;
		Vector3 LightDirection;
		float Padding;
	};

	struct LightBuffer2 {
		Vector3 LightPosition;
		float Padding;
	};

	ID3D11SamplerState *mSampleStateWrap;
	ID3D11SamplerState *mSampleStateClamp;
	ID3D11Buffer *mLightBuffer;
	ID3D11Buffer *mLightBuffer2;

public:
	ShadowShader(cstring vsFileName,cstring psFileName);
	~ShadowShader();

	void Release();

	void SetBuffers(ID3D11DeviceContext *deviceContext,cstring list,...);
	void SetTexture(ID3D11DeviceContext *deviceContext,cstring list,...);
	bool InitializeShader(ID3D11Device *device,HWND hWnd);
	void RenderShader(ID3D11DeviceContext *deviceContext,uint32 indexCount);
};
