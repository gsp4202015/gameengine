#pragma once
#include "core\EngineTypes.h"

#include "ShaderBase.h"

class DepthShader : public ShaderBase {
public:
	DepthShader(cstring vsFileName,cstring psFileName);
	~DepthShader();

	void Release();

	void SetBuffers(ID3D11DeviceContext *deviceContext,cstring list,...);
	void SetTexture(ID3D11DeviceContext *deviceContext,cstring list,...);
	bool InitializeShader(ID3D11Device *device,HWND hWnd);
	void RenderShader(ID3D11DeviceContext *deviceContext,uint32 indexCount);
};