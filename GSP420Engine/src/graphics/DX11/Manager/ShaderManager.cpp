#include "ShaderManager.h"

ShaderManager::ShaderManager() {
}

ShaderManager::~ShaderManager() {
}

void ShaderManager::LoadAndRegisterShader(ID3D11Device *device,HWND hWnd,const engine_string &shaderName,ShaderBase *shader) {
	mShaders.emplace(shaderName,shader);

	shader->InitializeShader(device,hWnd);
}

ShaderBase* ShaderManager::GetShader(const engine_string &shaderName) {
	ShaderBase *temp = mShaders.find(shaderName)->second;

	return temp;
}