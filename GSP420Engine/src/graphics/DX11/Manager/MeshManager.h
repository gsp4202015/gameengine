#pragma once
#include "core\EngineTypes.h"

#include "graphics\DX11\Mesh\Mesh.h"

#include <unordered_map>

class MeshManager {
private:
	std::unordered_map<string_normal,class Mesh*> mMeshes;

public:
	MeshManager();
	~MeshManager();

	bool LoadAndRegisterMesh(ID3D11Device *device,const string_normal registrationName,const string_normal meshFile);
	Mesh* GetMesh(const string_normal &meshName) const;
	Mesh* GetOrLoadMesh(const string_normal &meshName,ID3D11Device *device);
};
