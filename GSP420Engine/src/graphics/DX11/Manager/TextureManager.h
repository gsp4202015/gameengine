#pragma once
#include "core\EngineTypes.h"

#include "graphics\DX11\Tools\Texture2D.h"

#include <unordered_map>
#include <string>

class TextureManager {
private:
	std::unordered_map<engine_string,class DXTexture2D*> mTextures;

public:
	TextureManager();
	~TextureManager();

	void LoadAndRegisterTexture(ID3D11Device* device,const engine_string &registrationName,const engine_string &fileName);

	DXTexture2D* GetTexture(const engine_string &fileName) const;

	DXTexture2D* GetOrLoadTexture(ID3D11Device* device,const engine_string &registrationName,const engine_string &fileName);
};