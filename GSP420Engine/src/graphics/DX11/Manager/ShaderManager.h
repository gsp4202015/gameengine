#pragma once
#include "core\EngineTypes.h"

#include "graphics\DX11\Shader\ShaderBase.h"

#include <unordered_map>

class ShaderManager {
private:
	std::unordered_map<engine_string,class ShaderBase*> mShaders;

public:
	ShaderManager();
	~ShaderManager();

	void LoadAndRegisterShader(ID3D11Device *device,HWND hWnd,const engine_string &shaderName,ShaderBase *shader);

	ShaderBase* GetShader(const engine_string &shaderName);
};
