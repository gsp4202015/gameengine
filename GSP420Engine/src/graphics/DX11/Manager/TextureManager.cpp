#include "TextureManager.h"

#include <ctype.h>
#include <algorithm>
#include <WICTextureLoader.h>
#include <cassert>

TextureManager::TextureManager() {
}

TextureManager::~TextureManager() {
}

void TextureManager::LoadAndRegisterTexture(ID3D11Device* device,const engine_string &registrationName,const engine_string &fileName) {
	ID3D11Resource* rsrce = nullptr;
	ID3D11ShaderResourceView* texture = nullptr;
	UINT width,height;

	// isolate the file type
	auto itDot = std::find(fileName.rbegin(),fileName.rend(),STR('.'));
	engine_string fileType;
	fileType.insert(fileType.begin(),itDot.base(),fileName.end());

	// force lower case
	for(auto &ch : fileType) {
		ch = tolower(ch);
	}

	if(fileType == STR("dds")) {
		HRESULT result = DirectX::CreateDDSTextureFromFile(device,string_to_wide(fileName).c_str(),&rsrce,&texture,DirectX::DDS_ALPHA_MODE_UNKNOWN);
	} else {
		HRESULT result = DirectX::CreateWICTextureFromFile(device,string_to_wide(fileName).c_str(),&rsrce,&texture,DirectX::DDS_ALPHA_MODE_UNKNOWN);
	}


	D3D11_RESOURCE_DIMENSION dim;
	rsrce->GetType(&dim);

	switch(dim) {
		case D3D11_RESOURCE_DIMENSION_TEXTURE2D:
		{
			D3D11_TEXTURE2D_DESC desc;
			auto texture = reinterpret_cast<ID3D11Texture2D*>(rsrce);

			texture->GetDesc(&desc);
			width = desc.Width; //width of texture in pixels
			height = desc.Height; //height of texture in pixels
		}break;

		default:
		{
			width = 0; //width of texture in pixels
			height = 0; //height of texture in pixels
		}
	}

	DXTexture2D* temp = new DXTexture2D(fileName,registrationName,rsrce,texture,width,height);
	mTextures.emplace(registrationName,temp);
}

DXTexture2D* TextureManager::GetTexture(const engine_string &fileName) const {
	auto itFind = mTextures.find(fileName);
	assert(itFind != mTextures.end());
	return itFind->second;
}

DXTexture2D* TextureManager::GetOrLoadTexture(ID3D11Device* device,const engine_string &registrationName,const engine_string &fileName) {
	auto itFind = mTextures.find(registrationName);
	if(itFind != mTextures.end()) {
		return itFind->second;
	} else {
		LoadAndRegisterTexture(device,registrationName,fileName);
		return GetTexture(registrationName);
	}
}