#include "MeshManager.h"

MeshManager::MeshManager() {
}

MeshManager::~MeshManager() {
}

bool MeshManager::LoadAndRegisterMesh(ID3D11Device *device,const string_normal registrationName, const string_normal meshFile)
{
	Mesh *mesh = new Mesh();

	if(!mesh->InitializeBuffers(device,meshFile))
		return false;

	mMeshes.emplace(registrationName,mesh);

	return true;
}

Mesh* MeshManager::GetMesh(const string_normal &meshName) const {
	auto itFind = mMeshes.find(meshName);
	assert(itFind != mMeshes.end());
	return itFind->second;
}