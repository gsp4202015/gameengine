#pragma once

#pragma warning(push)
#pragma warning(disable : 4005)
#define DIRECTINPUT_VERSION 0x0800
#include <d3d11.h>
#include <dinput.h>
#include <DirectXColors.h>
#include <d3dcompiler.h>
#include <SpriteFont.h>
#include "graphics\DX11\DX_Misc\DXError.h"

#include "GeometricPrimitive.h"
#include "DDSTextureLoader.h"
#pragma warning(pop)
