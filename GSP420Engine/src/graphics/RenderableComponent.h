#pragma once

#include "core\ActorComponent.h"
#include "core\Tickable.h"

class GraphicsEntity;
class Mesh;
class DXTexture2D;

class RenderableComponent : public ActorComponent, public Tickable {
	friend class GameApp;

private:
	GraphicsEntity *graphicsEntity;

public:
	RenderableComponent(GraphicsEntity *graphicsEnt);
	virtual ~RenderableComponent();

	void syncInternals();

	void setMesh(const engine_string &name);
	Mesh* getMesh() const;

	void setTexture(const engine_string &name);
	DXTexture2D* getTexture() const;

	virtual void onTick(uint32 groups,engine_duration deltaTime);
};