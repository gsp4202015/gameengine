#include "core\GameApp.h"

#include <iostream>

#include <chrono>

#include "core\ThreadSpooler.h"

#include <windows.h>
#include <iostream>
#include <fstream>
#include <conio.h>
#include <stdio.h>
#include "core\console\guicon.h"

int WINAPI WinMain(__in HINSTANCE hInstance,__in_opt HINSTANCE hPrevInstance,__in LPSTR lpCmdLine,__in int nShowCmd) {
#ifdef _DEBUG
	RedirectIOToConsole();
#endif

	GameApp* gApp = new GameApp(true,false,hInstance,"GSP420 Engine");

	if(!gApp->Initialize(L"data\\assets\\fonts\\Arial.spritefont"))
		return 1;

	return gApp->Run();
}